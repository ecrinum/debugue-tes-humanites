# Débugue tes humanités

Dépôt rassemblant les sources du mini-site dédié à la formation organisée par la Chaire de recherche du Canada sur les écritures numériques.

## Pile logicielle

La pile logicielle utilisée pour le mini-site est la suivante :

- [Hugo](https://gohugo.io/) (version étendue, avec Sass)
- [Sass](https://sass-lang.com/) (langage de pré-processeur pour certaines feuilles de style CSS)
- [Reveal.js](https://revealjs.com/) (pour les diapositives)
- [paged.js](https://www.pagedjs.org/) (pour un rendu expérimental destiné à l'impression)

## Édition

Aide-mémoire pour l'édition.

Pour créer une nouvelle page de séance (attention! la saison doit aussi être spécifiée!) :

```shell
hugo new posts/saison-04/seance-01-premiere-seance-de-debogue.md
```

Ouvrir le fichier créé (ex. ici, la séance `01` pour la saison `04` en cours) et remplir les métadonnées markdown.

```yaml
---
title: "Première séance de Débogue"
description: "Dans cette séance, nous nous familiariserons avec certains usages du terminal."
date: 2023-09-01
# etc.
```

Pour prévisualiser le site, on lancera, à la racine du projet, la commande suivante :

```shell
hugo serve
```

Un serveur sera alors disponible localement, par défaut à l'adresse <http://localhost:1313/>.

## Distribution

Pour construire le site avec Hugo, on n'a qu'à lancer la commande suivante, depuis le répertoire racine du projet :

```shell
hugo
```

et les pages HTML seront construites dans le dossier `public/`, prêtes pour déploiement.

## Vignettes de couverture

Des vignettes de couverture peuvent être générées avec la sortie `/chemin/de/la/page/feature.html` (`/feature.html` à la fin de l'adresse d'une page régulière).
On peut se servir de Firefox pour effectuer une capture d'écran sur le rectangle de 1200px de large (ratio 1.91:1).

La vignette peut être sauvegardée dans le dossier `/static/images/feature/<saison>/<slug>.png` (`<saison>` et `<slug>` doivent être remplacés selon la page réelle).

## Archivage

On a choisi de pérenniser les fichiers HTML à la fin d'une saison, en déplaçant les fichiers HTML (générés dans le dossier `public/`) vers le dossier `static/`.
Ceci fait en sorte qu'ils ne seront plus affectés par des changements futurs avec Hugo (qui est, après tout, un _générateur_ de site statique).

Ces fichiers sont listés à partir de la page `/archives`.
Cette page est spéciale: elle liste des contenus statiques, HTML, indexés dans un dictionnaire YAML (dans le fichier `data/archives.yml`).
Ce fichier YAML a pu être produit dynamiquement avec une nouvelle sortie, disponible à `/archives/index.yml`, lorsque les sources d'une saison y sont déplacés temporairement (ex. `content/archives/saison-02/`).
On peut ensuite copier le YAML généré automatiquement vers le fichier `data/archives.yml`.
Attention à ne pas y écraser les données existantes!
