+++
title = "Séance 01 - Paramétrer son terminal"
date = "2022-01-05"
date_p = "2022-01-14"
description = "Installation de Zsh et de Oh My Zsh, modification de l'apparence. Découverte des fonctions avancées offertes par Zsh. Navigation dans l'historique."
layout = "archives-diapositive"
visible = true
saison = "2"
enregistrement = "https://api.nakala.fr/embed/10.34847/nkl.bb2f1950/15ca12a31f598ad2ef1cd8ce0842f9f55ee7ccce"
+++
{{< psectioni >}}
{{< pcache >}}
## Plan de la séance

1. Rappels utiles
2. Zsh
3. Oh My Zsh
4. L'apparence compte (les couleurs)
5. Fonctions avancées : exemple de la recherche dans l'historique
{{< /pcache >}}

{{< pnote >}}
Mais pourquoi utiliser un terminal alors que la plupart des logiciels, applications ou autres disposent désormais d'interfaces graphiques avec des _boutons_ qui permettent de faire à peu près toutes les actions ?
Pour deux raisons : la première est pragmatique, un terminal offre plus d'options pour interagir plus rapidement avec un ordinateur, à condition de connaître les commandes adéquates (ou de savoir les chercher) ; la seconde est la curiosité, les commandes d'un terminal permettent de comprendre le fonctionnement d'un ordinateur.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 1. Rappels utiles

- où je suis : `pwd`
- liste des fichiers : `ls`
- naviguer : `cd`, `cd document/mon-dossier` ou `cd ..`
- déplacer un fichier : `mv`, `mv document/mon-fichier.md document/mon-dossier/mon-fichier.md`
- supprimer (définitivement) un fichier : `rm`, `rm document/mon-dossier/mon-fichier.md`
- afficher le contenu d'un fichier : `cat`, `cat mon-fichier.md`
- créer un fichier : `touch`, `touch mon-nouveau-fichier.md`
- créer un dossier : `mkdir`, `mkdir document/mon-nouveau-dossier`

Bons réflexes :

- touche tabulation : autocomplétion
- flèche du haut : historique des commandes
- `CTRL + R` : rechercher dans l'historique

{{< pnote >}}
Ces commandes permettent d'utiliser facilement un terminal au quotidien, mais il y a bien plus de commandes possibles !
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Rappels utiles
{{< /pcache >}}
### Exercice

- créer un dossier `debugue`
- dans ce dossier, créer un fichier `mon-fichier.txt`
- créer un sous-dossier `fichiers` dans le dossier `debugue`
- déplacer `mon-fichier.txt` dans le dossier `fichiers`

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 2. Zsh
> The Z shell (Zsh) is a Unix shell that can be used as an interactive login shell and as a command interpreter for shell scripting.  
> [Source](https://en.wikipedia.org/wiki/Z_shell)

Zsh est un interpréteur de commandes (par défaut l'interpréteur de commandes est Bash sur les systèmes Unix) qui simplifie l'utilisation du terminal grâce à :

- autocomplétion des commandes (pour simplifier la vie)
- meilleure interface (informations affichées)
- meilleure interactivité

{{< pnote >}}
Zsh est un bonus par rapport à Bash, il améliore grandement l'interaction avec le terminal.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Zsh
{{< /pcache >}}
### Installation de Zsh
(vous aurez peut-être besoin d'autres programmes comme Git)

- Linux : `sudo apt install zsh` puis `chsh -s /usr/bin/zsh`, et redémarrer le terminal
- Mac :
    - installer homebrew : `/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`
    - installer Zsh : `brew install zsh zsh-completions` puis `chsh -s /bin/zsh`, et redémarrer le terminal
- Windows : l'installation est plus longue et pénible, exemple : [https://candid.technology/zsh-windows-10/](https://candid.technology/zsh-windows-10/)

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Zsh
{{< /pcache >}}
### Comment vérifier que l'installation s'est bien passée ?
Plusieurs possibilités, l'autocomplétion est un bon moyen de vérifier que tout s'est bien passé :

1. dans le dossier `debugue` que vous avez créé précédemment, taper la commande `touch mon-fichier-au-nom-complique-2022.txt`
2. taper à nouveau la même commande avec un moindre effort : `touch comp` puis la touche tabulation
3. l'autocomplétion vous permet de retrouver des commandes facilement !

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 3. Oh My Zsh
> Oh My Zsh is a delightful, open source, community-driven framework for managing your Zsh configuration. It comes bundled with thousands of helpful functions, helpers, plugins, themes, and a few things that make you shout...  
> [https://ohmyz.sh/](https://ohmyz.sh/)

Pour résumer : Zsh avec des options supplémentaires.

{{< pnote >}}
Loin d'être un gadget de plus, Oh My Zsh simplifie un peu plus l'interaction avec le terminal et permet d'installer facilement des thèmes.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Oh My Zsh
{{< /pcache >}}
### Installation
([https://ohmyz.sh/](https://ohmyz.sh/)) :

- `sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"`
- ou
- `sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"`

(Un script permet d'installer facilement Oh My Zsh, attention il faut avoir Git d'installé.)

Puis fermer et rouvrir le terminal.
{{< pnote >}}
Loin d'être un gadget de plus, Oh My Zsh simplifie un peu plus l'interaction avec le terminal et permet d'installer facilement des thèmes.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Oh My Zsh
{{< /pcache >}}
### Exercice
Modifier [le thème](https://github.com/ohmyzsh/ohmyzsh/wiki/Themes) à plusieurs reprises par défaut en suivant [la documentation](https://github.com/ohmyzsh/ohmyzsh/wiki/Settings#zsh_theme), pour cela il faut modifier le fichier `~/.zshrc`.

{{< psectiono >}}


{{< psectioni >}}
## 4. L'apparence compte (les couleurs)
Plusieurs possibilités existent (notamment en modifiant le profil dans les préférences du terminal), mais [Gogh](https://mayccoll.github.io/Gogh/) offre un moyen simple de choisir des palettes :

- prérequis :
    - `sudo apt-get install dconf-cli uuid-runtime`
    - créer un profil nommé "Default" depuis Preferences > Profiles > +
- Linux : `bash -c  "$(wget -qO- https://git.io/vQgMr)"`
- Mac : `bash -c  "$(curl -sLo- https://git.io/vQgMr)"`

Gogh propose alors de choisir un schéma de couleurs en fonction de [ceux existants](https://mayccoll.github.io/Gogh/), une fois le schéma ajouté il faut modifier les préférences du terminal pour le _voir_.

{{< pnote >}}
Ces manipulations qui peuvent paraître complexes permettent de modifier totalement l'aspect fu terminal.
D'autres solutions existent, mais celle-ci permet de continuer de travailler avec les outils par défaut du système, tout en personnalisant un certain nombre de paramètres.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. L'apparence compte (les couleurs)
{{< /pcache >}}
### Exercice
Ajouter plusieurs schémas de couleurs et modifier les préférences du terminal pour tester différents environnements.

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 5. Fonctions avancées : exemple de la recherche dans l'historique
Améliorons la fonction de recherche dans l'historique du terminal (`CTRL+R`) grâce à un _plugin_ Oh My Zsh :

- modifier le fichier `~/.zshrc` pour ajouter le plugin [zsh-navigation-tools](https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/zsh-navigation-tools), voici ce que vous devriez obtenir :  
`plugins=(git zsh-navigation-tools)`  
- fermer et rouvrir le terminal
- tester la fonction en faisant une recherche avec `CTRL+R`, un nouvel écran devrait s'afficher. Taper `texte` pour voir les résultats

{{< pnote >}}
Votre historique est encore très petit, mais dans quelques mois cette fonction de recherche vous sera très utile.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## Exercice pour la prochaine séance
Afin de mieux maîtriser votre terminal, effectuer chacune de ces actions via le terminal :

- création d'un nouveau dossier : `mkdir mon-dossier`
- création d'un nouveau fichier (plein texte) : `touch mon-fichier.xml`
- affichage du contenu d'un fichier (plein texte) : `cat mon-fichier.md`

{{< psectiono >}}