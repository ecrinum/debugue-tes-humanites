+++
title = "Séance 04 - Actions à répétition"
date = "2022-01-05"
date_p = "2022-02-25"
description = "Renommage de fichiers en lot et autres introductions à la programmation."
layout = "archives-diapositive"
visible = true
saison = "2"
+++
{{< psectioni >}}
{{< pcache >}}
1. Actions à répétition ?
2. Renommer des fichiers : quelques trucs et astuces
3. Trop courte introduction à la programmation
{{< /pcache >}}

{{< pnote >}}
Les situations où nous avons besoin de réaliser des actions à répétition sont très fréquentes : modifier l'extension de plusieurs dizaines de fichiers, ajouter un identifiant dans chaque nom, 

Les solutions existantes sont très nombreuses, nous n'allons pas ici tenter de toutes les exploiter mais plutôt d'entrevoir la panoplie d'outils existants.
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
## 1. Actions à répétition
L'équilibre entre à trouver entre :

- le temps requis pour effectuer les opérations manuellement
- et le temps que va demander de créer le script ou le programme pour réaliser les mêmes opérations automatiquement

{{< psectiono >}}


{{< psectioni >}}

{{< imageg src="automation.png" >}}

{{< pnote >}}
Passer plus de temps à programmer une tâche qui aurait pris moins de temps manuellement n'est pas du temps perdu : c'est l'occasion d'apprendre quelque chose.
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
## 2. Renommer des fichiers : quelques trucs et astuces

- terminal : `mv`
- dans Ubuntu : l'explorateur de fichier
- bash : un script type

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Renommer des fichiers : quelques trucs et astuces
{{< /pcache >}}
La commande `mv` :

- `mv mon-fichier.md mon-fichier.txt`
- `mv mon-dossier/* mon-nouveau-dossier/.`

{{< pnote >}}
La première commande renomme un seul fichier en changeant son extension, la seconde _déplace_ tous les fichiers d'un dossier à un autre.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Renommer des fichiers : quelques trucs et astuces
{{< /pcache >}}
Dans l'explorateur de fichiers d'Ubuntu (Nautilus) : clic droit et renommer sur tous les fichiers qui doivent être renommés, deux options :

- ajouter un élément dans le nom de tous les fichiers ;
- chercher et remplacer.

{{< imageg src="nautilus.png" >}}

{{< pnote >}}
Un exemple de renommage via une interface graphique sans avoir à installer de logiciel.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Renommer des fichiers : quelques trucs et astuces
{{< /pcache >}}
`mv` avec un script plus avancé :

`for f in *.md; do mv -- "$f" "${f%.md}.txt"; done`

Un autre script qui utilise `find` :

`find . -name "*.txt" -exec mv {} {}_brouillons \;`

{{< pnote >}}
La première commande permet de modifier l'extension des fichiers.

La seconde commande ajout `_brouillon` à tous les fichiers qui ont comme extension `.txt`.

Pour découvrir un programme plus puissant, voir `rename` : [https://www.computerhope.com/unix/rename.htm](https://www.computerhope.com/unix/rename.htm)
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Renommer des fichiers : quelques trucs et astuces
{{< /pcache >}}
Le programme `mmv` (voir [une présentation en ligne](https://dridk.me/mmv.html)) :

- logiciel en ligne de commande pour renommer facilement des fichiers à partir de _patterns_ (motifs)
- pour l'installer sous Ubuntu/Debian : `sudo apt install mmv`
- pour changer l'extension de fichiers de `.md` à `.txt` : `mmv "*.md" "#1.txt"`
- pour modifier une partie d'un fichier (remplacer `fichier` par `texte` dans tous les noms de mes fichiers) : `mmv *fichier* #1texte#2`

{{< pnote >}}
Un exemple de renommage via une interface graphique sans avoir à installer de logiciel.
{{< /pnote >}}
{{< psectiono >}}



{{< psectioni >}}
## 3. Trop courte introduction à la programmation


{{< pnote >}}
Voir quelques lignes de code de Marcello pendant la séance (vidéo ci-dessus).
{{< /pnote >}}
{{< psectiono >}}

