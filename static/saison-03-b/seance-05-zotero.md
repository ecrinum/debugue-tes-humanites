+++
title = "Séance 05 - Zotero"
date = "2021-09-10"
date_p = "2023-04-25"
description = "Usages avancés de Zotero."
layout = "diapositive"
visible = true
saison = "3b"
+++

{{< psectioni >}}
# Débugue mon Zotero


Pascal Martinolli   

Bibliothécaire et zoteromane depuis 2011   

Bibliothèque des lettres et sciences humaines, UdeM    

pascal.martinolli@umontreal.ca   

{{< psectiono >}}

{{< psectioni >}}

## Quoi ?

- [Logiciel libre](https://www.zotero.org/), gratuit et open-source
	- Linux, PC, Mac
	- Dialogue avec LibreOffice et MS Word 
- Plusieurs formes :
	- Fonctionnalités complètes : Logiciel installé 
	- Fonctionnalités limités : 
		- Site web 
		- Version light : [ZoteroBib](https://zbib.org/) 
		- Google Doc,... 


{{< psectiono >}}


{{< psectioni >}}

## Pourquoi ?

- Gestion bibliographique :
	- Rédige automatiquement des bibliographie et des références dans le texte
	- Gère des collections de références (individuel ou collaboratif)

{{< psectiono >}}



{{< psectioni >}}

## Qui ? 
- [Corporation for Digital Scholarship](https://digitalscholar.org/)
- International, multilingue
- Grosse communauté d'utilisateurs et de développeur : 
	- Bientôt : version 7 (plus rapide)
	- Extensions 
	- Support UdeM 
	- Blogue [Zotero francophone](https://zotero.hypotheses.org/) + [Doc-fr](https://docs.zotero-fr.org/) 

{{< psectiono >}}


{{< psectioni >}}

## Comment ? Bibliographie

3 mouvements : 
- Créer une référence dans le logiciel (méthodes multiples)
- Vérifier et enrichir la référence créée
- Citer la référence
	- Méthode simple (de type copier-coller)
	- Méthode avancée (lien dynamique entre le traitement de texte et Zotero)

{{< psectiono >}}



{{< psectioni >}}

## Comment ? Gestion des références

- [Organisation](https://zotero.hypotheses.org/3298) :
	- Collections
	- Marqueurs
		- Mon [thésaurus personnel](https://github.com/pmartinolli/TM-MyThesaurus)
	- Recherches enregistrées
	- Doublons
	- Connexe
- Prise de notes :
	- Notes filles
	- Extraction de notes de PDF
	- Notes indépendantes


{{< psectiono >}}


{{< psectioni >}}

## Comment ? Ouverture 

- Collaboration
	- Bibliothèques de groupe 
- Partage et diffusion
	- Via Zotero
	- Via [page web fixe](https://pmartinolli.github.io/academicTTRPG/) + javascript ([GH](https://github.com/pmartinolli/academicTTRPG)) 
	- Via site web dynamique (application [Kerko](https://github.com/whiskyechobravo/kerko) par ex.) 
- Formats ouverts 

{{< psectiono >}}


{{< psectioni >}}

## Comment ? Bonus avancés 

- Extensions : [Zutilo](https://github.com/wshanks/Zutilo), [ZotFile](http://zotfile.com/), [ZoteroPreview](https://github.com/dcartertod/zotero-plugins)
- Intégrité : Retraction Watch (inclus), [ZPubPeer](https://github.com/PubPeerFoundation/pubpeer_zotero_plugin), [ZScite](https://github.com/scitedotai/scite-zotero-plugin),
- Modifier un style bibliographique [CSLeditor](https://editor.citationstyles.org/visualEditor/)
- Multiremplacements 
- [Cita](https://github.com/diegodlh/zotero-cita) et Wikidata
- Et vous ? Vos extensions favorites ? 

{{< psectiono >}}