+++
title = "Séance 01 - Comprendre l'ordinateur avec le terminal"
date = "2021-01-03"
date_p = "2022-12-06"
description = "Où sont les fichiers sur un ordinateur. Le terminal."
layout = "diapositive"
visible = true
saison = "3b"
+++

{{< psectioni >}}
## Bienvenue !

1. Tour d'écrans
2. Déroulement de la formation
3. Origines de l'informatique
4. Principes du numérique
5. Qu'est-ce qu'un programme/logiciel ?
6. Usages basiques d'un terminal

{{< pnote >}}
Pour cette première séance nous vous proposons de découvrir les enjeux de cette formation ainsi que les différentes séances au programme.
{{< /pnote >}}

{{< psectiono >}}

{{< psectioni >}}
## 1. Tour d'écrans
3 questions :

1. votre nom
2. ce que vous faites cette année
3. l'objet _numérique_ le plus proche de vous (autre que votre ordinateur)

{{< psectiono >}}

{{< psectioni >}}
## 2. Déroulement de la formation

- des séances théoriques et pratiques
- des échanges
- vous pouvez amener vos projets
- des supports et de la documentation en ligne

{{< pnote >}}

{{< /pnote >}}

{{< psectiono >}}

{{< psectioni >}}

## 3. Origines de l'informatique

### Des calculateurs analogiques aux machines programmables

- première machine à calculer : boulier (antiquité) ;
- ordinateur : capacité de faire des calculs _sans_ intervention humaine ;
- 1936 : basculement dans l'histoire de l'informatique ;
- machine de turing.

{{< pnote >}}
Si l'informatique est une science récente, et un ensemble de technologies développées au 20e siècle, il faut garder à l'esprit que l'informatique née dès l'antiquité, soit 4000 ans avant notre ère.
Le père de l'ordinateur c'est la machine à calculer : un projet qui démarre pendant l'antiquité avec le boulier, et qui se concrétise plus tard avec les inventions de Pascal ou de Leibniz.
La grande différence entre ces prémisses et l'ordinateur réside dans l'automatisme : l'ordinateur peut faire des calculs sans intervention humaine (il y a une absence de mécanique).

La date de 1936 est un basculement dans l'histoire de l'informatique : Alan Turing publie un article fondateur sur la calculabilité, qui résout un problème fondamental de logique, qui passera à l'époque inaperçu auprès de celles et ceux qui travaillent sur les machines à calculer.
En 1936 c'est aussi une époque où les états se réarment, et beaucoup d'efforts sont mis sur la cryptographie pour sécuriser les moyens de communication, d'où ce besoin de calculateurs.

[La machine de Turing](https://fr.wikipedia.org/wiki/Machine_de_Turing) est une machine conceptuelle, très basique.
Elle n'existe pas en tant que telle, il s'agit simplement un modèle pour penser le principe de l'informatique.
Il n'y a par exemple pas de différence entre un ordinateur d'aujourd'hui et une machine de Turing.
On parle alors de "machine universelle", car elle traite l'information de façon simple.
La machine de Turing permet de faire n'importe quel calcul, elle traite l'information de façon universelle.
Alan Turing est un personnage emblématique dans l'histoire de l'informatique (et plus globalement dans l'histoire des sciences et des techniques).
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 3. Origines de l'informatique
{{< /pcache >}}
### Qu'est-ce qu'un ordinateur ?

- ceci n'est pas une boîte noire ;
- distinguer le matériel (_hardware_) du logiciel (_software_) ;
- support + message.

{{< pnote >}}
Tout d'abord il faut écarter l'idée que l'ordinateur serait une boîte noire, ou une machine dont le comportement serait aussi incompréhensible qu'imprévisible.
L'informatique s'est fortement complexifiée depuis une trentaine d'années, sans parler du fait que la plupart des terminaux sont désormais connectés à Internet, mais ce n'est pas pour cela qu'il faut considérer un ordinateur comme une chose mystérieuse.

Cela ne veut pas pour autant dire que je pourrais vous expliquer simplement comment fonctionne un ordinateur, mais déjà les distinctions que Michel Serres vous a présenté sont utiles :

- il s'agit de la composition de deux éléments (hardware et software)
- et de l'association d'un support et d'un message (pour le dire vite).

{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
## 4. Principes du numérique

{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 4. Principes du numérique
{{< /pcache >}}
### Le numérique au sens propre du terme
Représentation de la réalité via des éléments discrets et atomiques qui correspondent à des nombres naturels.

S’oppose à analogique: représentation du réel via un signal continu, "analogue" au réel.
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 4. Principes du numérique
### Le numérique au sens propre du terme
{{< /pcache >}}
![Numérique et analogique](http://www.bedwani.ch/electro/ch23/image88.gif)
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 4. Principes du numérique
{{< /pcache >}}
### La modélisation du monde
Trois étapes:

1. modèle représentationnel
   - donner une description en langage naturel de quelque chose
2. modèle fonctionnel
   - transformer la représentation en unités atomiques discrètes et définir des fonctions pour les traiter   
    Le "_numérique_" se situe ici!
3. modèle physique
   - implémenter le calcul fonctionnel dans une machine de Turing réelle.
   - calculable = computable

{{< pnote >}}
Les trois étapes ne sont pas étanches!
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 4. Principes du numérique
{{< /pcache >}}
### La base 2

Pour pouvoir implémenter l'approche numérique dans une machine avec **2** symboles disponibles (plein/vide, noir/blanc, +/-...).

|Base 10|Base 2|
|---------|---------|
|0|0|
|1|1|
|2|10|
|3|11|
|4|100|
|5|101|
|6|110|
|7|111|

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Principes du numérique
### La base 2
{{< /pcache >}}
|Base 10|Base 2|
|---------|---------|
|8|1000|
|9|1001|
|10|1010|
|11|1011|
|12|1100|
|13|1101|
|14|1110|
|15|1111|

{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 4. Principes du numérique
### La base 2
{{< /pcache >}}

- 11 en base 10 signifie: 1 dizaine et 1 unité (10+1).
- 11 en base 2 signifie: 1 couple et une unité ( et donc en base 10: 2+1=3)
- en base 10 avec 4 chiffres je peux exprimer: 10<sup>4</sup> = 10x10x10x10 = 10000 chifres (en effet de 0 à 9999)
- en base 2 avec 4 chiffres je peux exprimer 2<sup>4</sup> = 2x2x2x2=16
- en base 2 avec 8 chiffres je peux exprimer 2<sup>8</sup> = 256 (un octet)
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Principes du numérique
{{< /pcache >}}
### Les algorithmes
Ensemble d’instructions qui respectent deux conditions:

- à chaque instruction il est possible de connaître l’instruction suivante
- si on suit les étapes on arrive à une instruction qui demande l’arrêt
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Principes du numérique
{{< /pcache >}}
### La machine de Turing

[Jouez avec une machine de Turing virtuelle](https://interstices.info/comment-fonctionne-une-machine-de-turing/)
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 5. Qu'est-ce qu'un programme/logiciel ?
{{< /pcache >}}
- programme informatique : suite d'instructions qu'exécute un ordinateur
- logiciel : ensemble de programmes informatiques + interfaces
- les conditions d'utilisation
{{< psectiono >}}

{{< psectioni >}}
## 6. Usages basiques d'un terminal
### 6.1. Qu'est-ce qu'un terminal&nbsp;?
> Un terminal est un programme qui émule une console dans une interface graphique, il permet de lancer des commandes.  
> ([Source](https://doc.ubuntu-fr.org/terminal))

{{< pnote >}}
S'il faut retenir une chose : un terminal est un autre moyen d'interagir avec un ordinateur.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 6. Usages basiques d'un terminal
{{< /pcache >}}
### 6.2. Ouvrir un terminal
Sous Linux ou Mac, chercher "terminal".

Sous Windows, activer le terminal/bash en suivent [ces instructions](https://korben.info/installer-shell-bash-linux-windows-10.html) ou [celles-ci](https://blog.ineat-group.com/2020/02/utiliser-le-terminal-bash-natif-dans-windows-10/).

{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 6. Usages basiques d'un terminal
{{< /pcache >}}
### 6.3. Savoir où l'on est
`pwd`

```
pwd

/home/machine
```

{{< pnote >}}
À noter ici qu'il s'agit d'un **chemin absolu** puisque l'adresse/chemin indiquée commence par une barre oblique `/`.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 6. Usages basiques d'un terminal
{{< /pcache >}}
### 6.4. Lister les fichiers
`ls`

ou

`ls -a` pour voir aussi les fichiers cachés.

{{< pnote >}}
Pourquoi vouloir voir les fichiers cachés&nbsp;?
Ce sera utile pour la suite.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 6. Usages basiques d'un terminal
{{< /pcache >}}
### 6.5. Naviguer
`cd` suivi du chemin. Exemple :

`cd Document/Photos`

mène au dossier `Photos`.

{{< pnote >}}
Il est possible d'indiquer à la fois un chemin relatif, donc sans commencer par une barre oblique, ou en commençant par une barre oblique pour un chemin absolu.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 6. Usages basiques d'un terminal
{{< /pcache >}}
### 6.6. Les bons réflexes

- touche `tabulation` pour compléter une commande
- flèche du haut `↑` pour parcourir les dernières commandes utilisées
- `CTRL + R` pour recherche une commande en tapant les premières lettres (faites à nouveau `CTRL + R` autant de fois que nécessaire pour parcourir l'historique à partir des lettres que vous avez tapées)

{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 6. Usages basiques d'un terminal
{{< /pcache >}}
### 6.7. Déplacer un fichier
`mv` permet de renommer ou de déplacer un fichier, deux exemples :

- `mv mon-fichier.md fichier.md` renomme le fichier `mon-fichier.md` en `fichier.md`
- `mv /home/machine/mon-fichier.md /home/machine/Documents/mon-fichier.md` déplace le fichier `mon-fichier.md` dans le dossier `Documents`

{{< pnote >}}
Attention, pour cette dernière commande il est nécessaire que le dossier `Documents` existe déjà.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 6. Usages basiques d'un terminal
{{< /pcache >}}
### 6.8. Supprimer un fichier
`rm mon-fichier.md`

`rm -R mon-dossier` supprime le dossier et tout ce que contient ce dossier.

{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 6. Usages basiques d'un terminal
{{< /pcache >}}
### 6.9. Afficher le contenu d'un fichier
`cat mon-fichier.md`

{{< psectiono >}}