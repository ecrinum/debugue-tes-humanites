+++
title = "Séance 06 - Introduction à l'éditeur de texte Stylo"
date = "2021-09-10"
date_p = "2023-05-16"
description = "Un éditeur de texte pour les sciences humaines."
layout = "diapositive"
visible = true
saison = "3b"
+++

{{< psectioni >}}
## Plan de la séance
1. Stylo
2. Création d'un compte
3. Tour de plateforme
4. Gestion des articles
5. Gestion des contenus
6. Gestion de la bibliographie
7. BibTeX
8. Better BibTeX
9. YAML
10. Prévisualisation et annotation
11. Module d'export
12. GrapQL Playground
{{< psectiono >}}


{{< psectioni >}}
## 1. Stylo
- Éditeur de texte WYSIWYM
- Séparation entre mise forme et sens du texte
- Gestion du balisage dans le mains du chercheur·e·s
- Métadonnées enrichies (YAML, BibTeX)
- Single source publishing (PDF, HTML, XML...)
- API GraphQl et Pandoc

{{< psectiono >}}

{{< psectioni >}}
## 2. Création d'un compte
- Ma page utilisateur
{{< psectiono >}}

{{< psectioni >}}
## 3. Tour de plateforme
- Gestion des articles et des tags
- Éditeur de texte ([Monaco](https://microsoft.github.io/monaco-editor/)) avec **MarkDown** 
- Gestion des bibliographies avec **BibTeX**
- Gestion des métadonnées avec **YAML**
- Prévisualisation et annotation avec **Hypothes.is** : publique ou [dans un groupe privé] (https://hypothes.is/groups/B2j3obmw/debugue)
- Export avec **Pandoc**
{{< psectiono >}}

{{< psectioni >}}
## 4. Gestion des articles
- Créer un nouvel article
- Gérer les étiquettes
- Supprimer un article

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Gestion des articles
{{< /pcache >}}
- Copier la dernière version d'un article
- Partager un article avec toutes ses versions
- Exporter un article
- Prévisualisation d'un article

{{< psectiono >}}


{{< psectioni >}}
## 5. Gestion des contenus
- La syntaxe MarkDown
    - Titres
    - Notes
    - Italiques/gras
    - Images
    - Liens
    - Tableaux
    - Class
    {{< pnote >}}

    ``` 
    [texte]{.class}

    ```
    ```
    :::{.class}
    texte
    :::
    ``` 
    Équivalent HTML : 
    - Premier cas : `<span class="class">`
    - Deuxième cas : `<div class="class">`

    Seulement pour l'export PDF : `\epigraph texte`

    {{< /pnote >}}
    - Espace insécable : `Ctrl`+`Shift`+`Espace` ou `&nbsp;` en ASCII <!--American Standard Code for Information Interchange : format d'encodage des caractères -->
- Versions
    - Comparaison entre différentes versions
{{< psectiono >}}

{{< psectioni >}}
## 5. Gestion des contenus
### Modifier des parties de texte
- `CTRL/Cmd+F`
- `CTR//Cmd+H` : 
- Expressions régulières


{{< pnote >}}
#### RegEx -- Exemples

Voir [ICI](https://medium.com/factory-mind/regex-tutorial-a-simple-cheatsheet-by-examples-649dc1c3f285) et [ICI](https://medium.com/factory-mind/regex-cookbook-most-wanted-regex-aa721558c3c1) pour plus d'exemples

Et [ICI](https://regex101.com/) pour expérimenter les expressions rationnelles

```
^The : correspond à toute chaîne de caractères commençant par The
end$ : correspond à une chaîne qui se termine par end
^The end$ : correspondance exacte de la chaîne (commence et se termine par The end)
the : matches any string that has the text end in it

\*\*(?=) : pour trouver des mots en gras

abc*
abc+
abc{2}
a(b|\s) 

/(^|\s)(the\s.*?\.)/g : pur mettre en gras toutes les phrases commençant par The


``` 
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
## 5. Gestion des contenus
### Exercice --Docx vers MarkDown
- Convertir un texte docx en Markdown via Pandoc : `pandoc -f docx -t markdown "word_filename.docx" -o "markdown_filename.md"`
- Corriger les balises
Dans la version précédente, cette conversion était intégrée dans Stylo : Voir [ICI](https://stylo-export.ecrituresnumeriques.ca/importdocx.html)
- Une alternative temporaire pour comprendre les commandes Pandoc : [ICI](https://pandoc.org/try/)
{{< psectiono >}}

{{< psectioni >}}
## 6. Gestion de la bibliographie
- Manage Bibliography
- Import a collection from my account
- Raw BibTeX
- Citations dans le texte + autocomplétion
     - [@clef]
     - [-@clef]
     - @clef

{{< psectiono >}}


{{< psectioni >}}
## 7. BibTeX
BibTeX est un format de fichier structurant une bibliographie :

- format qui a été créé en 1985 pour gérer les bases de données bibliographiques dans des fichier LaTeX ;
- le format BibTeX fonctionne par entrée bibliographiques : chaque entrée correspond à une description d’un document avec des items “mot-clef = valeur”.
{{< psectiono >}}


{{< psectioni >}}
```
@book{kirschenbaum_track_2016,
  address = {{Cambridge, Massachusetts, Etats-Unis d'Am{\'e}rique}},
  title = {Track Changes: A Literary History of Word Processing},
  isbn = {978-0-674-41707-6},
  shorttitle = {Track Changes},
  language = {anglais},
  publisher = {{The Belknap Press of Harvard University Press, 2016}},
  author = {Kirschenbaum, Matthew G.},
  year = {2016, cop. 2016}
}
```
{{< psectiono >}}


{{< psectioni >}}
## 8. Better BibTeX
### Installation de Better BibTex
Better BibTex (https://retorque.re/zotero-better-bibtex/installation/) ajoute à Zotero des fonctionnalités très pratiques :

- affichage des clés de citation ;
- nouveaux formats d'export ;
- exports dynamiques (mises à jour automatiques) de bibliographies.
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 8. Better BibTeX
{{< /pcache >}}
### Installez Better BibTeX
[https://retorque.re/zotero-better-bibtex/installation/](https://retorque.re/zotero-better-bibtex/installation/)
{{< psectiono >}}


{{< psectioni >}}
## 9. YAML
[YAML](https://yaml.org/)
- Un langage de sérialisation de données facilement lisible.
- Fonctionne avec d'autres langages de programmation
- vs. [JSON](https://www.json.org/json-en.html)
{{< psectiono >}}


{{< psectioni >}}
## 10. Prévisualisation et annotation
- Hypothesis
- Création d'un compte
- Annotations publiques ou dans un [groupe privé](https://hypothes.is/groups/B2j3obmw/debugue) 
{{< psectiono >}}


{{< psectioni >}}
## 11. Module d'export
 https://export.stylo.huma-num.fr/
 Ancien :  https://stylo-export.ecrituresnumeriques.ca/ 
{{< psectiono >}}

{{< psectioni >}}
## 12.GrapQL Playground
- [Download de l'application](https://github.com/graphql/graphql-playground/releases)
- Observation de la plateforme et premières requêtes
- HTTP HEADER
{{< pnote >}}

``` 
{
    "Authorization": "Bearer  API Key.eyJlbWFpbCI6ImdpdWxpYS5mZXJyZXR0aTE5OTVAZ21haWwuY29tIiwiX2lkIjoiNjA1NDg3YjkxZDA2MDAwMDEyMTY3YmQ3IiwiYXV0aFR5cGUiOiJsb2NhbCIsImFkbWluIjpmYWxzZSwic2Vzc2lvbiI6dHJ1ZSwiaWF0IjoxNjc5NDA1MTk2fQ.xmeCU60hVnQG679jdf5_DQ4ssd6UyZfEjw8P11PgLb0"
}
``` 

{{< /pnote >}}

{{< pnote >}}
### Exemples
```
query tousMesArticles {
  user {
    _id
    email

    articles {
      _id
      title
    }
  }
}
```

```
query stats {
  stats {
    users {
      total
        local
      openid
      years {year count}
    }
    
    articles {
      total
      years {year count}
    }
  }
}
``` 

``` 
query singleArticle ($article: ID!) {
  article (article: $article) {
    _id
    title
    
    rename (title: "How Not to Stylo")
  }
}
```

QUERY VARIABLES : 
```
{"article":"ID"}
``` 
{{< /pnote >}}

{{< psectiono >}}