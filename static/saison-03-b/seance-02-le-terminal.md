+++
title = "Séance 02 - Gérer les fichiers avec le terminal"
date = "2021-09-10"
date_p = "2023-01-10"
description = "Le terminal. Commandes basiques et avancées."
layout = "diapositive"
visible = true
saison = "3b"
+++
{{< psectioni >}}
## Plan de la séance

1. Rappel
2. Commandes basiques du terminal : rappels
3. Commandes avancées
5. Bash
6. Zsh
{{< psectiono >}}

{{< psectioni >}}
## 1. Rappel
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 1. Rappel
{{< /pcache >}}
## 1.1. Définition : le terminal
Un terminal est une interface graphique (graphical user interface - GUI) qui émule une console. Il nous permet d'exécuter un shell.
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Rappel
{{< /pcache >}}
## 1.2. Définition : le shell
Le shell est un interpréteur de commandes. Il s'agit d'un programme qui transmet les commandes entrées par l'utilisateur au système d'exploitation pour qu'il les exécute.   

Des exemples de programmes shell sont `bash` (qui est aussi un langage de commande) et `Zsh` (Z shell).
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 1. Rappel
{{< /pcache >}}
## 1.3. L'intérêt
Les autres interfaces graphiques nous offrent une série d'options et nous orientent vers un parcours d'action. Grâce au terminal, nous pouvons construire nos propres commandes, adaptées à nos besoins.   
En apprenant à bien utiliser le terminal, nous pouvons exécuter des commandes complexes très rapidement.
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Rappel
{{< /pcache >}}
## 1.4. Ouvrir un terminal
Sous Linux ou Mac, chercher « terminal »    

Windows : vous pouvez suivre les instructions [ici](https://blog.ineat-group.com/2020/02/utiliser-le-terminal-bash-natif-dans-windows-10/)    

{{< psectiono >}}


{{< psectioni >}}
## 2. Commandes basiques du terminal : rappels

- où je suis : `pwd`
- liste des fichiers : `ls`
- naviguer : `cd`
- déplacer un fichier : `mv`
- supprimer un fichier : `rm`
- supprimer le dossier et tout ce que contient le dossier : `rm -R`
- afficher le contenu d'un fichier : `cat`
- créer un fichier : `touch`
- créer un dossier : `mkdir`

Bons réflexes :

- tabulation : autocomplétion
- flèche du haut : historique des commandes
- `CTRL + R` : rechercher dans l'historique

{{< pnote >}}
Pourquoi chercher à comprendre où sont les fichiers sur un ordinateur&nbsp;?
Parce que cela permet ensuite d'interagir plus facilement avec les différents fichiers qui constituent par exemple un projet d'édition ou d'écriture numérique.
Prendre conscience de l'organisation de ces fichiers est nécessaire notamment pour lier des fichiers entre eux, donc pour faire des liens relatifs ou absolus entre deux entités numériques.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## Exercice

1. créez un dossier `livre` contenant un sous-dossier `chapitre-01` contenant lui-même un fichier `texte.txt`
2. naviguez dans le dossier `chapitre-01`
3. revenez dans le dossier `livre`
4. déplacez le fichier `texte.txt` dans le dossier `livre`
5. renommez le fichier `texte.txt` en `chapitre-01.txt`
6. supprimez le dossier `chapitre-01`

{{< psectiono >}}


{{< psectioni >}}
## 3. Commandes avancées
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Commandes avancées
{{< /pcache >}}
### 3.1 Afficher récursivement la structure hiérarchique du contenu d'un répertoire
`tree`
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 3. Commandes avancées
{{< /pcache >}}
### 3.2. Afficher le manuel
`man`

Exemple : `man ls` pour tout savoir sur la commande `ls`
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Commandes avancées
{{< /pcache >}}
### 3.3. Supprimer les commandes affichées
`clear`

{{< pnote >}}
Pratique si vous voulez repartir sur un terminal _neuf_.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Commandes avancées
{{< /pcache >}}
### 3.4. Afficher l'historique

`history`

{{< pnote >}}
Pour afficher et parcourir l'historique des commandes que vous avez utilisées (seulement les commandes, pas les résultats de ces commandes).
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Commandes avancées
{{< /pcache >}}
### 3.5. Afficher le contenu d'un fichier page à page
`less`

Par exemple `less mon-texte.txt`

{{< pnote >}}
C'est une commande similaire à `cat`, mais plus lisible dans le résultat.

Pour quitter : `q`.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Commandes avancées
{{< /pcache >}}
### 3.6. Copier un fichier/dossier
`cp`

Par exemple `cp livre/texte.txt sauvegarde/divers/livre/texte.txt`

Ou encore `cp -r livre sauvegarde/divers`
{{< pnote >}}
Cette commande copie le dossier `livre` et son contenu (sous-dossiers, fichiers) dans le dossier `sauvegarde/divers`.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Commandes avancées
{{< /pcache >}}
### 3.7. Compter les mots
`wc`

Par exemple `wc texte.txt`
{{< pnote >}}
Cela permet de compter le nombre de lignes, de mots et de caractères dans un fichier texte.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Commandes avancées
{{< /pcache >}}
### 3.8. Chercher
`find`    
Par exemple `find -name "name-fichier.txt"` OU `find -name "*.txt"`

{{< pnote >}}
Cette commande permet de chercher dans les noms de fichiers/dossiers, et dans les fichiers eux-mêmes.
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 3. Commandes avancées
{{< /pcache >}}
### 3.9. Trouver les différences
`diff`   
Par exemple `diff -y -W 120 file1.txt file2.txt`
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 3. Commandes avancées
{{< /pcache >}}
### 3.11. Trouver des mots
`grep` 
Par exemple `grep "string" file3.txt` ou `grep "string" *.txt`   

{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 3. Commandes avancées
{{< /pcache >}}
### 3.12. Enchaîner des commandes
`|`   
Par exemple : `less file3.txt | grep "string"`
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 3. Commandes avancées
{{< /pcache >}}
### 3.13. Rendre un fichier exécutable
`chmod +x nom-fichier` 
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 3. Commandes avancées
{{< /pcache >}}
### 3.14. Afficher les lignes les chaînes de caractères qui sont passées comme arguments
`echo` 
{{< pnote >}}
Utilisé dans les scripts
{{< /pnote >}}
{{< psectiono >}}



{{< psectioni >}}
### 3.13. D'autres commandes
`rsync` pour synchroniser des dossiers (program complexes mais très pratique).

`sed` pour manipuler du texte, commande puissante.

`&&` pour enchaîner les commandes.

`htop` pour savoir ce que fait votre ordinateur.
{{< psectiono >}}


{{< psectioni >}}
## 4. Bash
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Bash
{{< /pcache >}}
## 4.1. Exemple 1
Préparer une suite de commandes dans un fichier.

```
#!/bin/bash

# aller dans le dossier
cd notes
# créer un fichier qui va s'appeler note- suivi du numéro de la note que vous aurez indiqué dans le terminal
touch note-$1.txt

```
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Bash
{{< /pcache >}}
## 4.2. Exemple 2

```
#!/bin/bash

# créer un dossier
mkdir dossier
# créer un fichier dans le dossier
touch "dossier/note2.txt"
# écrire "Hello World dans le fichier"
echo "Hello World" >> "dossier/note2.txt"
# afficher un message dans le terminal
echo "Hello World a été écrit dans dossier/note2.txt"

```
{{< psectiono >}}



{{< psectioni >}}
{{< pcache >}}
## 4. Bash
{{< /pcache >}}
## 4.3. Exemple 3
Pour savoir si un fichier existe ou non sur votre ordinateur

```
#!/bin/bash

# créer la variable FICHIER
FICHIER="note3.txt"
# si le fichier existe, afficher "le nom-du-fichier existe". 
if [ -e "$FICHIER" ]
then
   echo "le $FICHIER existe"
# si le fichiers n'existe pas, afficher "le nom-du-fichier n'existe pas".
else
   echo "le $FICHIER n'existe pas"
fi

```
[Ressource](https://gist.github.com/bradtraversy/ac3b1136fc7d739a788ad1e42a78b610)
{{< psectiono >}}


{{< psectioni >}}
## 5. Zsh
> The Z shell (Zsh) is a Unix shell that can be used as an interactive login shell and as a command interpreter for shell scripting.  
> [Source](https://en.wikipedia.org/wiki/Z_shell)

Zsh est un interpréteur de commandes (par défaut l'interpréteur de commandes est Bash sur les systèmes Unix) qui simplifie l'utilisation du terminal grâce à :

- autocomplétion des commandes (pour simplifier la vie)
- meilleure interface (informations affichées)
- meilleure interactivité

{{< pnote >}}
Zsh est un bonus par rapport à Bash, il améliore grandement l'interaction avec le terminal.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 5. Zsh
{{< /pcache >}}
### Installation de Zsh
(vous aurez peut-être besoin d'autres programmes comme Git)

- Linux : `sudo apt install zsh` puis `chsh -s /usr/bin/zsh`, et redémarrer le terminal
- Mac :
    - installer homebrew : `/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`
    - installer Zsh : `brew install zsh zsh-completions` puis `chsh -s /bin/zsh`, et redémarrer le terminal
- Windows : l'installation est plus longue et pénible, exemple : [https://candid.technology/zsh-windows-10/](https://candid.technology/zsh-windows-10/)

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 6. Zsh
{{< /pcache >}}
### Comment vérifier que l'installation s'est bien passée ?
Plusieurs possibilités, l'autocomplétion est un bon moyen de vérifier que tout s'est bien passé :

1. dans le dossier `debugue` que vous avez créé précédemment, taper la commande `touch mon-fichier-au-nom-complique-2022.txt`
2. taper à nouveau la même commande avec un moindre effort : `touch comp` puis la touche tabulation
3. l'autocomplétion vous permet de retrouver des commandes facilement !

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}
