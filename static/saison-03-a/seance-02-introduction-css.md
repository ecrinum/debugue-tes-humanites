+++
title = "Séance 02 - Introduction au CSS"
date = "2021-09-10"
date_p = "2023-03-14"
description = "Pour mettre en forme les documents au format HTML."
layout = "diapositive"
visible = true
saison = "3a"
+++
{{< psectioni >}}

## Plan de la séance

1. Rappel : HTML
2. Introduction à CSS (historique et concepts clés)
3. Atelier : création d'une page Web en CSS et en HTML
{{< psectiono >}}


{{< psectioni >}}

## 1. Rappel : HTML -- baliser le texte

- pourquoi des balises ?
- quelle version de HTML ?
- un format pérenne ?

{{< pnote >}}
Les balises HTML permettent de **structurer** l'information pour qu'elle puisse être interprétée par un navigateur web.
C'est un langage de balisage _non léger_, contrairement à [Markdown](https://en.wikipedia.org/wiki/Markdown#Examples).

La version _actuelle_ de HTML est [HTML5](https://www.tutorialspoint.com/html5/html5_new_tags.htm), est moins _document-centric_ que les précédentes : il contient de nouveaux éléments de structuration (`section`, `article`) et multimédias (`canvas`, `audio`, `video`), ainsi que de nouveaux éléments de gestion d'événements.
{{< /pnote >}}

{{< psectiono >}}


{{< psectioni >}}

## 2. Rappel : La structure d'un document HTML

```html
<!DOCTYPE html>
<html lang="fr">
  <head>
	  <title>Ma page</title>
	  <link rel="icon" href= "favicon.ico" />
	</head>
	<body>
	  <p>Du texte.</p>
	</body>
</html>
```

{{< pnote >}}
Il y a quelques règles à respecter pour écrire en HTML :

- les balises sont déterminées pas le standard, mais la validation n'est pas si stricte que pour XML par example ;
- les balises sont imbriquées, elles ne doivent se chevaucher, exemple de ce qu'il ne faut pas faire : `<p><em>du texte</p></em>`.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}

## 3. HTML : Exercise

- ouvrez un éditeur de texte (VSCode, Codium, Atom, Vim, etc.)
- créez un fichier ma-page.html
- renseignez un titre avec une balise `<h1>`
- ajoutez quelques renseignements sur cette page dans une balise `<details>`
- écrivez quelques lignes avec la balise `<p>`
- enregistrez votre fichier puis ouvrez-le avec votre navigateur favori

{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}

## 4. CSS

- 1994 : Håkon Wium Lie, au CERN, commence à travailler sur le CSS.
- 1996 : Le W3C publie la première norme pour CSS
- Cascading Style Sheets
- 1999 CSS3 : couleurs <!--extension de la liste des couleurs et des paramètres dispon-->, RGBA, HSLA <!--hue, saturation, lightness, transparency-->, coins arrondis (`border-radius:`), ombres (hauteur, largeur, flou), [dégradés](https://medium.com/beginners-guide-to-mobile-web-development/whats-new-in-css-3-dcd7fa6122e1https://medium.com/beginners-guide-to-mobile-web-development/whats-new-in-css-3-dcd7fa6122e1), [images d'arrière-plan multiples](https://www.w3schools.com/css/tryit.asp?filename=trycss3_background_multiple)
- *cascading* : Indique la priorité avec laquelle les informations sont traitées <!--1. CSS prend le dernier style défini pour un élément. 2. CSS prend le sélecteur le plus spécifique : id, class, element. 3. priorité au style en ligne -->
{{< pnote >}}
[Source](https://www.w3.org/Style/CSS20/history.html)
{{< /pnote >}}
{{< psectiono >}}



{{< psectioni >}}

## 5. CSS : Historique

- La séparation de la structure du document de sa mise en page était un objectif du HTML depuis sa création en [1990](http://info.cern.ch/hypertext/WWW/TheProject.html) par Team Berners Lee
- Il existait déjà des technologies permettant de réguler le style d'une page : Word a été lancé en 1983, TeX en 1979
- Selon l'idée originale, chaque navigateur aurait dû produire sa propre feuille de style
- Encouragé par Dave Ragget (le principal architecte de HTML3.0), Håkon a publié la première version de CSS 3 jours avant la présentation de Netscape Navigator
- 1995 : création de la liste de diffusion [www-style](https://lists.w3.org/Archives/Public/www-style/) et premier Workshop dédié à CSS
- 1996 : Internet Explorer et Netscape en concurrence pour prendre en charge CSS
{{< pnote >}}
[Source](https://www.w3.org/Style/CSS20/history.html)
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 5. CSS : Historique
{{< /pcache >}}
### Points de discussion après première présentation 

- Équilibre entre les préférences de l'auteur et celles de l'utilisateur
- CSS n'est pas un langage de programmation complet
{{< pnote >}}
[Source](https://www.w3.org/Style/CSS20/history.html)
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}

## 6. CSS : mettre en forme (mais pas que)

- appliquer une mise en forme à partir d'une sémantique
- des règles imbriquées
- langage spécifique à un domaine (_domain specific language_)
- langage déclaratif (vs. langages impératifs)
- langage pour la mise en style (_stylesheet language_): peut être utilisé pour décrire la présentation d'un langage de balisage tel que HTML
- langage de Turing incomplet


{{< pnote >}}

Concrètement un fichier CSS ressemble à cela :

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
```css
p {
  text-align: right;
}
```
{{< pnote >}}
Chaque élément est décrit selon des propriétés définies dans le standard CSS.
Il est aussi possible de créer des attributs (et leur valeur) _dans_ le langage HTML, de spécifier des propriétés CSS sur ces attributs (`class`, `id`).

La feuille style peut soit être intégrée _dans_ le fichier HTML à l'intérieur d'une balise `<style>p { text-align: right;}</style>` ou via un fichier appelé ainsi :
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
```html
<!DOCTYPE html>
<html lang="fr">
  <head>
	  <link rel="stylesheet" href="styles.css">
	  <title>Ma page</title>
	</head>
	<body>
	<p>Du texte.</p>
  </body>
</html>
```

{{< pnote >}}
Ici le fichier `styles.css` doit être placé dans le même dossier que le fichier HTML.

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 5. CSS : mettre en forme (mais pas que)

{{< /pcache >}}

### Exercice

1. créez un fichier `styles.css` avec votre éditeur de texte que vous placez au même niveau que votre fichier HTML précédemment créé
2. indiquez que la couleur de votre titre de niveau 1 doit être rouge avec la propriété `color`
3. enregistrez votre fichier
4. modifier votre fichier HTML précédent pour que la feuille CSS puisse être appelée
5. affichez votre fichier HTML dans votre navigateur web préféré

{{< psectiono >}}


{{< psectioni >}}

## 7. CSS : Concepts fondamentaux

- Sélecteurs = élément, `#id`, `.class`
- [Le modèle de boîte](https://developer.mozilla.org/fr/docs/Learn/CSS/Building_blocks/The_box_model) = padding, margin, border
- Display = inline (`span`, `i`), block (`div`, `p`), flexbox, grid
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 7. CSS : Concepts fondamentaux

{{< /pcache >}}
### Le modèle de boîte
![Box model](/images/box-model.png)
{{< psectiono >}}


{{< psectiono >}}

{{< psectioni >}}

## 8. Démo : Création et mise en forme d'une page web

{{< pnote >}}
Activité réalisée par [Louis-Olivier Brassard](https://www.loupbrun.ca/)
{{< /pnote >}}
{{< psectiono >}}



{{< psectioni >}}
{{< pcache >}}
## 8. Démo : Création et mise en forme d'une page web
{{< /pcache >}}
### Header : déclaration de la police

```html
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <title>Curriculum vitæ</title>

  <link href="style.css" rel="stylesheet" type="text/css" />
  <link href="raleway/raleway.css" rel="stylesheet" type="text/css" />
</head>
```
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 8. Démo : Création et mise en forme d'une page web
{{< /pcache >}}

### En-tête

```html
<header class="entete">

    <h1 class="entete__nom">Giulia Ferretti</h1>

    <span class="entete__titre">Doctorante en humanités numériques</span>

</header>
```

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 8. Démo : Création et mise en forme d'une page web
{{< /pcache >}}

### Premier paragraph

```html
  <hr>

  <section>
    <h2 id="profil">Profil</h2>

    <p>Giulia Ferretti est doctorante à l’université de Montréal. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ac tempus ante. Maecenas efficitur tempus consequat. Aenean egestas ligula sit amet ligula consequat facilisis. Etiam vel vulputate nulla. Integer eget fermentum orci, ac sagittis libero. Etiam efficitur volutpat neque, id placerat ante porta vel.</p>
  </section>

  <hr>
```
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 8. Démo : Création et mise en forme d'une page web

{{< /pcache >}}

### Liste des descriptions

```html
<section>
    <h2 id="informations-personnelles">Informations personnelles</h2>

    <dl class="liste-infos-personnelles">
      <dt>Date de naissance</dt>
      <dd>01 janvier 1999</dd>

      <dt>Adresse</dt>
      <dd>Montréal, QC</dd>

      <dt>Téléphone</dt>
      <dd>+1 (234) 456-7890</dd>

      <dt>Courriel</dt>
      <dd>bonjour@exemple.com</dd>

      <dt>Site web</dt>
      <dd>http://mapageweb.org/</dd>
    </dl>

</section>
```
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 8. Démo : Création et mise en forme d'une page web

{{< /pcache >}}

### `section` Éducation

```html
  <hr>

  <section>
    <h2 id="education">Éducation</h2>
    <section class="entree">
      <h3 class="nom">Université X</h3>
      <div class="periode">depuis 2020</div>
      <div class="diplome">Doctorat</div>

      <div class="description">Lorem ipsum dolor sit amet.</div>
    </section>
    <section class="entree">
      <h3 class="nom">Université Y</h3>

      <div class="periode">2018–2020</div>
      <div class="diplome">Maîtrise</div>
      <div class="description">Lorem ipsum dolor sit amet.</div>
    </section>

    <section class="entree">
      <h3 class="nom">Université Z</h3>

      <div class="periode">2015–2018</div>
      <div class="diplome">Doctorat</div>
      <div class="description">Lorem ipsum dolor sit amet.</div>
    </section>
  </section>

  <hr>
```

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 8. Démo : Création et mise en forme d'une page web
{{< /pcache >}}

### Liste de compétences

```html
 <hr>

  <section>
    <h2 id="competences">Compétences</h2>

    <ul>
      <li>Une liste</li>
      <li>de</li>
      <li>Compétences</li>
    </ul>
  </section>
```

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 8. Démo : Création et mise en forme d'une page web

{{< /pcache >}}

### Pied de page

```html
  <footer>
    Pied de page – Giulia Ferretti
  </footer>

</body>
</html>
```

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 8. Démo : Création et mise en forme d'une page web

{{< /pcache >}}
### Les résultats finaux

<a href="/atelier-css/atelier.html" download>HTML</a>
<a href="/atelier-css/atelier.css" download>CSS</a>
<a href="/atelier-css/raleway.zip" download>Fonts</a>


{{< psectiono >}}
