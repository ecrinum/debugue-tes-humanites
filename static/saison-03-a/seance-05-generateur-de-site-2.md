+++
title = "Séance 05 - Générateur de site statique (2/2)"
date = "2021-09-10"
date_p = "2023-05-30"
description = "Le déploiement d'un site sur le Web grâce au service GitHub Pages."
layout = "diapositive"
visible = true
saison = "3a"
+++


{{< psectioni >}}

# Plan de la séance (2/2)

- Retour sur le site web de la séance 1
- Création d’un dépôt sur la plateforme GitHub
- Déploiement du site personnel sur le web

{{< psectiono >}}

{{< psectioni >}}

## Retour sur le site web personnel

![Aperçu du site statique](/images/apercu-site-statique.png)

{{< psectiono >}}

{{< psectioni >}}

### Fonctionnement d’un site web statique

- La computation se fait une seule fois et non à chaque visite
- Chaque visiteur verra **le même document**
- Différences par rapport à un site « dynamique », dont la réponse dépend de chaque requête
- Il existe bien sûr des solutions hybrides (certaines parties statiques, certaines parties dynamiques)

{{< psectiono >}}

{{< psectioni >}}

### Avantages du paradigme des sites statiques

- **Pages pérennes, facilement archivables** (ex. Internet Archive - archive.org)
- **Le HTML jouit d’une spécification ouverte** (Tim Berners-Lee a placé son invention dans le domaine public)
- **Des pages performantes et résilientes** puisqu’elles n’ont pas besoin d’être « traitées » sur demande (« calculées », « imprimées » ou « emballées », selon votre analogie préférée!), les temps de chargement sont généralement très courts.
  - Moins de vulnérabilités aux attaques groupées (<abbr lang="en" title="Distributed Denial of Service">DDoS</abbr>)
  - Pas d’injection de code par défaut! :)
- **Solution économique** : les environnements de publication très peu gourmands en ressources et en consommation d’énergie.

{{< psectiono >}}

{{< psectioni >}}

## Création d’un dépôt sur la plateforme GitHub

- Connectez-vous à votre compte [GitHub](https://github.com/), ou créez-vous en un si vous n’en avez pas.
- Créez un nouveau dépôt pour votre site personnel. Cela peut être fait de deux façons :
    - **Solution A** : créer un dépôt vide et mettre en ligne vos propres fichiers.
    - **Solution B** : faire d’un <em lang="en">fork</em> du dépôt de l’atelier. Vous devrez ensuite synchroniser vos propres modifications (contenus, réglages et personnalisation).

{{< psectiono >}}

{{< psectioni >}}

### Note sur GitHub

À l’heure actuelle, GitHub est la plateforme la plus ubiquitaire pour le partage et la mise en ligne de code informatique. C’est un peu comme un réseau social pour les développeurs, dont la base d’utilisateurs s’étend aujourd’hui bien au-delà des <em lang="en">geeks</em>. La plateforme est propriété de Microsoft, qui en a fait l’acquisition en 2018. Sachez qu’il existe d’autres services similaires, et que le fonctionnement y est à peu près identique. Ce que vous apprendrez dans cet atelier s’appliquera aisément ailleurs.

{{< psectiono >}}

{{< psectioni >}}

### Solution A : créer un dépôt vide

Lorsque vous êtes connecté·e à GitHub, cliquez sur le petit `+` dans la barre supérieure. Cliquez sur «&nbsp;New repository&nbsp;» (interface disponible en anglais actuellement).

![Création d’un nouveau dépôt sur GitHub](/images/github-nouveau-depot.png)

{{< psectiono >}}

{{< psectioni >}}

Donnez un nom à votre dépôt. GitHub demande d’utiliser des caractères alphanumériques, sans espaces, avec l’option d’utiliser des traits d’union. Vous pouvez aussi ajouter une brève description.

Vous pouvez choisir de rendre votre dépôt «&nbsp;public&nbsp;» ou «&nbsp;privé&nbsp;» (visible uniquement pour vous). Cependant, le statut «&nbsp;public&nbsp;» doit être utilisé pour héberger votre site gratuitement sur GitHub pages. Vous pourrez donc garder vos fichiers privés jusqu’au moment de publier.

Laissez les autres champs vides.

{{< psectiono >}}

{{< psectioni >}}

![Création d’un nouveau dépôt sur GitHub](/images/github-formulaire-depot.png)

{{< psectiono >}}

{{< psectioni >}}

Sur la page de votre dépôt vide, plusieurs options s’offrent à vous : **téléverser** des fichiers, **synchroniser** un dépôt Git existant (sur votre ordinateur par exemple) ou encore **importer** à partir d’une source publique.

Si vous êtes à l’aise avec Git, l’option de la synchronisation est assurément la meilleure; sinon, téléversez vos propres fichiers, nous reviendrons à Git plus tard.

Commencez par initialiser Git pour le répertoire courant, si ce n’est pas déjà fait (dans le dossier du projet sur votre ordinateur)&nbsp;:

```shell
git init
```

Créez un commit, si ce n'est pas déjà fait&nbsp;:

```shell
git commit -m 'Commit initial'
```

{{< psectiono >}}

{{< psectioni >}}

Ensuite, liez l’adresse du dépôt sur GitHub (choisissez HTTPS si vous n’avez jamais utilisé SSH), qui sera appelée `origin` par convention&nbsp;:

```shell
git remote add origin https://remplacez-moi-par-votre-url
```

Enfin, poussez vos modifications! (l’option `-u` doit être utilisé seulement la première fois)

```shell
git push -u origin --all
```

Observez la réponse dans votre terminal. Si aucune erreur ne s’est produite, vous devriez avoir synchronisé vos fichiers sur la plateforme GitHub, félicitations!

{{< psectiono >}}

{{< psectioni >}}

![Page d’un dépôt vide sur GitHub](/images/github-depot-vide.png)

{{< psectiono >}}

{{< psectioni >}}

## Solution B : fork du dépôt de l’atelier

Lorsque vous êtes connecté·e à votre compte GitHub, naviguez à la [page du dépôt](https://github.com/loup-brun/atelier-generateur-site-statique) de cet atelier. Dans le coin supérieur droit, cliquez sur le bouton «&nbsp;Fork&nbsp;» pour créer une divergence. L’historique du dépôt existant sera synchronisé avec votre copie personnelle.

![Bouton fork](/images/github-fork-btn.png)

{{< psectiono >}}

{{< psectioni >}}

### Synchronisation de vos modifications

Prenez un moment pour synchroniser votre répertoire local et votre dépôt hébergé par GitHub. Utilisez la puissance de Git pour versionner vos fichiers. (Référez-vous à la [séance sur Git](https://debugue.ecrituresnumeriques.ca/seance-04-git/) pour un aide-mémoire!)

N’oubliez pas de regénérer vos fichiers de sortie HTML avec Pandoc lorsque vous avez modifié vos fichiers source (par exemple `index.md`, `reglages.yml`), puis de les «&nbsp;`commit`ter&nbsp;».

{{< psectiono >}}

{{< psectioni >}}

## Déploiement de votre site avec GitHub pages

Vous devez ajouter un fichier de configuration. Il y a deux manières de le faire :

1. Depuis l’interface web de GitHub
2. Depuis votre ordinateur

{{< psectiono >}}

{{< psectioni >}}

### Depuis l’interface web de GitHub

Lorsque vos fichiers sont prêts à être publiés, rendez-vous sur la page **Réglages** (<em lang="en">settings</em>) de votre dépôt GitHub. Dans la barre latérale, cliquez sur **Pages**.

Choisissez alors GitHub Actions comme source de déploiement (cette option offre beaucoup plus de souplesse). GitHub vous suggérera peut-être l’action **Static HTML** : cliquez sur le bouton **Configure**. Sinon, retrouvez-la en cliquant sur «&nbsp;Browser all workflows&nbsp;».

{{< psectiono >}}

{{< psectioni >}}

![Réglages GitHub Pages](/images/github-pages.png)

{{< psectiono >}}

{{< psectioni >}}

Vous aurez alors un fichier de configuration pré-rempli : assurez-vous simplement que la propriété `path` de l’étape du téléversement corresponde à votre projet (utilisez un point `.` pour publier le dépôt à partir de la racine, ou un sous-répertoire, comme `site`, si votre site ne se trouve pas à la racine).

```yaml
  - name: Upload artifact
    uses: actions/upload-pages-artifact@v1
    with:
      # Upload entire repository
      path: '.'
```

Pour confirmer vos modifications, créez un commit avec le bouton **Commit changes...** et résumez vos changements dans la boîte de texte prévue à cet effet.

{{< psectiono >}}

{{< psectioni >}}

![Commit sur l’interface web de GitHub](/images/github-commit.png)

{{< psectiono >}}

{{< psectioni >}}

### Depuis votre ordinateur

Pour déployer avec GitHub, vous devrez créer un répertoire et un sous-répertoire à la racine de votre dépôt&nbsp;: `.github/workflows` (où le répertoire `workflows` se situe à l’intérieur du répertoire `.github`). Notez que votre explorateur de fichiers peut masquer les fichiers et dossiers débutant par un point&nbsp;: cela peut être changé dans les réglages de votre système d’exploitation. Depuis le terminal, vous y arriverez sans problème. Git ne synchronise pas les répertoires vides&nbsp;, mais uniquement ceux contenant des fichiers.

Depuis la racine du répertoire du projet sur votre ordinateur, créez deux sous-dossiers simultanément (`mkdir` avec l’option `-p`)&nbsp;:

```shell
mkdir -p .github/workflows
```

{{< psectiono >}}

{{< psectioni >}}

Créez à présent un fichier `mon-site.yml` dans ce sous-répertoire. Vous pouvez lui donner le nom que vous voulez, cela n’a pas d’importance, mais vous devez utiliser l’extension `.yml`&nbsp;:

```shell
touch .github/workflows/mon-site.yml
```

Ouvrez ce fichier pour édition et remplissez-le avec [l’exemple fourni dans le dépôt de l’atelier](https://github.com/loup-brun/atelier-generateur-site-statique/blob/main/.github/workflows/mon-site.yml).

Committez vos changements (`git commit -m '...'`) et poussez-les vers le dépôt en ligne (`git push`).

{{< psectiono >}}

{{< psectioni >}}

Rendez-vous maintenant à la page **Actions** de votre dépôt sur GitHub. À la suite de votre `push`, GitHub devrait avoir lancé les actions spécifiées dans votre fichier de configuration. Si tout s’est bien passé, vous verrez un crochet vert signifiant que les opérations se sont conclues avec succès!

Cliquez sur le détail de l’action. Vous pourrez alors consulter l’adresse de déploiement de votre site.

{{< psectiono >}}

{{< psectioni >}}

![Résultat d’un workflow terminé](/images/github-workflow-termine.png)

{{< psectiono >}}

{{< psectioni >}}

## Pour aller plus loin

- Vous pouvez créer un fichier de configuration qui reconstruit votre site automatiquement avec Pandoc. Fini les oublis après avoir modifié les fichiers source!
- La plupart des générateurs de sites statiques suggèrent de ne pas versionner les artéfacts HTML dans Git. Pour des usages avancés avec un grand nombre de pages, cette pratique est recommandée. Il suffira de spécifier les fichiers à ignorer dans un fichier `.gitignore`. [Exemple avec le générateur de site statique Hugo.](https://github.com/github/gitignore/blob/main/community/Golang/Hugo.gitignore)
- Si vous disposez d’un nom de domaine, vous pouvez [l’utiliser avec votre dépôt](https://docs.github.com/en/pages/configuring-a-custom-domain-for-your-github-pages-site/managing-a-custom-domain-for-your-github-pages-site).

{{< psectiono >}}

{{< psectioni >}}

## Conclusion

- N’oubliez pas qu’il existe [une myriade](https://jamstack.org/generators/) de générateurs de sites statiques. Chacun vient avec sa propre documentation, ses normes et conventions, ses forces et ses faiblesses.
- Si vous rédigez en markdown, il y a de bonnes chances que vous puissiez passer de l’un à l‘autre facilement!
- Et si vous cherchez de l’inspiration pour votre propre site personnel, n’hésitez pas à [jeter un coup d’œil par ici](https://personalsit.es/)!

{{< psectiono >}}
