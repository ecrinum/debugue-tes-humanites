+++
title = "Séance 03 - Les formats"
date = "2021-09-11"
date_p = "2021-09-24"
description = "Définitions, implications techniques et politiques, encodage et ouverture."
layout = "archives-diapositive"
visible = true
enregistrement = "https://api.nakala.fr/embed/10.34847/nkl.eae6naxs/412b18ad892607fde76dd56ba38d1f1ef1143324"
saison = "1"
+++
{{< psectioni >}}
## Plan de la séance

1. Définitions
2. Implications techniques et politiques
3. Encodage et ouverture
{{< psectiono >}}

{{< psectioni >}}
## 1. Définitions

{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 1. Définitions
{{< /pcache >}}
### 1.1. Origines des formats

{{< pnote >}}
Partons de l'édition : les formats de papier pour l'imprimé, première apparition _technique_ du terme ?

Le terme _format_ est un terme technique, son usage permet de délimiter les caractéristiques d'un objet : avec le format nous donnons un certain nombres de données, d'instructions, ou de règles.
Pourquoi définir tout cela ?
L'objectif est de constituer une série d'informations compréhensible, utilisable et communicable.

Pour prendre un exemple concret du côté du livre, l'impression d'un document nécessite de s'accorder sur un format de papier.
Les largeurs, longueurs et orientations sont normalisées, des standards sont établis, ils permettent alors de concevoir des imprimantes qui peuvent gérer des types définis de papier.
Sans des formats de papier il est difficile de créer des machines adéquates, comme des presses à imprimer ou des imprimantes.
L'usage du format dans l'imprimerie est sans doute la première apparition de ce terme technique, il est intéressant de noter que le _format_ est ainsi d'abord attaché au livre et à sa fabrication.

Notons également que des outils ou des processus sont associés au format : les instructions sont définies pour qu'une action soit réalisée par un agent — humain, analogique, mécanique, numérique.

Enfin, sans format, pas de média.
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 1. Définitions
{{< /pcache >}}
### 1.2. Qu'est-ce qu'un format informatique ?
Structurer les informations avec des spécifications techniques.

{{< pnote >}}
Un _format informatique_ est le pivot entre une organisation logique et son implémentation dans un système informatique.
Un fichier doit avoir un format, sans quoi il ne pourra être produit, transmis ou lu.
Un format informatique est le lien entre l'infrastructure et l'agent (humain ou programme) qui utilise cette infrastructure.
Le choix des formats informatiques détermine la manière dont les informations sont créés, stockées, envoyées, reçues, interprétées, affichées.
Aujourd'hui les formats prennent une place importante dans notre environnement, et leur incidence dépasse le domaine de l'informatique, leur étude a pourtant été longtemps délaissée dans le champ des médias.

Exemple du format DOC ou .doc : le logiciel Microsoft Word ne peut pas lire n'importe quel format informatique, les données doivent être structurées d'une façon précise pour que le logiciel puisse les interpréter, et ensuite les modifier, et enfin produire une nouvelle version du fichier.
Ici le format DOC a été créé pour les besoins d'un logiciel spécifique.

Dans cet exemple c'est le format informatique qui est le résultat du logiciel, mais d'autres fonctionnement sont possibles.
Par ailleurs, le format DOC est longtemps resté propriétaire (jusqu'à l'arrivée du format DOCX), ses spécifications n'étaient pas publiques et des brevets empêchaient toute initiative de développement d'un logiciel autre que Word capable de lire ou de modifier des fichiers .doc.
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 1. Définitions
{{< /pcache >}}
### 1.3. Pourquoi s'intéresser aux formats ?
Comprendre les rouages du numérique.

{{< pnote >}}
Les formats informatiques ont structuré et structurent encore l'espace numérique.

{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
## 2. Implications techniques et politiques

{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 2. Implications techniques et politiques
{{< /pcache >}}
### 2.1. L'interopérabilité
La condition du numérique : faire dialoguer les machines.

{{< pnote >}}
L'interopérabilité est un principe qui permet à plusieurs machines de dialoguer :

- en s'accordant sur des règles pour définir une série d'informations, les machines peuvent lire et écrire un fichier ;
- si ces spécifications sont clairement énoncées, il n'y a alors plus de dépendance vis-à-vis d'un logiciel spécifique ;
- la question de l'interopérabilité est intimement liée à la standardisation et à l'ouverture du format.

{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 2. Implications techniques et politiques
{{< /pcache >}}
### 2.2. Ouvert ou fermé ?
Standards et licences.

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 2. Implications techniques et politiques
{{< /pcache >}}
### 2.3. Écrire un format
Comment un standard apparaît ?

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 3. Encodage et ouverture

{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 3. Encodage et ouverture
{{< /pcache >}}
### Texte brut

- caractères sans informations graphiques 
- ASCII 1 octet 2<sup>8</sup>
- utf-8, iso8859 etc. de 1 à 4 octet 2
- editors, formats et syntaxe (txt, html, md)

{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 3. Encodage et ouverture
{{< /pcache >}}
### word

![](http://vitalirosati.net/slides/img/traitement-de-texte-rouge.png)

{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 3. Encodage et ouverture
{{< /pcache >}}
### À quoi sert un ordinateur?

- à calculer
- à enregistrer?
- à écrire?
- documenter le code

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Encodage et ouverture
{{< /pcache >}}
### Les machines à écrire
Pourquoi ne pas utiliser une machine à écrire?

- parce qu’on écrit aussi du code
- pour enregistrer

![Avant Word](http://vitalirosati.net/slides/img/word_processorEP.jpg)

{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 3. Encodage et ouverture
{{< /pcache >}}
### Écrire du code et de la doc, enregistrer et imprimer.

- 1976 Electric Pencil (Michael Shrayer)
- 1979 Easy Writer - John Thomas Draper
- 1979 WordStar: WYSYWYG - car notion de page
- L’ordinateur pour tous - 1984
- 1983 Word

{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 3. Encodage et ouverture
{{< /pcache >}}
### Les principes

- interface graphique
- imprimé
- bureautique
- format=software

{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 3. Encodage et ouverture
{{< /pcache >}}
### Les effets

- la “désintermédiation”
    - perte de compétences
- la perte de contrôle
- la perte d’utilité (utiliser un ordinateur comme une machine à écrire)

{{< psectiono >}}