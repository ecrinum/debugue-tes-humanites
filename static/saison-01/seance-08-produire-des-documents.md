+++
title = "Séance 08 - Produire des documents"
date = "2021-09-11"
date_p = "2021-11-12"
description = "Structuration du texte, introduction à Pandoc et manipulations."
layout = "archives-diapositive"
visible = true
enregistrement = "https://api.nakala.fr/embed/10.34847/nkl.b59453c4/fd298a8ef5393abf818338d3330c467d36100531"
saison = "1"
+++
{{< psectioni >}}
## Plan de la séance

1. Rappels : qu'est-ce que structurer un texte ?
2. Transformer, convertir ou générer
3. Pandoc : "le couteau suisse de l'édition"
4. Découverte de Pandoc par la manipulation
{{< psectiono >}}


{{< psectioni >}}
## 1. Rappels : qu'est-ce que structurer un texte ?
Donner du sens au texte.

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Rappels : qu'est-ce que structurer un texte ?
{{< /pcache >}}
### 1.1. Séparer le fond et la forme
Ne pas attribuer un rendu graphique au texte avant d'avoir attribué un sens.

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Rappels : qu'est-ce que structurer un texte ?
{{< /pcache >}}
### 1.2. Baliser le texte
Qualifier le texte :

`_Qualifier_ le texte`

`<em>Qualifier</em> le texte`

`<marquage typemarq="italique">Qualifier</marquage> le texte`

{{< pnote >}}
Il s'agit alors de baliser le texte, c'est-à-dire d'indiquer par une convention de signes typographiques les éléments constitutifs du sens du texte, qui ensuite se traduiront par une mise en forme graphique spécifique.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 2. Transformer, convertir ou générer

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Transformer, convertir ou générer
{{< /pcache >}}
### 2.1. Que faire du texte balisé ?
Le balisage n'est qu'une étape intermédiaire.

{{< pnote >}}
Car il faut désormais rendre ce balisage lisible, utile ou interrogeable.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Transformer, convertir ou générer
{{< /pcache >}}
### 2.2. Passer d'un balisage à un autre
D'un format texte/brut à un autre.

`_Qualifier_ le texte`

`<em>Qualifier</em> le texte`

{{< pnote >}}
Il s'agit de passer d'un format de fichier à un autre, en automatisant ces transformations.
Typiquement : transformer un fichier Markdown en fichier HTML.

Attention ici, lorsque l'on parle de _conversion_, il ne s'agit pas d'obtenir un format PDF.
La conversion est la transformation de balises en d'autres balises, et non la production d'une image graphique.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Transformer, convertir ou générer
{{< /pcache >}}
### 2.3. Paramètres de la conversion
Les contenus en entrée :

- texte balisé
- métadonnées sérialisées
- bibliographie structurée


{{< pnote >}}
Ce qu'il faut comprendre ici c'est que la conversion est un processus qui prendre un certain nombre de données en compte, et pas uniquement le _texte_ balisé.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 3. Pandoc : "le couteau suisse de l'édition"
Pandoc est un logiciel libre (en ligne de commande) de conversion de fichiers texte, créé par John MacFarlane et sous licence GPL.

Markdown ⟷ HTML  
Markdown ⟷ LaTeX  
HTML ⟷ TEI  
HTML ⟷ EPUB

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Pandoc : "le couteau suisse de l'édition"
{{< /pcache >}}
### 3.1. Fonctionnement de Pandoc
`programme` `option` `fichier.entrée`

{{< pnote >}}
Pandoc fonctionne en ligne de commande, avec un schéma classique d'options.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Pandoc : "le couteau suisse de l'édition"
{{< /pcache >}}
### 3.2. Exemples
`pandoc mon-fichier-markdown.md`

`pandoc -f markdown -t html mon-fichier-markdown.md -o mon-fichier-html.html`

`pandoc -f markdown -t html --template=mon-modele.html mon-fichier-markdown.md -o mon-fichier-html.html`

{{< pnote >}}
La première commande convertit le fichier en HTML, le format de sortie par défaut de Pandoc.

La deuxième commande fait exactement la même chose, mais elle est plus verbeuse.

La troisième commande applique un modèle ou _template_, permettant ainsi de structurer le document d'une façon plus précise.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 4. Découverte de Pandoc par la manipulation
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Découverte de Pandoc par la manipulation
{{< /pcache >}}
### 4.1. Installation de Pandoc
Suivre les instructions sur cette page : [https://docs.zettlr.com/fr/installing-pandoc/](https://docs.zettlr.com/fr/installing-pandoc/)

{{< pnote >}}
Quelque soit votre système d'exploitation une solution existe.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Découverte de Pandoc par la manipulation
{{< /pcache >}}
### 4.2. Créer un document Markdown et le convertir en HTML

- créer un document Markdown avec des niveaux de titre, une liste, une citation longue et de l'emphase (italique et gras)
- lancer la commande `pandoc mon-fichier.md` en l'adaptant 
- ouvrir le fichier HTML obtenu
{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Découverte de Pandoc par la manipulation
{{< /pcache >}}
### 4.3. Ajouter des métadonnées

- ajouter des métadonnées à votre document avec un entête du type :

```
---
title: Le titre de mon document
author: Mon Nom
---
```

- convertir ce fichier Markdown en HTML puis en DOCX
- ouvrir les fichiers obtenus, que remarquez-vous ?

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Découverte de Pandoc par la manipulation
{{< /pcache >}}
### 4.4. Appliquer un modèle

- créer un fichier HTML avec le code suivant :

```
<html>
    <head>
    <title>$titre$</title>
    <meta name="author" content="$auteur$">
    </head>
    <body>
    <h1 class="titre">$titre$</h1>

    <p class="auteur">$auteur$</p>

    $if(date)$
    <p class="date">$date$</p>
    $endif$


    <div>$body$</div>

    </body>
</html>
```


- lancer la conversion en HTML en appliquant le modèle/template
- ouvrir le document obtenu

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## Ressources

- Arthur Perret, L’écriture académique au format texte, [https://www.arthurperret.fr/2021-09-21-ecriture-academique-format-texte.html](https://www.arthurperret.fr/2021-09-21-ecriture-academique-format-texte.html)
- Antoine Fauchié, Fabriques de publication : Pandoc, [https://www.quaternum.net/2020/04/30/fabriques-de-publication-pandoc/](https://www.quaternum.net/2020/04/30/fabriques-de-publication-pandoc/)
- Dennis Tenen et Grant Wythoff, Rédaction durable avec Pandoc et Markdown, Programming Historian, [https://programminghistorian.org/fr/lecons/redaction-durable-avec-pandoc-et-markdown](https://programminghistorian.org/fr/lecons/redaction-durable-avec-pandoc-et-markdown)
- Nicolas Sauret et Marcello Vitali-Rosati, tutorielMdPandoc, [https://framagit.org/stylo-editeur/tutorielmdpandoc](https://framagit.org/stylo-editeur/tutorielmdpandoc)
- documentation de Pandoc : [https://pandoc.org/MANUAL.html](https://pandoc.org/MANUAL.html)

{{< psectiono >}}
