+++
title = "Séance 07 - Structurer une bibliographie"
date = "2021-09-11"
date_p = "2021-11-05"
description = "Définition, les styles bibliographiques, introduction à Zotero."
layout = "archives-diapositive"
visible = true
enregistrement = "https://api.nakala.fr/embed/10.34847/nkl.8f3bn05c/b8ea81caee90fb2ad80e224d5076e51616a2cda7"
saison = "1"
+++
{{< psectioni >}}
## Plan de la séance

1. Qu'est-ce qu'une bibliographie structurée ?
2. Pourquoi tant de styles bibliographiques ?
3. Introduction à Zotero
4. Vous avez dit BibTeX ?
{{< psectiono >}}


{{< psectioni >}}
## 1. Qu'est-ce qu'une bibliographie structurée ?

- deux actes : la citation et la bibliographie
- une citation ou une référence bibliographique = des données
- réutiliser les données
- exposer des données

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Qu'est-ce qu'une bibliographie structurée ?
{{< /pcache >}}

Vandendorpe, C. (1999). _Du papyrus à l’hypertexte : Essai sur les mutations du texte et de la lecture_. La Découverte.

Vandendorpe, C. (1999). _Du papyrus à l’hypertexte : essai sur les mutations du texte et de la lecture_. La Découverte.

Vandendorpe, Christian. _Du papyrus à l’hypertexte : essai sur les mutations du texte et de la lecture_. La Découverte, 1999.

Vandendorpe, Christian. 1999. _Du papyrus à l’hypertexte : essai sur les mutations du texte et de la lecture_. Paris, France : La Découverte.

VANDENDORPE, Christian, _Du papyrus à l’hypertexte : essai sur les mutations du texte et de la lecture_, Paris, France, La Découverte, 1999, 271 p.

{{< pnote >}}
Styles respectifs :

- APA 7th
- Université de Montréal - APA (Français - Canada)
- Modern Language Association 9th edition
- Chicago Manual of Style 17th edition (author-date, Français)
- Lettres et Sciences Humaines (biblio et notes, Français)

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Qu'est-ce qu'une bibliographie structurée ?
{{< /pcache >}}
Vandendorpe, C. (1999). _Du papyrus à l’hypertexte : essai sur les mutations du texte et de la lecture_. La Découverte.

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Qu'est-ce qu'une bibliographie structurée ?
{{< /pcache >}}

<span class="bib-nom">Vandendorpe</span>, <span class="bib-prenom">C.</span> (<span class="bib-annee">1999</span>). <span class="bib-titre">Du papyrus à l’hypertexte : essai sur les mutations du texte et de la lecture</span>. <span class="bib-editeur">La Découverte</span>.

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Qu'est-ce qu'une bibliographie structurée ?
{{< /pcache >}}
```
<div id="vandendorpe_du_1999">
    <dt>Vandendorpe (1999)</dt>
    <dd><span itemscope itemtype="https://schema.org/Book" data-type="book">
        <span itemprop="author" itemscope itemtype="https://schema.org/Person">
            <span itemprop="familyName">Vandendorpe</span>,
            <meta itemprop="givenName" content="Christian" /> C.</span>
            (<span itemprop="datePublished">1999</span>).
            <span itemprop="name">
                <i>Du papyrus à l&rsquo;hypertexte:
                essai sur les mutations du texte et de la lecture</i>
            </span>.
        <meta itemprop="contentLocation" value="Paris, France">&#32;
        <span itemprop="publisher" itemtype="http://schema.org/Organization">
        <span itemprop="name">La Découverte</span>
        </span>.
        </span>
    </dd>
</div>
```

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Qu'est-ce qu'une bibliographie structurée ?
{{< /pcache >}}


{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 2. Pourquoi tant de styles bibliographiques ?

- un style bibliographique : liste de règles pour mettre en forme une référence (citation + bibliographie)
- spécificités selon les domaines
- 10 153 styles bibliographiques (dont beaucoup de doublons)

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 3. Introduction à Zotero
### 3.1. Qu'est-ce que Zotero ?

- un logiciel et un service qui permet de gérer des références bibliographiques&nbsp;: conserver, organiser, citer et créer des bibliographies&nbsp;;
- un logiciel libre, et gratuit&nbsp;;
- un outil qui peut être collaboratif&nbsp;;
- un logiciel + une extension de navigateur pour récupérer des références.
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Introduction à Zotero
{{< /pcache >}}
### 3.2. Fonctionnalités

- *collecter*&nbsp;: collecter des références bibliographiques en ligne et les conserver&nbsp;;
- *organiser*&nbsp;: organiser ses références en collections et sous-collections, et en attribuant des tags et des marqueurs&nbsp;;
- *citer*&nbsp;: citer ses références facilement et les exporter/intégrer dynamiquement dans des documents&nbsp;;
- *synchroniser*&nbsp;: synchroniser ses références avec un compte en ligne&nbsp;;
- *collaborer*&nbsp;: échanger ses références avec d'autres utilisateurs.

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Introduction à Zotero
{{< /pcache >}}
### 3.3. Un logiciel libre
Zotero est logiciel libre qui peut être téléchargé puis installé sur tous les ordinateurs.

Il faut également utiliser un _connecteur_ pour récupérer des références sur le web puis faire le lien avec le logiciel qui les conserve.
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Introduction à Zotero
{{< /pcache >}}

### 3.4. Utilisation générale

1. récupérer des références depuis un navigateur web (grâce à l'extension Zotero)&nbsp;;
2. une icône apparaît pour enregistrer une référence (plusieurs types de documents)&nbsp;;
3. la référence est enregistrée dans le logiciel Zotero sous la forme d'une notice&nbsp;;
4. il est possible de compléter la notice s'il manque des informations&nbsp;;
5. la synchronisation permet de sauvegarder les données en ligne (pour cela il faut créer un compte Zotero).
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Introduction à Zotero
{{< /pcache >}}
### 3.5. Exemple à partir du catalogue de la Bibliothèque universitaire de l'UdeM
_À voir en cours._
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Introduction à Zotero
{{< /pcache >}}
### 3.6. De nombreuses bases de données compatibles
De nombreux catalogues, bases de données, revues en ligne, journaux, blog, réseaux sociaux proposent des références&nbsp;:

- Sudoc
- Gallica
- OpenEdition, revues.org, Hypothèses
- Érudit
- Google Livres, Google Scholar, YouTube
- Internet Archive
- Cairn.info
- etc.
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Introduction à Zotero
{{< /pcache >}}
### 3.7. Description d'une utilisation classique
Prérequis&nbsp;: logiciel installé et extension installée (par exemple dans Firefox).

1. ouvrir le logiciel (Zotero Standalone)&nbsp;;
2. rechercher un document sur le web&nbsp;;
3. enregistrer la référence via l'extension du navigateur&nbsp;;
4. vérifier dans le logiciel si la référence convient (et si elle est placée au bon endroit)&nbsp;;
5. compléter les informations.
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Introduction à Zotero
{{< /pcache >}}
### 3.8. Quelques points importants

- pas besoin d'être en ligne pour _utiliser_ Zotero (mais vous ne pourrez pas afficher de pages web ou synchroniser vos collections)&nbsp;;
- les fonctions de Zotero sont nombreuses, n'hésitez pas à les tester et à les utiliser !
- Zotero permet de conserver et de gérer des références de pages web, mais ce n'est pas sa fonction première&nbsp;;
- n'oubliez pas d'ouvrir le logiciel Zotero pour enregistrer vos références.
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Introduction à Zotero
{{< /pcache >}}
### 3.9. Collecter

- via la navigateur en cliquant sur l'icône&nbsp;: Zotero récupère les métadonnées de la page web (page web qui présente un document)&nbsp;;
- si le texte intégral est disponible en PDF, Zotero le récupère et le sauvegarde&nbsp;;
- si la page web contient plusieurs références (par exemple une page de résultats), alors l'icône est un dossier et il est possible de sélectionner les résultats que vous souhaitez conserver&nbsp;;
- Zotero peut ajouter une référence automatiquement grâce à un identifiant – ISBN ou DOI&nbsp;: cliquez sur "Ajouter un élément par son identifiant" dans la barre d'outils Zotero, puis tapez le numéro d'identification, l'élément sera ajouté à votre bibliothèque.
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 3. Introduction à Zotero
{{< /pcache >}}
### 3.10. Créer
Vous pouvez aussi créer manuellement des références en sélectionnant le type de document que vous souhaitez référencer et en remplissant les champs de la notice.
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 3. Introduction à Zotero
{{< /pcache >}}
### 3.11. Collections

- dans colonne de gauche, "Mes collections" ou "Ma bibliothèque", il est possible de créer plusieurs collections&nbsp;;
- créez une nouvelle collection en cliquant sur le bouton "Nouvelle collection"&nbsp;;
- il est possible de créer, renommer ou supprimer une collection (via un clic droit sur une collection)&nbsp;;
- pour organiser vos références il suffit de les déplacer d'un dossier d'une collection à une autre&nbsp;;
- les collections peuvent comprendre des sous-collections !
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Introduction à Zotero
{{< /pcache >}}
### 3.12. Tags et marqueurs

- les tags et les marqueurs servent à ajouter des informations de description&nbsp;;
- vous pouvez vous créer votre propre système de marqueurs et de tags pour organiser plus finement vos références&nbsp;;
- certaines références importées comportent parfois des marqueurs, vous pouvez vous en inspirer ou les supprimer&nbsp;;
- une recherche dans Zotero interroge tous les champs, y compris les marqueurs et les tags !
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 3. Introduction à Zotero
{{< /pcache >}}
### 3.13. Champ "note"

- il permet de saisir du texte libre lié à une référence précise&nbsp;;
- il peut y avoir plusieurs notes pour une même référence&nbsp;;
- il est possible de les visualiser rapidement, de les supprimer, et elles sont synchronisées si vous avez un compte Zotero&nbsp;;
- une recherche dans Zotero interroge tous les champs, y compris les notes !
{{< psectiono >}}


{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 4. Vous avez dit BibTeX ?


{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}