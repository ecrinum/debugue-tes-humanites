+++
title = "Séance 04 - Gérer ses fichiers"
date = "2021-09-11"
date_p = "2021-10-01"
description = "Où sont les fichiers sur un ordinateur, usages basiques d'un terminal, les nuages."
layout = "archives-diapositive"
visible = true
enregistrement = "https://api.nakala.fr/embed/10.34847/nkl.adeb964l/bf6caddec692dfdfb16dcdd34d1614f49e3f829f"
message = "L'enregistrement de cette séance est malheureusement défectueux, la capture de l'écran n'a pas fonctionné. Nous nous excusons pour ce problème."
saison = "1"
+++
{{< psectioni >}}
## Plan de la séance

1. Où sont les fichiers sur un ordinateur&nbsp;?
2. Usages basiques d'un terminal
3. Les nuages
{{< psectiono >}}


{{< psectioni >}}
## 1. Où sont les fichiers sur un ordinateur&nbsp;?

{{< pnote >}}
Pourquoi chercher à comprendre où sont les fichiers sur un ordinateur&nbsp;?
Parce que cela permet ensuite d'interagir plus facilement avec les différents fichiers qui constituent par exemple un projet d'édition ou d'écriture numérique.
Prendre conscience de l'organisation de ces fichiers est nécessaire notamment pour lier des fichiers entre eux, donc pour faire des liens relatifs ou absolus entre deux entités numériques.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Où sont les fichiers sur un ordinateur&nbsp;?
{{< /pcache >}}
### 1.1. L'arborescence

```
disque dur
    └── home
        └── machine
            ├── Documents
            |   ├── mon-texte.md
            |   ├── Photos
            |   |   └── image.jpg
            ├── .cache
            ...
```

{{< pnote >}}
Chaque système d'exploitation a son propre système d'arborescence.
Deux types de fichiers sont stockés sur un ordinateur :

- des fichiers liés au système d'exploitation ou aux logiciels ;
- des fichiers de contenu, allant du texte brut aux vidéos en passant par des fichiers pour des logiciels de traitement de texte.

Comprendre le fonctionnement global de l'arborescence permet de se repérer dans son ordinateur.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Où sont les fichiers sur un ordinateur&nbsp;?
{{< /pcache >}}
### 1.2. Le stockage
Un fichier = plusieurs moyens d'y accéder

{{< pnote >}}
Il est possible d'accéder à un fichier de plusieurs façons différentes :

- navigateur/explorateur de fichiers de votre système d'exploitation
- chemin complet
- moteur de recherche de votre explorateur de fichiers
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 1. Où sont les fichiers sur un ordinateur&nbsp;?
{{< /pcache >}}
### 1.3. Chemins relatifs et chemins absolus

- **chemins relatifs** : le chemin est indiqué depuis un point de référence
- **chemins absolus** : le chemin est complet et toujours valide où que l'on soit

{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 2. Usages basiques d'un terminal
Une autre façon d'utiliser un ordinateur.

{{< pnote >}}
Le terminal est à un explorateur de fichiers ce qu'un fichier en texte brut est à un document d'un traitement de texte : simple et puissant.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Usages basiques d'un terminal
{{< /pcache >}}
### 2.1. Qu'est-ce qu'un terminal&nbsp;?
> Un terminal est un programme qui émule une console dans une interface graphique, il permet de lancer des commandes.  
> ([Source](https://doc.ubuntu-fr.org/terminal))

{{< pnote >}}
S'il faut retenir une chose : un terminal est un autre moyen d'interagir avec un ordinateur.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Usages basiques d'un terminal
{{< /pcache >}}
### 2.2. Ouvrir un terminal
Sous Linux ou Mac, chercher "terminal".

Sous Windows, activer le terminal/bash en suivent [ces instructions](https://korben.info/installer-shell-bash-linux-windows-10.html) ou [celles-ci](https://blog.ineat-group.com/2020/02/utiliser-le-terminal-bash-natif-dans-windows-10/).

{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Usages basiques d'un terminal
{{< /pcache >}}
### 2.3. Savoir où l'on est
`pwd`

```
pwd

/home/machine
```

{{< pnote >}}
À noter ici qu'il s'agit d'un **chemin absolu** puisque l'adresse/chemin indiquée commence par une barre oblique `/`.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Usages basiques d'un terminal
{{< /pcache >}}
### 2.4. Lister les fichiers
`ls`

ou

`ls -a` pour voir aussi les fichiers cachés.

{{< pnote >}}
Pourquoi vouloir voir les fichiers cachés&nbsp;?
Ce sera utile pour la suite.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Usages basiques d'un terminal
{{< /pcache >}}
### 2.5. Naviguer
`cd` suivi du chemin. Exemple :

`cd Document/Photos`

mène au dossier `Photos`.

{{< pnote >}}
Il est possible d'indiquer à la fois un chemin relatif, donc sans commencer par une barre oblique, ou en commençant par une barre oblique pour un chemin absolu.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Usages basiques d'un terminal
{{< /pcache >}}
### 2.6. Les bons réflexes

- touche `tabulation` pour compléter une commande
- flèche du haut `↑` pour parcourir les dernières commandes utilisées
- `CTRL + R` pour recherche une commande en tapant les premières lettres (faites à nouveau `CTRL + R` autant de fois que nécessaire pour parcourir l'historique à partir des lettres que vous avez tapées)

{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Usages basiques d'un terminal
{{< /pcache >}}
### 2.7. Déplacer un fichier
`mv` permet de renommer ou de déplacer un fichier, deux exemples :

- `mv mon-fichier.md fichier.md` renomme le fichier `mon-fichier.md` en `fichier.md`
- `mv /home/machine/mon-fichier.md /home/machine/Documents/mon-fichier.md` déplace le fichier `mon-fichier.md` dans le dossier `Documents`

{{< pnote >}}
Attention, pour cette dernière commande il est nécessaire que le dossier `Documents` existe déjà.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Usages basiques d'un terminal
{{< /pcache >}}
### 2.8. Supprimer un fichier
`rm mon-fichier.md`

`rm -R mon-dossier` supprime le dossier et tout ce que contient ce dossier.

{{< pnote >}}
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Usages basiques d'un terminal
{{< /pcache >}}
### 2.9. Afficher le contenu d'un fichier
`cat mon-fichier.md`

{{< psectiono >}}

