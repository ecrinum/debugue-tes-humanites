+++
title = "Séance 05 - Tout savoir sur le terminal"
date = "2021-09-11"
date_p = "2021-10-08"
description = "Commandes basiques, commandes avancées, grep, bash et oh my zsh."
layout = "archives-diapositive"
visible = true
enregistrement = "https://api.nakala.fr/embed/10.34847/nkl.c1f50w7g/edc13b04edb9e36990d5d5abb98728da095e6ebb"
saison = "1"
+++
{{< psectioni >}}
## Plan de la séance

1. Commandes basiques du terminal : rappels
2. Commandes avancées
3. grep
4. bash
5. zsh
{{< psectiono >}}


{{< psectioni >}}
## 1. Commandes basiques du terminal : rappels

- où je suis : `pwd`
- liste des fichiers : `ls`
- naviguer : `cd`
- déplacer un fichier : `mv`
- supprimer un fichier : `rm`
- afficher le contenu d'un fichier : `cat`
- créer un fichier : `touch`
- créer un dossier : `mkdir`

Bons réflexes :

- tabulation : autocomplétion
- flèche du haut : historique des commandes
- `CTRL + R` : rechercher dans l'historique

{{< pnote >}}
Pourquoi chercher à comprendre où sont les fichiers sur un ordinateur&nbsp;?
Parce que cela permet ensuite d'interagir plus facilement avec les différents fichiers qui constituent par exemple un projet d'édition ou d'écriture numérique.
Prendre conscience de l'organisation de ces fichiers est nécessaire notamment pour lier des fichiers entre eux, donc pour faire des liens relatifs ou absolus entre deux entités numériques.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## Exercice

1. créez un dossier `livre` contenant un sous-dossier `chapitre-01` contenant lui-même un fichier `texte.txt`
2. naviguez dans le dossier `chapitre`
3. revenez dans le dossier `livre`
4. déplacez le fichier `texte.txt` dans le dossier `livre`
5. renommez le fichier `texte.txt` en `chapitre-01.txt`
6. supprimez le dossier `chapitre-01`

{{< psectiono >}}


{{< psectioni >}}
## 2. Commandes avancées
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Commandes avancées
{{< /pcache >}}
### 2.1. Supprimer les commandes affichées
`clear`

{{< pnote >}}
Pratique si vous voulez repartir sur un terminal _neuf_.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Commandes avancées
{{< /pcache >}}
### 2.2. Afficher l'historique

`history`

{{< pnote >}}
Pour afficher et parcourir l'historique des commandes que vous avez utilisées (seulement les commandes, pas les résultats de ces commandes).
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Commandes avancées
{{< /pcache >}}
### 2.3. Afficher le contenu d'un fichier page à page
`less`

Par exemple `less mon-texte.txt`

{{< pnote >}}
C'est une commande similaire à `cat`, mais plus lisible dans le résultat.

Pour quitter : `q`.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Commandes avancées
{{< /pcache >}}
### 2.4. Copier un fichier/dossier
`cp`

Par exemple `cp livre/texte.txt sauvegarde/divers/livre/texte.txt`

Ou encore `cp -r livre sauvegarde/divers`
{{< pnote >}}
Cette commande copie le dossier `livre` et son contenu (sous-dossiers, fichiers) dans le dossier `sauvegarde/divers`.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Commandes avancées
{{< /pcache >}}
### 2.5. Compter les mots
`wc`

Par exemple `wc texte.txt`
{{< pnote >}}
Cela permet de compter le nombre de lignes, de mots et de caractères dans un fichier texte.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Commandes avancées
{{< /pcache >}}
### 2.6. Chercher
`find`

{{< pnote >}}
Cette commande permet de chercher dans les noms de fichiers/dossiers, et dans les fichiers eux-mêmes.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Commandes avancées
{{< /pcache >}}
### 2.7. Afficher le manuel
`man`

Exemple : `man wc` pour tout savoir sur la commande `wc`

{{< pnote >}}
Cette commande permet de chercher dans les noms de fichiers/dossiers, et dans les fichiers eux-mêmes.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Commandes avancées
{{< /pcache >}}
### 2.8. D'autres commandes
`rsync` pour synchroniser des dossiers (programmes complexes mais très pratique).

`sed` pour manipuler du texte, commande puissante.

`&&` pour enchaîner les commandes.

`htop` pour savoir ce que fait votre ordinateur.

{{< pnote >}}

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 3. bash
Préparer une suite de commandes dans un fichier.
Exemple :

```
#!/bin/bash

# aller dans le dossier
cd notes
# créer un fichier qui va s'appeler note- suivi du numéro de la note que vous aurez indiqué dans le terminal
touch note-$1.txt

```
{{< psectiono >}}
