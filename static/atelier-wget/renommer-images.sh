#!/bin/bash

# Nous avons une liste de fichiers avec l'extension `.medres`,
# mais il serait plus pratique qu'ils soient nommés en fonction
# de leur type de fichier, dans ce cas-ci JPEG (`.jpg`)!
for i in *.medres; do
  echo "Renommage de $i -> $i.jpg";
  mv "$i" "$i.jpg";
done
