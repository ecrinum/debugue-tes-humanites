---
# Modèle de métadonnées pour une séance

# intitulé de la présentation
# @type chaîne de caractères libre
title: "Structure et architecture de données"

# courte description
# chaîne de caractères libre
description: "Transformer un tableau Excel dans un fichier en format CSV. Connaître les usages possibles des données structurées en CSV. Comprendre l’intérêt de structurer ses propres données."

# date de modification de la page (pour Hugo)
# @type chaîne de caractères contenant une date (format strict: "2006-01-02")
date: 2024-03-26

# heure de la présentation
# @type chaîne de caractères libre
heure_p: "13h-15h"

# lien de présentation (laisser vide pour "à venir" ou `false` pour aucun lien)
# @type booléen (false) | chaîne de caractères (vide, url)
lien: "https://meet.jit.si/DebogueHumanitesCRCEN-BLSH" # https://url-visio.example...

# lieu, nom du local
# @type chaîne de caractères libre
lieu: "Bibliothèque des lettres et sciences humaines, local 2074"

# liste des compétences acquises (phrases complètes)
# @type liste<chaîne de caractères>
competences:
- Transformer un tableau Excel dans un fichier en format CSV.
- Connaître les usages possibles des données structurées en CSV.
- Comprendre l’intérêt de structurer ses propres données.

# liste des formatrice·teur·s de la séance
# @type liste<chaîne de caractères>
formatrices:
- Louis-Olivier Brassard
- Giulia Ferretti

# numéro de la séance
# @type chaîne de caractères
seance: "08"

# saison, pour assurer l'archivage
# @type numéro
saison: 4

# Si la séance doit être listée ou non. La page sera tout de même générée.
# Notez que cela est différent de l'option `published` de Hugo,
# cette dernière permettant de régler la génération ou non de la page comme telle.
# @type booléen (true|false)
visible: true

# disposition style diapositives, voire `layouts/_default/diapositive.html`
# @type chaîne de caractères
layout: diapositive

# si les diapositives doivent figurer sur la page
# @type booléen (true|false)
afficher_diapositives: true

# si la boîte de présentation doit être ouverte par défaut ou non
# @type booléen (true|false) 
diapositives_ouvertes: true

# vignette de couverture (à fabriquer séparément, voire README.md)
# décommenter les deux lignes suivantes lorsque l'image a été générée
# @type liste
images:
- /images/feature/saison-04/seance-08-structure-et-architecture-de-donnees.png
---

<!-- Le corps de texte va ici. -->

<!-- Baliser les diapositives avec les shortcodes `{{< psectioni >}}` pour ouvrir et `{{< psectiono >}}` pour fermer. -->

{{< psectioni >}}
{{< pcache >}}
## Plan de la séance

1. Qu'est-ce qu'une « donnée structurée » ? Pourquoi est-ce utile ?
2. Le format CSV et Microsoft Excel (XLSX) : qu'est-ce qui est quoi ?
3. `csvkit` : le couteau suisse pour manipuler des tableaux CSV
{{< /pcache >}}

{{< pnote >}}
**Un mot sur les notions** : bien qu'il ne soit ne soit pas nécessaire d'assister aux séances précédentes de _Débogue tes humanités_, cette séance fera appel à certaines notions importantes : comprendre [les dossiers et les fichiers](/saison-04/seance-01-comprendre-l-ordinateur-avec-le-terminal/) sur son ordinateur (fondamental !), les [formats](/saison-04/seance-02-les-formats/), et même le [terminal](/saison-04/seance-01-comprendre-l-ordinateur-avec-le-terminal/) pour les plus aventureux·ses !

🧠 Rappelez-vous que nous ne cherchons pas à enseigner l'utilisation _d'outils spécifiques_, mais plutôt des stratégies qui vous permettent d'accroître votre **littéracie numérique** -- quels que soient les outils que vous utiliserez !
C'est comme **apprendre à cuisiner** : mieux vaut apprendre comment mélanger les ingrédients que d'apprendre à utiliser l'appareil d'une marque en particulier !
{{< /pnote >}}

{{< psectiono >}}


{{< psectioni >}}
## 1. Qu'est-ce qu'une « donnée structurée » ?

Une donnée structurée est une donnée balisée dans un **format** standardisé, ce qui lui permet d'être facilement lue par une machine ou un être humain.
On en retrouve dans :

📄 **les documents textuels :**
: _les titres de chapitres d'un roman ou les noms des personnages balisés dans une pièce de théâtre (ce qui permet de générer automatiquement une table des matières ou un index des personnages) ;_

☎️ **les carnets d'adresses :**
: _prénom, nom, numéro de téléphone, adresse courriel, etc. ;_

📷 **les appareils photo :**
: _la date de prise de vue, le profil de couleur, les coordonnées géographiques, etc. ;_

📚 **les catalogues de bibliothèque :**
: _elles permettant d'effectuer une recherche par titre, auteur, sujet, etc._

Bref, les données structurées sont partout et existent sous une myriade de formes !

{{< psectiono >}}


{{< psectioni >}}
### 1.2 Exemple de données structurées : les chansons en format numérique

Les catalogues de chansons regorgent de données structurées !
Celles-ci sont nécessaires pour permettre **l'affichage des bonnes informations** dans le lecteur de musique (comme la durée, le numéro de piste ou la pochette de l'album), **effectuer le tri** (par artiste, par album, par année) ou même **produire des recommandations** (en fonction du genre ou du nombre d'écoutes).

{{< psectiono >}}


{{< psectioni >}}
{{< figure
      src="/images/2024-03-26-donnees-structurees-chansons-liste.png"
      caption="Capture d'écran d'une liste de lecture. Les champs qui utilisent des données structurées ont été encadrés."
>}}
{{< psectiono >}}


{{< psectioni >}}

Vous pouvez vous-même inspecter les **métadonnées** d'un fichier audio en accédant à ses « propriétés », ou en l'ouvrant dans un logiciel dédié aux contenus multimédias.

{{< figure
      src="/images/2024-03-26-chanson-metadonnees.png"
      width="375"
      caption="Capture d'écran d'une fenêtre d'inspection des métadonnées d'une pièce musicale. Les informations de titre, album, éditeur, etc. peuvent être saisies dans des champs séparés."
>}}
{{< psectiono >}}


{{< psectioni >}}
### 1.3 Exemple de données structurées : les moteurs de recherche

Les **moteurs de recherche** construisent des modèles de représentation (_knowledge graphs_) de l'information.
C'est grâce à eux qu'ils peuvent produire des pages de résultats davantage structurées.

{{< figure
      src="/images/2024-03-26-recherche-donnee-structuree.png"
      width="550"
      caption="Capture d'écran d'une simple recherche en ligne. Les champs qui utilisent des données structurées ont été encadrés."
>}}
{{< psectiono >}}


{{< psectioni >}}
### 1.4 Exemple de données structurées : les publications scientifiques

La publication scientifique évolue dans un contexte de massification importante de l'information, et l'accès à une **information de qualité** devient un enjeu plus criant que jamais.

Il est crucial de veiller à ce que les ouvrages et articles soient facilement découvrables grâce à des données « propres » et surtout **bien structurées** : noms des auteurs et autrices, date de publication, mots-clefs, résumés -- sans ces informations (et bien d'autres !), un document a peu de chances d'être trouvé ou consulté.

{{< psectiono >}}


{{< psectioni >}}
{{< figure
      src="/images/2024-03-26-stylo-donnees-structurees.png"
      width="650"
      caption="Capture d'écran d'un article dans l'éditeur Stylo. Les métadonnées de l'article (auteur, résumé, mots-clefs, etc.) peuvent être saisies dans le panneau latéral de droite."
>}}
{{< psectiono >}}


{{< psectioni >}}
{{< figure
      src="/images/2024-03-26-article-scientifique-donnees-structurees.png"
      width="650"
      caption="Capture d'écran de la page couverture article scientifique en format PDF distribué par la plateforme Érudit. Le document contient de nombreuses données structurées qui facilitent son indexation et sa découvrabilité."
>}}
{{< psectiono >}}


{{< psectioni >}}

### 1.5 Les tableaux : une représentation couramment utilisée pour les données structurées

On retrouve souvent des ensembles de données structurées dans des **tableaux**.
Le type de donnée est alors défini par le **nom de la colonne** dans laquelle l'information se trouve.

{{< figure
    src="/images/2024-03-26-tableau-simple.png"
    caption="La première rangée d'un tableau est généralement son « en-tête », avec les noms de colonne."
>}}
{{< psectiono >}}


{{< psectioni >}}
## 2. Le format CSV et Microsoft Excel (XLSX) : qu'est-ce qui est quoi ?

Dans les formations de _Débogue tes humanités_, nous nous intéressons souvent aux **formats** de représentation des différentes données : texte / texte brut, HTML, XML, DOCX, PDF…

Ici, nous nous attardons en particulier à des formats pour les **données tabulaires**.

Comment procédez-vous pour traiter des tableaux ?
Quels outils utilisez-vous ?
{{< psectiono >}}


{{< psectioni >}}
### 2.1 Le format XLSX

Le format de fichier utilisé par le tableur Microsoft Excel fait l'objet d'un [standard officiel](https://www.iso.org/fr/standard/71691.html) (ISO/IEC 29500-1:2016), encadré par l'organisation internationale de standardisation (ISO). Et le document de cette spécification fait… plus de 5000 pages !

[De la même manière que DOCX](/saison-04/seance-02-les-formats/#32-docx) utilisé par le logiciel de traitement de texte Microsoft Word, un fichier XLSX n'est rien d'autre qu'un paquet `.ZIP` contenant de nombreux fichiers XML (il s'agit de la même spécification pour tous les fichiers de la suite Microsoft Office).

{{< pnote >}}
La documentation du format `.ZIP` (format cité dans la spécification Office Open XML) est considérable, même s'il n'est utilisé que pour « empaqueter » des fichiers :  
https://pkwaredownloads.blob.core.windows.net/pkware-general/Documentation/APPNOTE_6.2.0.TXT
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< figure
    src="/images/2024-03-26-xlsx-standards.png"
    width="50%"
    caption="Extrait du préambule de la spécification pour le format Office Open XML. De nombreux autres standards sont utilisés, comme XML et ZIP."
>}}
{{< psectiono >}}


{{< psectioni >}}
{{< figure
    src="/images/2024-03-26-xslx-deballe.png"
    width="80%"
    caption="Aperçu des fichiers constituant un document XLSX dans lequel on a écrit « Colonne A » dans une cellule."
>}}
{{< psectiono >}}


{{< psectioni >}}
Le logiciel Microsoft Excel s'avère pratique dans bien des cas (pour effectuer des transformations, des calculs complexes ou des tableaux croisés dynamiques), mais son format de fichier associé XLSX est-il toujours le meilleur pour **stocker** des données tabulaires ?

N'y aurait-il pas un format plus simple, compréhensible et pérenne ?
{{< psectiono >}}


{{< psectioni >}}
### 2.2 Le format CSV

CSV est l'acronyme de _Comma-Separated Values_ (« valeurs séparées par une virgule »).
Il s'agit d'un format **texte brut** qu'on peut ouvrir avec n'importe quel éditeur de texte (_Notepad_ sur Windows, _TextEdit_ sur macOS, _gedit_ sur Linux, et [une myriade d'autres](/saison-04/seance-07-editeurs-de-texte/)…).

Voici à quoi pourrait ressembler un fichier CSV\* :

```csv
Colonne A,  Colonne B,  Colonne C,  Colonne D
A,          B,          C,          D
A,          B,          C,          D
A,          B,          C,          D
```

<small>\* Des espaces ont été ajoutées à des fins d'alignement visuel.</small>
{{< psectiono >}}


{{< psectioni >}}
... et sa correspondance :

{{< figure
    src="/images/2024-03-26-tableau-simple.png"
    alt="La première rangée d'un tableau est généralement son « en-tête », avec les noms de colonne."
>}}
{{< psectiono >}}


{{< psectioni >}}
_Mais qu'arrive-t-il si une cellule contient une virgule ?!_

**Pas de problème !**
Dans les faits, un fichier `.csv` ressemble plus souvent à ceci, avec des guillemets droits `" "` qui encadrent le texte de chaque cellule pour éviter les erreurs de délimitation :

```csv
"Colonne A","Colonne B","Colonne C","Colonne D"
"A","B","C","D"
"A","B","C","D"
"A","B","C","D"
```
{{< psectiono >}}


{{< psectioni >}}
### 2.3 Créez (facilement !) votre premier tableau CSV !

Essayez d'exporter un tableau en format CSV depuis votre logiciel de tableur préféré.

Ce format apparaîtra peut-être sous le libellé « Texte (délimité par des virgules) », « Texte CSV » ou autre dans le menu déroulant des formats de sauvegarde.

{{< pnote >}}
Lorsque vous enregistrez un fichier au format texte brut, vous pouvez choisir les caractères de délimitation. Pour le format CSV, utilisez la virgule comme séparateur de champ. Pour l'encodage des caractères, vous choisirez généralement UTF-8 (Microsoft Excel utilisera un autre encodage par défaut, selon votre région -- notamment, ANSI / Windows-1252 pour les anglophones d'Amérique du Nord, ISO-8859-1 pour l'Europe occidentale -- mais cela est configurable au moment de de l'export).
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< figure
    src="/images/2024-03-26-enregistrer-sous-csv.png"
    caption="Boîte de dialogue lors de l'enregistrement au format texte (CSV) dans le logiciel LibreOffice."
>}}
{{< psectiono >}}


{{< psectioni >}}

Enregistrez votre tableau dans un dossier que vous créerez pour cet exercice (rappel : il est important de [comprendre l'emplacement des fichiers](/saison-04/seance-01-comprendre-l-ordinateur-avec-le-terminal/) sur son ordinateur !) et essayez de l'ouvrir avec votre éditeur de texte préféré, ou encore avec le terminal :

{{< highlight bash "linenos=false" >}}
# Dans un terminal, déplacez-vous d'abord dans le
# répertoire pour votre exercice, par exemple:
#   cd ~/Documents/exercice-csv

# Ensuite, examinez le contenu de votre fichier CSV
# (remplacez "mon-tableau.csv" par le nom de votre fichier)
cat mon-tableau.csv

# (le contenu de votre fichier s'affichera ici!)
{{< /highlight >}}
{{< psectiono >}}


{{< psectioni >}}
**Bravo !**
Vous avez créé votre premier fichier CSV.
Ce type de fichier, comme vous l'avez constaté, est **extrêmement simple** et permet de stocker une grande quantité de données tabulaires.

C'est un format qui permet une **interopérabilité significative** entre des systèmes d'information : il est beaucoup plus facile d'importer/exporter des données en format CSV.

Des services populaires comme Airtable ou Notion permettent d'importer ou d'exporter ses données en format CSV.
On peut ensuite facilement les intégrer dans un tableur, ou en faire autre chose, comme des visualisations de données !
{{< psectiono >}}


{{< psectioni >}}
### 2.4 Un exemple d'utilisation de données CSV : les actes criminels sur le territoire de la ville de Montréal

Le jeu de données est accessible librement [sur le site](https://www.donneesquebec.ca/recherche/dataset/vmtl-actes-criminels) de Données Québec.

{{< figure
    src="/images/2024-03-26-tableau-donnees-structurees.png"
    caption="Exemple de tableau : crimes répertoriés sur le territoire du service de police de la ville de Montréal."
>}}
{{< psectiono >}}


{{< psectioni >}}
{{< figure
    src="/images/2024-03-26-csv-carte-rstudio.png"
    caption="Visualisation des données tabulaires avec le langage de programmation R (dans RStudio). D'après un atelier préparé par Lisa Theichmann."
>}}
{{< psectiono >}}


{{< psectioni >}}
## 3. csvkit: le couteau suisse pour manipuler des fichiers CSV

Pour clore cette formation, nous vous proposons la démonstration d'un outil très puissant, qu'on peut utiliser dans la ligne de commande : [`csvkit`](https://csvkit.readthedocs.io/).

Il s'agit en réalité d'une boîte contenant plusieurs outils, qu'on peut utiliser séparément ou conjointement en combinant les commandes.
{{< psectiono >}}


{{< psectioni >}}
### 3.1 Installation de csvkit

Le logiciel est écrit en langage Python (version 3) ; ce langage doit être installé sur votre poste pour que vous puissiez utiliser `csvkit`.

<details>
<summary>Python est déjà installé sur votre machine</summary>

Si Python est installé sur votre machine, vous pouvez installer `csvkit` avec le gestionnaire de paquets Python `pip` :

{{< highlight bash "linenos=false" >}}
#!/bin/bash

sudo pip install csvkit
{{< /highlight >}}

{{< pnote >}}
Le préfixe `sudo` dans la commande précédente vous permet d'installer le logiciel sur votre système (au lieu de simplement le télécharger pour un projt isolé).
Votre mot de passe d'utilisateur (sur votre poste) vous sera demandé, et il est possible que les caractères ne s'affichent pas lorsque vous le tapez (c'est normal !).
{{< /pnote >}}

</details>

<details>
<summary>Python n'est pas installé sur votre machine</summary>

Cela dépasse le cadre de cet atelier : rendez-vous sur le [site web de Python](https://www.python.org/) pour en savoir plus.

</details>

{{< psectiono >}}


{{< psectioni >}}
### 3.2 Premiers pas avec csvkit

`csvkit` propose un excellent [tutoriel](https://csvkit.readthedocs.io/en/latest/tutorial.html) pour débuter facilement.

Quelques commandes couramment utilisées :

`csvcut` ✂️ :
: L'utilitaire original qui a donné lieu à la suite d'outils. On s'en sert pour découper une partie du fichier -- comme si on prenait une paire de ciseaux.

`csvstat` 📊 :
: Permet de résumer les colonnes d'un fichier et d'en produire des statistiques.

`csvlook` 👀 :
: Un « périscope » pour examiner le contenu d'un fichier CSV (très utile lorsque combiné avec la commande `less -S` !)
{{< psectiono >}}


{{< psectioni >}}
### 3.3 Un jeu de données pour expérimenter

Il nous faudrait des données à nous mettre sous la dent !
Utilisons un outil [que nous connaissons bien](/saison-04/seance-04-zotero-avance/) : le gestionnaire de références bibliographiques Zotero.

Nous allons exporter une collection en sélectionnant CSV comme format de sortie.
Nous sauvegarderons le fichier sous le nom de `bibliotheque.csv` (c'est notre bibliothèque de références) dans le répertoire que nous avons créé pour cet atelier.

{{< highlight bash "linenos=false" >}}
# répertoire pour cet exercice

exercice-csv/
├── bibliotheque.csv # <-- nous allons utiliser ce fichier!
├── mon-tableau.csv
└── mon-tableau.xlsx
{{< /highlight >}}
{{< psectiono >}}


{{< psectioni >}}
Que contient notre fichier `bibliotheque.csv` ?

Dans le terminal, on peut « découper » les en-têtes de colonnes :
{{< highlight bash "linenos=false" >}}
# bash

csvcut --names bibliotheque.csv
{{< /highlight >}}
{{< psectiono >}}


{{< psectioni >}}
On pourrait produire des statistiques sur certaines colonnes (avec le drapeau `--columns` ou `-c`), comme le type de document, dénoté par le champ `"Item Type"` :

{{< highlight bash "linenos=false" >}}
# bash

csvstat --columns "Item Type" bibliotheque.csv
{{< /highlight >}}
{{< psectiono >}}


{{< psectioni >}}
Visualisons le fichier directement dans le terminal, en combinant la commande avec l'utilitaire `less` :

- l'argument `-S` permet d'éviter les retours à la ligne et de défiler avec les flèches du clavier ;
- l'argument `-#` permet de régler la « vitesse » du défilement (`2`, `6`, `10` ...) lorsqu'on appuie sur une touche.


{{< highlight bash "linenos=false" >}}
# bash

csvlook bibliotheque.csv | less -S -#2
{{< /highlight >}}
{{< psectiono >}}


{{< psectioni >}}
Il y a tant de choses que la boîte à outils de `csvkit` permet de faire.
Suivez leur [tutoriel](https://csvkit.readthedocs.io/en/latest/tutorial.html) ou référez-vous à la [documentation](https://csvkit.readthedocs.io/) pour en savoir plus!
{{< psectiono >}}


{{< psectioni >}}
## Conclusion

Nous espérons que vous aurez compris l'atout d'utiliser le format CSV pour stocker vos données tabulaires.
La simplicité, la malléabilité et l'interopérabilité de ce format en font un candidat idéal pour vos projets, petits ou grands, et ses applications sont très nombreuses !
C'est un format texte brut facile à lire et facile à archiver, ce qui en fait un gage de pérennité.
{{< psectiono >}}
