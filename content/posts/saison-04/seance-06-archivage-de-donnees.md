---
# Modèle de métadonnées pour une séance

# intitulé de la présentation
# @type chaîne de caractères libre
title: "Archivage de données"

# courte description
# chaîne de caractères libre
description: "Le Web et les technologies informatiques en général sont en constante évolution. Comment préserver les données qui sont importantes pour notre recherche ou notre activité professionelle ? Au cours de cette session, nous présenterons des stratégies et des bonnes pratiques pour la préservation des données."

# date de modification de la page (pour Hugo)
# @type chaîne de caractères contenant une date (format strict: "2006-01-02")
date: 2024-02-27

# heure de la présentation
# @type chaîne de caractères libre
heure_p: "13h-15h"

# lien de présentation (laisser vide pour "à venir" ou `false` pour aucun lien)
# @type booléen | chaîne de caractères
lien: "https://meet.jit.si/DebogueHumanitesCRCEN-BLSH" # https://url-visio.example...

# lieu, nom du local
# @type chaîne de caractères libre
lieu: "Bibliothèque des lettres et sciences humaines, local 2074"

# liste des compétences acquises (phrases complètes)
# @type liste<chaîne de caractères>
competences:
- "Stocker une page web localement, sur son propre ordinateur"
- "Connaître différentes stratégies pour préserver des données localement"
- "Comprendre l'importance de réfléchir à la préservation des données de recherche"

# liste des formatrice·teur·s de la séance
# @type liste<chaîne de caractères>
formatrices:
- Louis-Olivier Brassard
- Giulia Ferretti

# numéro de la séance
# @type chaîne de caractères
seance: "06"

# saison, pour assurer l'archivage
# @type numéro
saison: 4

# Si la séance doit être listée ou non. La page sera tout de même générée.
# Notez que cela est différent de l'option `published` de Hugo,
# cette dernière permettant de régler la génération ou non de la page comme telle.
# @type booléen (true|false)
visible: true

# disposition style diapositives, voire `layouts/_default/diapositive.html`
# @type chaîne de caractères
layout: diapositive

# si les diapositives doivent figurer sur la page
# @type booléen (true|false)
afficher_diapositives: true

# si la boîte de présentation doit être ouverte par défaut ou non
# @type booléen (true|false) 
diapositives_ouvertes: true

# vignette de couverture (à fabriquer séparément)
images:
- /images/feature/saison-04/seance-06-archivage-de-donnees.png
---

<!-- Le corps de texte va ici. -->

<!-- Baliser les diapositives avec les shortcodes `{{< psectioni >}}` pour ouvrir et `{{< psectiono >}}` pour fermer. -->

{{< psectioni >}}

{{< pcache >}}
## Plan de la séance

0. Introduction
1. Qu’est-ce que Wget ?
2. Installation
3. Premier téléchargement avec Wget
4. Télécharger des images en vrac avec Wget
5. Archiver des documents web avec Wget
6. Conclusion
{{< /pcache >}}

{{< psectiono >}}


{{< psectioni >}}
## 0. Introduction

Internet donne accès à une quaisi-infinité de contenus, disponibles sur demande vingt-quatre heures par jour, chaque jour de l'année. Comme c'est pratique !

**... jusqu'au jour où ce n'est plus vrai** : bande passante au ralenti ou connexion coupée, site web en maintenance ou hors service, archives disponibles uniquement par VPN ou depuis une adresse autorisée, etc. Les raisons deviennent soudainement de plus en plus nombreuses d'assurer un accès aux ressources dont on a besoin pour travailler, comme une base de données, des images précises ou des textes particuliers. Bref, il faut une <mark>stratégie d'archivage</mark>.

Pour y parvenir, nous vous proposons aujourd'hui un outil : `wget` !
{{< psectiono >}}


{{< psectioni >}}
## 1. Qu'est-ce que Wget ?

`wget` est un logiciel qui fonctionne en **ligne de commande**.
Il permet d'effectuer des téléchargements avec diverses options et protocoles.
Nous verrons en quoi ces options peuvent être utiles !

Nous allons nous en servir aujourd'hui pour archiver certains objets d'étude disponibles en ligne, mais que nous souhaitons pouvoir consulter **hors connexion**.

{{< pnote >}}
Pour un rappel sur le fonctionnement du **terminal** (l'interface en ligne de commande), jetez un coup d'œil à la [**séance 01**](/saison-04/seance-01-comprendre-l-ordinateur-avec-le-terminal/) de la saison 4.

Une bonne compréhension de la notion de **fichiers** et de **dossiers** est également essentielle !
{{< /pnote >}}

{{< psectiono >}}


{{< psectioni >}}
## 2. Installation

Si vous utilisez macOS ou Linux, `wget` est probablement déjà installé.
Vous pouvez vous en assurer en ouvrant une fenêtre de terminal et en entrant :

{{< highlight bash "linenos=false" >}}
# cela affichera le message d'aide de wget
wget --help
{{< /highlight >}}

**Sur Windows**, vous pouvez utiliser [Cygwin](https://cygwin.com/) (émulateur de commandes Unix pour Windows) ou [Ubuntu bash sur Windows 10](https://www.howtogeek.com/249966/how-to-install-and-use-the-linux-bash-shell-on-windows-10/) pour avoir accès à `wget`.
Vous pouvez aussi télécharger `wget` directement [depuis ce site](https://eternallybored.org/misc/wget/) qui en fait la distribution.

<details>
<summary>Installation sur Linux</summary>

{{< highlight bash "linenos=false" >}}
sudo apt install wget
{{< /highlight >}}
</details>

<details>
<summary>Installation sur macOS</summary>

{{< highlight bash "linenos=false" >}}
# avec Homebrew
brew install wget
{{< /highlight >}}
</details>

{{< psectiono >}}


{{< psectioni >}}
## 3. Premier téléchargement avec Wget

Nous allons effectuer un premier téléchargement tout simple avec `wget`.

Par exemple, nous allons archiver l'image d'une toile de l'artiste suprématiste Kasimir Malevitch sur Wikimedia Commons.

Sur la [page de l'image Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Amsterdam_-_Stedelijk_Museum_-_Kazimir_Malevich_(1878-1935)_-_Suprematist_Painting_(with_Black_Trapezium_and_Red_Square)_(A_7681)_1915.jpg?uselang=fr), nous allons cliquer sur le bouton « **Télécharger** ».
Dans la boîte de dialogue, plusieurs champs s'offrent à nous :

- l'URL de la **page**, qui est la même que celle qui figure dans la barre d'adresse du navigateur ;
- l'URL du **fichier**, qui est véritablement celle qui nous intéresse ;
- le crédit de l'auteur du fichier, qu'on peut copier en texte brut ou en format HTML.

{{< psectiono >}}


{{< psectioni >}}

{{< figure src="/images/2024-02-27-wikimedia-commons-boite-dialogue-image.png" caption="Boîte de dialogue d'un fichier sur Wikimédia Commons" >}}

{{< psectiono >}}


{{< psectioni >}}

Dans la boîte de dialogue, copier l'**URL du fichier**, puis le coller dans le terminal, en remplaçant `url-du-fichier` par la véritable URL :

{{< highlight bash "linenos=false" >}}
wget url-du-fichier

# ...

# si tout s'est bien passé, wget devrait avoir téléchargé
# l'image dans le répertoire courant!
{{< /highlight>}}

{{< psectiono >}}


{{< psectioni >}}

Nous pouvons examiner le résultat avec la commande `ls` :

{{< highlight bash "linenos=false" >}}
ls

# la console devrait retourner notre fichier téléchargé
# -> Amsterdam_-_Stedelijk_Museum_-_Kazimir_Malevich_...
{{< /highlight >}}

Et la même chose dans notre explorateur de fichiers :

{{< figure src="/images/2024-02-26-liste-fichiers.png" caption="Le fichier que nous venons de télécharger avec Wget" >}}

{{< psectiono >}}


{{< psectioni >}}
**Bravo !** Vous avez fait votre premier téléchargement avec `wget` !

... mais il est vrai que c'est assez peu excitant (nous aurions pu tout simplement sauvegarder l'image directement depuis la page de Wikmédia Commons).

Examinons un cas où `wget` s'avérera encore plus utile !
{{< psectiono >}}


{{< psectioni >}}
## 4. Télécharger des images en vrac avec Wget
### 4.1 Une image unique sur Gallica

De nombreux objets d'étude, comme un **livre** ou un **journal**, consistent en une série de plusieurs numérisations -- une image pour chaque page numérisée. Nous pouvons tirer parti de ce découpage sur des plateformes comme Gallica pour archiver les pages qui nous intéressent !

Comment pourrions-nous numériser les dix premières pages du roman *Une saison en enfer* d'Arthur Rimbaud, [accessible librement](https://gallica.bnf.fr/ark:/12148/btv1b86108277) grâce à la plateforme de la <abbr title="Bibliothèque Nationale de France">BNF</abbr> ?

{{< psectiono >}}


{{< psectioni >}}

{{< figure src="/images/2024-02-27-wget-zuckerberg-meme.png" caption="Dans le film *The Social Network* (2010), le personnage de Mark Zuckerberg mentionne l'outil Wget pour récupérer les photos du serveur de son université." >}}

{{< pnote >}}
Bien sûr, l'utilisation d'un outil comme `wget` devrait toujours respecter les lois, règlements et principes du savoir-vivre. Les agissements du créateur de Facebook demeurent en ce sens discutables et sont utilisés à des fins d'exemple seulement.
{{< /pnote >}}

{{< psectiono >}}


{{< psectioni >}}

{{< figure src="/images/2024-02-27-gallica-rimbaud-p13.png" caption="Une saison en enfer d'Arthur Rimbaud, affichage d'un exemplaire disponible sur Gallica." >}}

{{< psectiono >}}


{{< psectioni >}}
Un clic droit sur l'image permet d'accéder à l'URL de l'image via le menu contextuel.

Dans cet exemple, la nouvelle commence à partir du 13<sup>e</sup> folio numérisé.

Vous pouvez **copier** cette URL pour effectuer le téléchargement avec `wget` :

{{< highlight bash "linenos=false" >}}
# créez-vous un dossier de travail et naviguez-y
# 1. creation du dossier
mkdir mon-dossier-pour-apprendre-wget

# 2. se déplacer dans le dossier
cd mon-dossier-pour-apprendre-wget

# on récupère l'image du folio no. 13 avec la commande wget
wget https://gallica.bnf.fr/ark:/12148/btv1b86108277/f13.highres
{{< /highlight >}}

{{< psectiono >}}


{{< psectioni >}}
### 4.3 Télécharger une liste d'images

Nous voulons archiver les **10 premières pages** du livre. Nous pourrions poursuivre en répétant la commande `wget`, mais en incrémentant le numéro de folio (`13`, `14`, ...), à la fin de l'URL (`...f13.highres`) :

{{< highlight bash "linenos=false" >}}
wget https://gallica.bnf.fr/ark:/12148/btv1b86108277/f14.highres

wget https://gallica.bnf.fr/ark:/12148/btv1b86108277/f15.highres

wget https://gallica.bnf.fr/ark:/12148/btv1b86108277/f16.highres

# ... et ainsi de suite
{{< /highlight >}}

{{< psectiono >}}


{{< psectioni >}}
Cela est un peu redondant ! Nous pourrions automatiser ces opérations répétitives afin de lancer le téléchargement en arrière-plan, ce qui nous permettrait de faire autre chose pendant ce temps -- une façon judicieuse de gagner son temps, surtout pour des téléchargements très longs avec de nombreux fichiers !

Nous allons mettre à profit ce que nous avons appris avec la notion de **texte brut**, ou le format `.txt`.

`wget` peut lire une liste d'URLs **à partir d'un fichier**. On pourrait donc créer un fichier `liste-urls.txt` qui ressemblerait à ceci :

{{< highlight txt >}}
http://exemple.com/image1.jpg
http://exemple.com/image2.jpg
http://exemple.com/image3.jpg
...
{{< /highlight >}}

{{< psectiono >}}


{{< psectioni >}}
Pour lancer le téléchargement de cette liste d'images inscrite dans le fichier, on peut utiliser l'option `-i` (ou `--input-file`) de `wget` :

{{< highlight bash "linenos=false" >}}
# Pour lancer le téléchargement à partir d'une liste dans un fichier

wget -i liste-urls.txt
{{< /highlight >}}

{{< pnote >}}
**Attention !** On se rappelle que le format Microsoft Word `.docx` n'est pas un format texte, mais un format binaire, dans lequel le texte ne peut pas être lu directement. Le format texte brut (ou format texte, tout court) ne contient rien d'autre que le texte qu'on tape dans un éditeur tel que Notepad, TextEdit ou Vim.
{{< /pnote >}}

{{< psectiono >}}


{{< psectioni >}}
Si cette liste est très longue, elle pourrait s'avérer fastidieuse à faire à la main, et nous pourrions risquer d'y introduire des erreurs. Un script pourrait nous aider à générer la liste de manière rapide et fiable !

Si vous avez `python` installé sur votre appareil, essayez l'étape suivante !
{{< psectiono >}}


{{< psectioni >}}
### 4.3 Utilisation d'un script python (optionnel)

Créez un fichier `generateur-urls.py`. Remplissez-le avec le bloc de code suivant.
{{< psectiono >}}


{{< psectioni >}}

{{< highlight python >}}
#!/usr/bin/python
# -*- coding: utf-8 -*-

# Liste des urls (séparées par des sauts de ligne `\n`),
# qu'on va écrire ligne par ligne dans le fichier `urls.txt`.
urls = '';

# L'URL de base, avec un jeton `%d` à remplacer par un numéro de
# folio. Le suffixe `.medres` permet d'avoir une image en
# résolution moyenne, bien suffisant pour nos besoins.
# (On pourrait aussi utiliser les extensions `.highres` ou `.lowres`)
baseUrl = 'https://gallica.bnf.fr/ark:/12148/btv1b86108277/%d.medres';

# on ouvre le fichier `liste-urls.txt` pour pouvoir écrire dedans pendant
# la boucle (ne pas oublier de le fermer à la fin)
fichier = open('liste-urls.txt', 'w')

# Itération sur un intervalle de pages avec la variable `x`
# ici, les folios 13 à 23 (il faut inclure la borne supérieure)
for x in range(13, 24):
    # on crée une URL à partir de l'itération, en remplaçant le jeton `%d`
    # par la valeur actuelle de `x`
    url = baseUrl % (x)
    # et on ajoute un saut de ligne à la fin de chaque URL lorsqu'on écrit
    # dans le fichier
    fichier.write(url + '\n')

# Lorsque la boucle est terminée, on ferme le fichier
fichier.close
{{< /highlight >}}

{{< psectiono >}}


{{< psectioni >}}
Il ne reste plus qu'à lancer le script :

{{< highlight bash "linenos=false" >}}
# ce script devrait remplir le fichier `liste-urls.txt`
python generateur-urls.py
{{< /highlight >}}

On a ainsi un fichier `liste-urls.txt` rempli avec tous les folios que nous voulons télécharger.

{{< highlight bash "linenos=false" >}}
# on peut examiner le contenu du fichier avec la commande `cat`
cat liste-urls.txt
{{< /highlight >}}
{{< psectiono >}}


{{< psectioni >}}
Lançons à présent le téléchargement de nos pages avec `wget` :

{{< highlight bash "linenos=false" >}}
# la commande wget, avec quelques options
wget \
  -i liste-urls.txt \
  --limit-rate=100k \
  --directory-prefix=rimbaud-une-saison-en-enfer \
  --continue

# ... 
# pour chaque fichier, wget affichera un état d'avancement
{{< /highlight >}}

{{< psectiono >}}


{{< psectioni >}}
Et voilà le résultat final : un répertoire avec nos images archivées, juste pour nous !

{{< highlight txt "linenos=false" >}}
tree .
.
├── generateur-urls.py
├── liste-urls.txt
└── rimbaud-une-saison-en-enfer/
    ├── 13.medres
    ├── 14.medres
    ├── 15.medres
    ├── 16.medres
    ├── 17.medres
    ├── 18.medres
    ├── 19.medres
    ├── 20.medres
    ├── 21.medres
    ├── 22.medres
    └── 23.medres
{{< /highlight >}}
{{< psectiono >}}

{{< psectioni >}}
{{< figure src="/images/2024-02-27-wget-resultat-images-rimbaud.png" caption="Le même résultat, dans l'exploratur de fichiers (interface graphique)." >}}
{{< psectiono >}}

{{< psectioni >}}
## 5. Archiver des documents web avec Wget

On peut utiliser `wget` pour sauvegarder une page web, ou même un site web au complet !

L'option `--mirror` (ou `-m`) peut justement être pratique pour créer une « **copie miroir** » d'un site web localement sur sa machine.
{{< psectiono >}}


{{< psectioni >}}
### 6. Archiver une page web

Télécharger une page html avec `wget`. Ajouter l'option `-P` (ou `--directory-prefix`) pour indiquer le dossier dans lequel la page doit être enregistrée.
`-O` (ou `--output-document`) pour changer le nom du fichier.
`--adjust-extension` pour ajouter une extension appropriée aux fichiers téléchargés (cela s'applique principalement aux pages HTML).

{{< highlight bash "linenos=false" >}}
wget \
  https://fr.wikipedia.org/wiki/Orang-outan \
  --directory-prefix=dossier-sur-les-orangs-outans
  --output-document=page-wikipedia.html \
  --adjust-extension \
  --limit-rate=200k
{{< /highlight >}}
{{< psectiono >}}


{{< psectioni >}}
### 6.1 Archiver uniquement certains fichiers d'un site web
Télécharger uniquement les fichiers d'un format donné à partir d'un site web. L'option `-c` ou `--continue` permet de continuer un téléchargement interrompu.

{{< highlight bash "linenos=false" >}}
wget https:/skhole.ecrituresnumeriques.ca/ \
  -r \
  -l2 \
  -nd \
  -A.mp3 \
  -c \
  -P archives-audio-skhole
{{< /highlight >}}
{{< psectiono >}}


{{< psectioni >}}
### 7. Archiver un site web au complet
Télécharger un site web au complet. (Attention : l'option `--mirror` n'est pas toujours autorisée.)

{{< highlight bash "linenos=false" >}}
wget [URL] \
  --mirror \
  --convert-links \
  --adjust-extension \
  --page-requisites \
  --no-parent \
  --limit-rate=200k
{{< /highlight >}}

{{< psectiono >}}


{{< psectioni >}}
## 8. Conclusion

Nous espérons que vous avez compris l'utilité de l'outil `wget` et qu'il vous aidera à mettre en place vos propres stratégies d'archivage. Vous aurez compris à la lumière de cet atelier qu'il peut être judicieux de combiner `wget` avec un langage de programmation, comme `python` ou `bash`. Ce n'est qu'une pièce parmi tant d'autres dans votre boîte à outils !
{{< psectiono >}}
