---
# Modèle de métadonnées pour une séance

# intitulé de la présentation
# @type chaîne de caractères libre
title: "Zotero avancé"

# courte description
# chaîne de caractères libre
description: "Organiser sa bibliographie comme un·e pro."

# date de modification de la page (pour Hugo)
# @type chaîne de caractères contenant une date (format strict: "2006-01-02")
date: 2024-01-30

# heure de la présentation
# @type chaîne de caractères libre
heure_p: "13h-15h"

# lien de présentation (laisser vide pour "à venir" ou `false` pour aucun lien)
# @type booléen | chaîne de caractères
lien: "https://meet.jit.si/DebogueHumanitesCRCEN-BLSH" # https://url-visio.example...

# lieu, nom du local
# @type chaîne de caractères libre
lieu: "Bibliothèque des lettres et sciences humaines, local 2074"

# liste des compétences acquises (phrases complètes)
# @type liste<chaîne de caractères>
competences:
- "Organiser sa montagne de références sans peine."

# liste des formatrice·teur·s de la séance
# @type liste<chaîne de caractères>
formatrices:
- Pascal Martinolli

# numéro de la séance
# @type chaîne de caractères
seance: "04"

# saison, pour assurer l'archivage
# @type numéro
saison: 4

# Si la séance doit être listée ou non. La page sera tout de même générée.
# Notez que cela est différent de l'option `published` de Hugo,
# cette dernière permettant de régler la génération ou non de la page comme telle.
# @type booléen (true|false)
visible: true

# disposition style diapositives, voire `layouts/_default/diapositive.html`
# @type chaîne de caractères
layout: diapositive

# si les diapositives doivent figurer sur la page
# @type booléen (true|false)
afficher_diapositives: true

# si la boîte de présentation doit être ouverte par défaut ou non
# @type booléen (true|false) 
diapositives_ouvertes: true

# vignette de couverture (à fabriquer séparément)
images:
- /images/feature/saison-04/seance-04-zotero-avance.png
---

<!-- Le corps de texte va ici. -->

<!-- Baliser les diapositives avec les shortcodes `{{< psectioni >}}` pour ouvrir et `{{< psectiono >}}` pour fermer. -->


{{< psectioni >}}

# Débogue mon Zotero


Pascal Martinolli
Bibliothécaire et zoteromane depuis 2011 
Bibliothèque des lettres et sciences humaines, UdeM 
pascal.martinolli@umontreal.ca 

{{< psectiono >}}


{{< psectioni >}}

## Quoi ?

- [Logiciel libre](https://www.zotero.org/), gratuit et open-source
	- Linux, PC, Mac
	- Dialogue avec LibreOffice et MS Word 
	- Échange en format TeX 
- Plusieurs formes :
	- Fonctionnalités complètes : Logiciel installé 
	- Fonctionnalités limitées : 
		- Site web 
		- Version light : [ZoteroBib](https://zbib.org/) 
		- Google Doc,... 
- Version 7 bientôt 		

{{< psectiono >}}	


{{< psectioni >}}

## Pourquoi ?

- Gestion bibliographique :
	- Rédige automatiquement des bibliographie et des références dans le texte
	- Gère des collections de références (individuel ou collaboratif)
	- Partage de références et de notes 

{{< psectiono >}}


{{< psectioni >}}

## Qui ? 

- [Corporation for Digital Scholarship](https://digitalscholar.org/)
- International, multilingue
- Grosse communauté d'utilisateurs et de développeurs : 
	- Bientôt : version 7 (plus rapide)
	- Extensions 
	- Support et formation Bib/UdeM 
	- Blogue [Zotero francophone](https://zotero.hypotheses.org/) + [Doc-fr](https://docs.zotero-fr.org/) 

{{< psectiono >}}


{{< psectioni >}}

## Bibliographie

Trois mouvements : 

1. Créer une référence dans le logiciel (méthodes multiples)
2. Vérifier et enrichir la référence créée
3. Citer la référence
	- Méthode simple (de type copier-coller)
	- Méthode avancée (lien dynamique entre le traitement de texte et Zotero)

{{< psectiono >}}


{{< psectioni >}}

## Gestion des références

- [Organisation](https://zotero.hypotheses.org/3298) :
	- Collections
	- Marqueurs
		- Mon [thésaurus personnel](https://github.com/pmartinolli/TM-MyThesaurus)
	- Recherches enregistrées
	- Doublons
	- Connexe
- Prise de notes :
	- Notes filles
	- Extraction de notes de PDF
	- Notes indépendantes

{{< psectiono >}}


{{< psectioni >}}

## Ouverture 

- Collaboration
	- Bibliothèques de groupe 
- Partage et diffusion
	- Via Zotero
	- Via [page web fixe](https://pmartinolli.github.io/academicTTRPG/) + javascript [GH](https://github.com/pmartinolli/academicTTRPG) 
	- Via site web dynamique (application [Kerko](https://github.com/whiskyechobravo/kerko) par ex.) 
- Formats ouverts  

{{< psectiono >}}


{{< psectioni >}}

## Limites de Zotero

- Multiremplacements difficiles 
- Prise de notes : centrée sur les références et non sur les idées 

{{< psectiono >}}


{{< psectioni >}}

## Bonus avancés 

- Extensions : [Zutilo](https://github.com/wshanks/Zutilo), [ZotFile](http://zotfile.com/), [ZoteroPreview](https://github.com/dcartertod/zotero-plugins)
- Intégrité : Retraction Watch (inclus), [ZPubPeer](https://github.com/PubPeerFoundation/pubpeer_zotero_plugin), [ZScite](https://github.com/scitedotai/scite-zotero-plugin),
- Modifier un style bibliographique [CSLeditor](https://editor.citationstyles.org/visualEditor/)
- [Cita](https://github.com/diegodlh/zotero-cita) et Wikidata
- Et vous ? Vos extensions favorites ? 

{{< psectiono >}}


{{< psectioni >}}

## Zotero : ouvert ET interopérable 

- [*file over app*](https://stephango.com/file-over-app) : « Les fichiers au centre des méthodes de travail (et non les programmes) »
- Better BibTeX : fichier BibTeX ou CSL JSON 
	- Zettlr : rédiger / compiler 
	- Obsidian : organiser / lier 
	- Tutoriels de [Bibliopea](https://bibliopea.hypotheses.org/1)

{{< psectiono >}}


{{< psectioni >}}

## ZoteroRnalysis

- Analyser une bibliothèque Zotero (csv) avec R Studio
- https://github.com/pmartinolli/ZoteroRnalysis 
- Réconciliation de données avec OpenRefine et Wikidata

{{< psectiono >}}

