---
# Modèle de métadonnées pour une séance

# intitulé de la présentation
# @type chaîne de caractères libre
title: "Git – Archivage et partage des données"

# courte description
# chaîne de caractères libre
description: "Le versionnement des documents peut souvent être fastidieux et générer des erreurs ou des pertes de données. Pour vous aider à organiser vos fichiers, nous vous présenterons le protocole de versionnement Git, ainsi que l'une de ses applications les plus utiles."

# date de modification de la page (pour Hugo)
# @type chaîne de caractères contenant une date (format strict: "2006-01-02")
date: 2024-02-13

# heure de la présentation
# @type chaîne de caractères libre
heure_p: "13h-15h"

# lien de présentation (laisser vide pour "à venir" ou `false` pour aucun lien)
# @type booléen | chaîne de caractères
lien: "https://meet.jit.si/DebogueHumanitesCRCEN-BLSH" # https://url-visio.example...

# lieu, nom du local
# @type chaîne de caractères libre
lieu: "Bibliothèque des lettres et sciences humaines, local 2074"

# liste des compétences acquises (phrases complètes)
# @type liste<chaîne de caractères>
competences:
- "Utilisation basique de GitHub pour gérer et partager des projets en conservant l'historique des modifications"
- "Comprendre le protocole Git"
- "Paramétrer son application Git (avec une clé SSH)"

# liste des formatrice·teur·s de la séance
# @type liste<chaîne de caractères>
formatrices:
- Louis-Olivier Brassard
- Giulia Ferretti

# numéro de la séance
# @type chaîne de caractères
seance: "05"

# saison, pour assurer l'archivage
# @type numéro
saison: 4

# Si la séance doit être listée ou non. La page sera tout de même générée.
# Notez que cela est différent de l'option `published` de Hugo,
# cette dernière permettant de régler la génération ou non de la page comme telle.
# @type booléen (true|false)
visible: true

# disposition style diapositives, voire `layouts/_default/diapositive.html`
# @type chaîne de caractères
layout: diapositive

# si les diapositives doivent figurer sur la page
# @type booléen (true|false)
afficher_diapositives: true

# si la boîte de présentation doit être ouverte par défaut ou non
# @type booléen (true|false) 
diapositives_ouvertes: true

# vignette de couverture (à fabriquer séparément)
images:
- /images/feature/saison-04/seance-05-git-archivage-et-partage-des-donnees.png
---

<!-- Le corps de texte va ici. -->

<!-- Baliser les diapositives avec les shortcodes `{{< psectioni >}}` pour ouvrir et `{{< psectiono >}}` pour fermer. -->


{{< psectioni >}}
{{< pcache >}}
## Plan de la séance

0. Introduction
1. Qu’est-ce que Git ? Trois principes
2. Installation
3. Créer un projet en utilisant Git
4. Les plateformes d’hébergement Git (GitHub / GitLab / FramaGit / …)
5. Configurer Git
6. Les commandes les plus usuelles
{{< /pcache >}}

{{< psectiono >}}


{{< psectioni >}}

## 0. Introduction

Lorsque vous travaillez seul ou en équipe, vous risquez toujours de perdre une partie, voire **l'entièreté de votre travail**.
Cela devient particulièrement critique lorsque vous avez accumulé des mois, voire **des années de travail** et que l'échéance approche : les conséquences d'une mauvaise sauvegarde peuvent être désastreuses !

Heureusement, un système simple mais fort robuste vous permet d'enregistrer l'historique de vos modifications et d'en faire une archive distribuée à différents endroits.
Ce système est d'ailleurs fort utile lors de travaux réalisés à plusieurs : **Git**.

{{< psectiono >}}


{{< psectioni >}}
## 1. Qu'est-ce que Git ? Trois principes

![Logo de Git](https://git-scm.com/images/logo@2x.png)

Git est un logiciel de gestion de versions décentralisé : il met en place un _système distribué de contrôle de versions_ (*Distributed version control system*).

{{< pcache >}}
- **Système distribué**
- **Versionnement**
- **Contrôle**
{{< /pcache >}}

{{< pnote >}}

1. **Système distribué** : un ensemble de composants indépendants situés sur des machines différentes qui partagent des messages entre eux afin d'atteindre des objectifs communs.
2. **Versionnement** : chaque fois qu'on enregistre (commit), ou qu'on sauvegarde l'état de notre projet dans Git, Git prend un instantané de tous les fichiers, en sauvegardant une référence à l'instantané. Git traite donc nos fichiers comme une série d'instantanés dans le temps. Pour être plus efficace, si Git recconaît que certains fichiers n'ont pas changé depuis l'instantané précédent, Git ne les récupère pas à nouveau, mais crée smplement une collation vers le fichier précédent qui a déjà été sauvegardé. 
3. **Contrôle** : il n'est pas possible d'enregistrer (commit) une modification dans un fichier sans que cette modification soit vérifiée par le système de Git. Il ne peut pas arriver que des information soient perudes ou qu'un fichier soit corrompu sans que Git s'en aperçoive. Le mécanisme utilisé par Git pour mettre en œuvre cette vérification est un hash appelé SHA-1.

{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}

### 1.1. Un système distribué

- Git permet de **copier** des fichiers de son ordinateur vers un serveur tiers.
- Git permet d'avoir **plusieurs emplacements** de sauvegarde facilement.
- Git permet produire différentes versions **sans effacer son travail en cours** !
- Git permet de travailler à **plusieurs personnes** sur un même dossier.

{{< pnote >}}
**Les avantages d'un système distribué**

- Un système distribué permet de ne pas centraliser la sauvegarde ;
- de _cloner_ facilement = copier le dossier de travail avec l'ensemble de l'historique ;
- de travailler hors connexion ;
- de créer des versions _parallèles_ d'un projet

{{< /pnote >}}

{{< psectiono >}}


{{< psectioni >}}
<div style="display: grid; grid-template-columns: repeat(2, 1fr); gap: .5rem; align-items: last baseline;">

{{< figure src="/images/schema-reseau-simple.png" caption="Schéma d'un réseau centralisé simple" >}}

{{< figure src="/images/schema-reseau-distribue.png" caption="Schéma d'un réseau distribué" >}}

</div>

{{< psectiono >}}

{{< psectioni >}}

{{< figure src="/images/reseaux-paul-baran.png" caption="Les trois formes de réseaux selon Paul Baran (1962)" >}}

{{< pnote >}}
**Les défauts des logiciels de gestion de versions**

Il y a eu de nombreux systèmes ou logiciels de gestion de versions, mais tous avaient un ou plusieurs défauts :

- la nécessité d'être connecté pour travailler
- ne pas pouvoir disposer de tous les fichiers sur sa propre machine
- centraliser les fichiers à un seul endroit
- etc.

</details>
{{< /pnote >}}

{{< psectiono >}}


{{< psectioni >}}
### 1.2. Versionnement

Prenons un dossier de travail collaboratif sur un article :

{{< highlight shell "linenos=false" >}}
# dossier de travail
.
├── article-michel-coquilles-corrigees.txt
├── article-relu-2021-12-09-final-ok-okok.txt
├── article-v1-relu-alice-bob-13fev.txt
├── article-v1-relu-alice-bob-VRAI.txt
└── article-vfinal-jeanne.txt

{{< /highlight >}}

_Quel fichier est le bon ?_
_Quels fichiers contiennent les relectures de qui ?_

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
### 1.2. Versionnement
{{< /pcache >}}

Git permt de suivre un projet **dans son ensemble** (et pas forcément un fichier en particulier).

Il y a plusieurs principes inhérents à une bonne gestion collective de fichiers informatiques :

- **ne pas** centraliser la sauvegarde _(à votre avis, pourquoi ?)_
- la possibilité pour chacun d'obtenir une copie de **l'historique complet** du projet
- le travail **hors connexion**
- la création de versions parallèles d'un projet _(les branches ! nous y reviendrons)_.

{{< psectiono >}}


{{< psectioni >}}

{{< figure src="/images/branches-git-ecridil2018.png" caption="Visualisation d'un fil de travail collaboratif avec les « branches » de Git" >}}

{{< pnote >}}
Le **versionnement** (_versioning_ en anglais) entend répondre à plusieurs objectifs :

- enregistrer un ou plusieurs fichiers
- garder une trace des versions des fichiers
- naviguer dans l'historique des versions
- (le tout à plusieurs)

{{< /pnote >}}

{{< psectiono>}}


{{< psectioni >}}
### 1.3. Contrôle 

Git est un système de fichiers adressables en fonction de leur **contenu**.

{{< pnote >}}

Chaque fichier du dépôt Git est identifié par une chaîne de caractères hexadécimaux (0-9 et a-f) qui est calculée à partir du contenu du fichier et de sa position dans le dépôt.
- cette chaîne est appelé _hash_
- un identifiant pour un commit
- ex. `10b70d6c`

{{< /pnote >}}

{{< psectiono>}}


{{< psectioni >}}

{{< figure src="/images/git-exemple-diff-fichier.png" caption="Exemple de fichier avec la mise en surbrillance des modifications (*diff*)" >}}

{{< psectiono >}}


{{< psectioni >}}

### 1.5 Utilisations

Git est utilisé pour toutes sortes de projets: recherche scientifique, mémoires et doctorats, développement collaboratif de logiciels, archives personnelles, rédaction de documents en tous genres, publication de données, etc.

Exemples de projets utilisant Git :

- Programmes informatiques : [Hugo](https://github.com/gohugoio/hugo/)
- Documentation : [pandoc](https://github.com/jgm/pandoc/tree/master/doc)
- Projets éditoriaux : [Mosaïques romaines (catalogue de musée)](https://github.com/thegetty/romanmosaics)
- Données ouvertes : [NY Times, données migratoires sur les enfants traversant la frontière américaine sans l'accompagnement d'un adulte](https://github.com/nytimes/hhs-child-migrant-data)
- Des ressources pour [gérer ses références bibliographiques](https://github.com/pmartinolli/TM-MyThesaurus), par un certain bibliothécaire
- Rédaction universitaire : [mémoire de maîtrise](https://git.loupbrun.ca/louis/maitrise)

{{< psectiono>}}


{{< psectioni >}}
<!--
mkdir folder
git init .
git status
git add file.md (staging area)
git commit -m "comment"
git log
(modifier le fichier)
git status 
git diff file.md
 -->
## 2. Installation

### 2.1 En ligne de commande
Vous pouvez installer Git [depuis son site web](https://git-scm.com/downloads).


<details>
<summary>Pour voir si Git est bien installé sur votre machine</summary>

Dans votre terminal, tapez : `git --version`

</details>

<details>
<summary>Quelques méthodes d'installation en ligne de commande</summary>

- Linux : sur Debian ou Ubuntu, tapez `sudo apt install git-all`
- MacOS : `git --version` vous proposera d'installer un outillage pour le développement, lequel inclut Git
</details>

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 2. Installation
{{< /pcache >}}
### 2.2 Avec une interface graphique (GUI)

Si la ligne de commande n'est pas encore votre tasse de thé, ne craignez rien !
Il existe des **interfaces graphiques** qui vous permettent d'utiliser Git de manière visuelle.

{{< pnote >}}
**Quelques logiciels à interface graphique suggérés**

- [**Gitg**](https://wiki.gnome.org/Apps/Gitg/) est un logiciel libre très simple, suffisant pour apprendre les rudiments de Git.
- [**GitHub Desktop**](https://desktop.github.com/) propose une interface épurée, avec moins de fonctionnalités, disponible pour Windows et macOS.
- [**Sourcetree**](https://www.sourcetreeapp.com/) est un logiciel robuste, facile d'utilisation, développé par la société commerciale Atlassian.
- [**GitKraken**](https://www.gitkraken.com/) peut être téléchargé gratuitement, avec un nombre limité de fonctionnalités sans une licence payante.
{{< /pnote >}}

L'organisation qui chapeaute Git recense [plusieurs logiciels](https://git-scm.com/downloads/guis) dans cette catégorie.
(Attention, la plupart de ces logiciels ne sont que des _interfaces_ qui nécessitent l'installation de Git séparément !)

{{< psectiono >}}


{{< psectioni >}}
{{< figure src="/images/git-interface-graphique.png" caption="Exemple de répertoire de travail suivi avec Git avec une interface graphique" >}}
{{< psectiono >}}


{{< psectioni >}}
## 3. Créer un projet en utilisant Git

1. Créez un nouveau dossier pour votre projet.
2. Ajoutez des fichiers.
3. Commencez à versionner avec Git !

{{< figure src="/images/exemple-repertoire-git.png" caption="Exemple de répertoire pour un projet de rédaction d'article" >}}

{{< psectiono >}}


<!--{{< psectioni >}}
## 1. Les principes de Git

- suivre un projet
- développements/évolutions non linéaires (branches)
- historique général et particulier
- système de gestion de versions distribué
- complexité relative
- logiciels/plateformes qui simplifient l'usage

{{< pnote >}}
Git est pensé pour versionner **un projet**, seul·e ou à plusieurs.
Si Git permet de versionner des fichiers, il ne faut pas oublier que l'objectif final est bien de suivre un projet dans son ensemble et pas forcément un fichier en particulier
{{< /pnote >}}
{{< psectiono >}} -->

{{< psectioni >}}

{{< pcache >}}
## 3. Créer un projet en utilisant Git
{{< /pcache >}}

### Quelques définitions

- **dépôt** : C'est un dossier, tout simplement. Les fichiers sont versionnés dans un historique commun.
- **commit** : Une « photo » des fichiers à un instant donné ; un enregistrement atomique des changements dans un dépôt.
- **conflit** : Un conflit peut arriver lorsque l'on tente de _fusionner_ deux versions d'un même projet. Par exemple : Alice et Bob ont tous les deux modifié le titre de l'article -- mais lequel est le bon ?
- **clone** : Copier un projet chez soi. (Sur certaines platformes, on parle aussi de **fork** : une divergence d'un projet, une copie qui va vivre sa propre vie, ou presque.)<!-- attention, un fork fait partie de la lingua github -->

{{< psectiono >}}


{{< psectioni >}}
Exemple d'un fil de _commits_ :

{{< figure src="/images/exemple-commits-git-mmellet.png" caption="Fil de commits par Margot Mellet pour son [carnet d'écriture](https://github.com/Mmellet/Blank) (CC0 1.0 universel - domaine public)" >}}

{{< pnote >}}
**Qu'est-ce qu'un commit ?**

Un **commit** consiste en une série d'informations :

- un auteur
- une date
- un identifiant
- un message
- une liste de modifications associées : les fichiers modifiés

Pour comprendre comment fonctionne Git, il faut comprendre ce qu'est un _commit_ : ce n'est pas un enregistrement classique, mais **l'état du projet tout entier** après une série de modifications sur un ou plusieurs fichiers.
**C'est comme prendre une « photo » d'un dossier à un instant donné** :
on peut revenir à tout moment à cette photo -- ou à la photo d'un seul fichier !

{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}

### Exercice (en ligne de commande)

Dans un dossier `mon-projet/` :

1. `git init` pour initialiser un dépôt Git dans ce dossier.
2. Créez un nouveau fichier nommé `article.md`.
3. `git status` -- qu'est-ce que vous voyez ?
4. `git add article.md` -- vous indexez votre nouveau fichier dans les modifications à venir !
5. `git commit -m "le message d'enregistrement"` -- validez votre enregistrement.
6. `git log` (touche `q` pour sortir) -- qu'est-ce que vous voyez ?

{{< psectiono >}}


{{< psectioni >}}

{{< figure src="/images/exemple-depot-git-premier-commit.png" caption="Exemple d'état d'un dépôt après une première validation dans une interface graphique" >}}

{{< psectiono >}}


{{< psectioni >}}

{{< figure src="/images/exemple-depot-git-commits-exercice.png" caption="Exemple d'état d'un dépôt après plusieurs commits (rondes de modifications) dans une interface graphique" >}}

{{< psectiono >}}


{{< psectioni >}}

{{< figure src="/images/exemle-depot-git-commits-terminal.png" caption="Exemple d'état d'un dépôt après plusieurs commits, visualisation dans le terminal (avec la commande `git log`)" >}}

{{< psectiono >}}


{{< psectioni >}}
Hourra !
Nous avons **l'historique** de notre projet localement, sur notre ordinateur.
Cependant, il serait plus pratique d'ajouter une synchronisation externe.
{{< psectiono >}}


{{< psectioni >}}
## 4. Les plateformes d'hébergement Git (GitHub / GitLab / FramaGit / ...)

Les plateformes comme GitHub ou GitLab sont des services en ligne qui permettent d'héberger vos projets versionnés avec le protocole Git.

Avec Git, vous pouvez facilement passer d'un fournisseur à l'autre, et même en utiliser plusieurs à la fois !

Nous commencerons avec [GitHub](https://www.github.com/), qui est un service populaire et gratuit.

{{< psectiono >}}


{{< psectioni >}}
Exemple de page d'accueil d'un dépôt Git, sur la plateforme GitHub :

{{< figure src="/images/exemple-depot-git-mmellet.png" caption="Dépôt du [carnet d'écriture](https://github.com/Mmellet/Blank) de Margot Mellet (CC0 1.0 universel - domaine public)" >}}

{{< pnote >}}
**Git ≠ GitHub**

Attention ! Git (le logiciel) n'est pas synonyme GitHub/GitLab/... (des plateformes qui s'appuient sur ce logiciel).
{{< /pnote >}}

{{< psectiono >}}


{{< psectioni >}}

{{< figure src="/images/schema-git-simple.png" caption="Schéma d'une organisation simple qui distribue les copies d'un projet de manière centralisée" >}}

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Les plateformes d'hébergement Git (GitHub / GitLab / FramaGit / ...)
{{< /pcache >}}
### 4.1 Commencer avec GitHub

1. Créez un compte sur [GitHub.com](https://github.com/)
2. Créez un dépôt nommé `projet-debogue`. N'initialisez pas le dépôt (décochez cette option).
3. Explorez l'interface : Qu'observez-vous de différent par rapport aux propriétés du logiciel Git ? Qu'est-ce qui est similaire ?
4. Copiez l'URL de clonage via le bouton **Code**.
5. Ajoutez une synchronisation, en ligne de commande ou via l'interface graphique.
  - En ligne de commande : `git remote add origin url-que-vous-avez-copié`

... mais avant de pouvoir faire la synchronisation, il va falloir configurer Git sur votre poste !
{{< psectiono >}}


{{< psectioni >}}

## 5. Configurer Git

### 5.1. Configuration générale

Comme beaucoup d'outils, Git nécessite quelques réglages préalables !
Il est aussi possible de ajouter des configurations spécifiques pour un projet en particulier.

- De base : un nom et une adresse courriel
- Avancée : éditeur par défaut, branche par défaut
- Pour voir la configuration actuelle : `git config --list`
- Pour modifier la configuration globale depuis le terminal (recommandé la première fois) : \
  `git config --global user.name "Prénom Nom"` \
  `git config --global user.email "mon.addresse@umontreal.ca"`

<!--
mkdir folder
git init .
git status
git add file.md (staging area)
git commit -m "comment"
git log
(modifier le fichier)
git status 
git diff file.md
 -->

{{< pnote >}}
Git repose sur la reconnaissance des personnes qui contribuent ensemble à un même projet, il est donc primordial de bien identifier qui est qui.
Pour cela il faut au moins une adresse électronique, mais avec un nom ou un pseudonyme on gagne en lisibilité.

Il est aussi possible de spécifier [beaucoup d'autres paramètres](https://git-scm.com/docs/git-config), comme l'éditeur de texte par défaut qui sera utilisé pour les messages des _merges_. Par exemple : `git config --global core.editor /usr/bin/vim` (sur Windows il faut préciser le chemin complet de l'éditeur de texte).
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 5. Configurer Git
{{< /pcache >}}
### 5.2. Les clés SSH
- Secure Socket Shell
- Protocole de cryptage
- Pour faire interagir mon ordinateur (client) avec un serveur distant en sécurité
- Une **clé publique** (fichier avec le suffixe `.pub`, qui sera partagée) et une **clé privée** -- <mark>celle-ci ne doit jamais quitter votre ordinateur !</mark>

Pour en savoir plus, lire la [documentation fournie par GitHub](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent).

{{< pnote >}}
**Protocoles utilisés par Git**

Nous nous concentrons sur **SSH**, mais Git peut en réalité utiliser quatre protocoles distincts pour transférer des données : 
- Local
- HTTP
- Secure Shell (SSH) 
- Git
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 5. Configurer Git
{{< /pcache >}}
### 5.3. Générer une paire de clés SSH

{{< pnote >}}
Différents algorithmes peuvent être utilisés pour générer une clé SSH.
Ces algorithmes correspondent à différents modèles de cryptage de la clé.
{{< /pnote >}}

- Pour générer une paire de clés avec l'algorithme ed25519 :
  {{< highlight shell "linenos=false">}}
  ssh-keygen -t ed25519 -C "email@exemple.com"
  {{< /highlight >}}
- Pour générer une paire de clés avec l'algorithme RSA :
  {{< highlight shell "linenos=false">}}
  ssh-keygen -t rsa -b 4096 -C "email@exemple.com"
  {{< /highlight >}}
- drapeau `-t` pour spécifier l'algorithme à utiliser
- drapeau `-b` pour spécifier la longer (bits) de la clé
- drapeau `-C` pour ajouter un commentaire

Pour en savoir plus, lire la [documentation fournie par GitHub](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent#generating-a-new-ssh-key).

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 5. Configurer Git
{{< /pcache >}}
### 5.4 Installer une clé SSH
Lorsque vous aurez généré votre **paire de clés**, il faudra ajouter votre **clé publique** (`id_rsa.pub`) à votre compte GitHub.

1. Sur GitHub, naviguez aux paramètres de votre compte (**Settings**).
2. Dans la section **SSH keys**, ajoutez une nouvelle clé SSH.
3. À l'écran de l'ajout d'une nouvelle clé, collez le contenu de votre clé publique (`~/.ssh/id_rsa.pub`) dans le champ **Key**.
4. Donnez un nom à cette clé (une clé est normalement associée à un ordinateur), par exemple `Alice@portable-perso` ou `Bob@ordi-de-bureau`.
5. Assurez-vous que la clé est de type **Authentication Key**.
6. Cliquez sur le bouton **Add SSH Key**.

Voilà ! Vous devriez maintenant avoir les droits d'accès depuis votre poste.
Si vous avez trouvé cette étape complexe, n'ayez crainte : vous n'avez qu'à la faire la première fois !
{{< psectiono >}}

{{< psectioni >}}
## 6. Les commandes les plus usuelles

- `git init` : initialiser un dossier
- `git status` : voir l'état du projet
- `git log` : afficher l'historique de la branche actuelle
- `git add` : ajouter un fichier dans l'index avant de commiter
- `git commit` : déclarer des modifications
- `git branch` : créer une nouvelle branche
- `git checkout` : pour basculer sur une branche
- `git push` : envoyer les modifications sur un dépôt distant
- `git fetch` : récupérer les modifications d'un dépôt distant
- `git pull` : récupérer les modifications d'un dépôt distant **et** les fusionner avec le dépôt local

{{< pnote >}}
**Affichage de l'historique en ligne de commande**

Pour afficher un historique plus détaillé :

{{< highlight bash "linenos=false" >}}
git log --decorate=full --raw
{{< /highlight >}}

Ou pour afficher l'arbre des branches :

{{< highlight shell "linenos=false" >}}
git log \
  --branches \
  --remotes \
  --tags \
  --graph \
  --oneline \
  --decorate \
  --pretty=format:"%h - %ar - %s"
{{< /highlight >}}

Le concept de branche est une fonctionnalité bien pratique pour maintenir différentes versions d'un même projet.
{{< /pnote >}}
{{< psectiono >}} 


{{< psectioni >}} 
### 6.1. Quelques commandes avancées

- `git merge` -- fusionner l'historique d'une branche sur une autre
- `git rebase` -- harmoniser les historiques de deux branches (stratégie utile pour la fusion)
- `git cherry-pick` -- récupérer une seule modification sur une autre branche
- `git log` -- pour examiner l'historique des modifications

{{< psectiono >}}


{{< psectioni >}}
{{< imageg src="git-purr-01.jpg" >}}
{{< psectiono >}}


{{< psectioni >}}
{{< imageg src="git-purr-02.jpg" >}}
{{< psectiono >}}


{{< psectioni >}}
{{< imageg src="git-purr-03.jpg" >}}
{{< psectiono >}}


{{< psectioni >}}
{{< imageg src="basic-branching-6.png" >}}
{{< psectiono >}}


{{< psectioni >}}
### Conclusion

Voilà !
Vous avez appris les rudiments de Git.
Il ne vous reste plus qu'a l'employer dans vos propres projets.

Vous pouvez poursuivre la leçon pour des cas d'utilisation plus pointus (et tout à fait utiles !).
{{< psectiono >}}


{{< psectioni >}}
## 7. Conflits, merge, rebase, forks 

- un conflit : il faut choisir
- forks : comment maintenir sa version à jour ?
- rebase : les bonnes pratiques difficiles à mettre en place

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 7. Conflits, merge, rebase, forks 
{{< /pcache >}}
### 7.1. Un conflit : choisir/arbitrer

- pour distinguer les portions : des signes typographiques sont utilisés
- certains éditeurs de texte facilitent la visualisation des conflits
- si le conflit n'est pas résolu : impossible de continuer

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 7. Conflits, merge, rebase, forks 
{{< /pcache >}}
### 7.2. Exercice : gestion d'un conflit
Suite à l'exercice 1, nous allons créer plusieurs modifications :

1. créer une nouvelle branche appelée `modifs` : `git branch modifs` et puis `git checkout modifs`
2. vous êtes désormais sur cette nouvelle branche
3. modifier le fichier, par exemple : `echo "Autre texte hop là" >fichier-01.txt`
4. enregistrer vos modifications dans Git : `git commit -a -m "révision de la première ligne"`
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 7. Conflits, merge, rebase, forks 
{{< /pcache >}}
### 7.3. Exercice pt 2 : gestion d'un conflit

5. retourner sur la branche principale : `git checkout master` (ou `git checkout main` selon votre configuration)
6. effectuer une nouvelle modification : `echo "Ceci est mon texte" >fichier-01.txt`
7. enregistrer vos modifications : `git commit -a -m "réécriture"`
8. tenter de fusionner les deux branches : `git merge modifs`

Vous devez avoir un conflit !

{{< pnote >}}
Vous devez ouvrir le fichier pour résoudre le conflit :

- dans des éditeurs de texte comme Vim, Nano ou autre, Git ajoute des indications :
    - les `<<<<<<`, les `======` et les `>>>>>>`, ainsi que les mentions des commits et de `HEAD`
		- c'est à vous de conserver manuellement ce qui vous intéresse
- dans un éditeur de texte plus sophistiqué comme VSCode/VSCodium, des options vont vous être proposées pour choisir la version souhaitée (taper `code nom-du-fichier.md` sur terminal pour ouvrir VSCode en cas de conflit) ; 
- vous pouvez également utiliser des logiciels appelées "gitmerge tools", notamment meld (pour Linux : `sudo apt install meld` et `git config --global merge.tool meld`, puis `git meld` en cas de conflits)
- une fois ces modifications faites, vous devez enregistrer (`git add`) le fichier, et commiter tout cela ;
- si vous arrivez pas à résoudre les conflits et vous voulez retourner à la situation précédente : `git merge --abort`
- si vous volez annuler plusieurs commits : `git reset --hard hash-du-commit-que-je-veux-sauvegarder`
- effectuer un `git status` pour s'assurer que tout est en ordre !

{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 7. Conflits, merge, rebase, forks 
{{< /pcache >}}
### 7.4. Git merge
{{< imageg src="merge.png" >}}

[Source](https://git-scm.com/docs/git-merge)
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 7. Conflits, merge, rebase, forks 
{{< /pcache >}}
### 7.5. Git rebase
{{< imageg src="rebase.png" >}}

[Source](https://git-scm.com/docs/git-rebase)

{{< psectiono >}}


{{< psectioni >}}

{{< pcache >}}
## 7. Conflits, merge, rebase, forks 
{{< /pcache >}}
- `git checkout -b modifs` pour se positionner sur une nouvelle branche secondaire
- ajoutez et commitez deux fichiers sur cette branche
- `git checkout master`
- ajoutez et commitez un fichier sur cette branche
- `git checkout modifs`
- `git rebase master` : les commits dans la branche modifs sont positionnés à la suite des commits de la branche master
- `git checkout master`
- `git rebase feature` : les commits en plus dans ma branche _modifs_ sont désormais ajoutez dans la branche master  
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 7. Conflits, merge, rebase, forks 
{{< /pcache >}}
### 7.6. Forks : comment maintenir sa version à jour ?

1. Créer un **Fork** du projet sur votre compte GitHub.
2. Cloner le dépôt sur votre ordinateur (dans un autre dossier que le projet précédent).
3. Ajouter l'indication du fork en local : `git remote add upstream git@gitlab.chemin-du/projet`.
4. Pour récupérer les modifications du dépôt d'origine : `git fetch upstream`.
5. Pour synchroniser ces modifications avec _votre_ dépôt : `git rebase upstream/master`.
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 7. Conflits, merge, rebase, forks 
{{< /pcache >}}
### 7.7. Rebase : les bonnes pratiques difficiles à mettre en place
Un tutoriel complexe à explorer si vous êtes motivé·e :

[https://git-rebase.io](https://git-rebase.io)

{{< psectiono >}}


{{< psectioni >}}

### Conclusion

Vous avez passé à travers une montagne d'information -- ouf !
Ne soyez pas dur·e·s avec vous-mêmes : Git est un système complexe qui nécessite un apprentissage continu dans la durée.
Même les experts ne connaissent généralement qu'une fraction de son fonctionnement.

Toutefois, tous peuvent l'utiliser, et nous espérons que vous saurez bénéficier de ce qu'il a à offrir.

{{< psectiono >}}
