---
# Modèle de métadonnées pour une séance

# intitulé de la présentation
# @type chaîne de caractères libre
title: "Les langages de balisage (de markdown à HTML)"

# courte description
# chaîne de caractères libre
description: "Qu'est-ce qu'un langage de balisage ? Nous aborderons les enjeux autour du balisage d'un texte ; les différences entre plusieurs langages de balisage ainsi que certains cas d'usage."

# date de modification de la page (pour Hugo)
# @type chaîne de caractères contenant une date (format strict: "2006-01-02")
date: 2023-11-07

# heure de la présentation
# @type chaîne de caractères libre
heure_p: "13h-15h"

# lien de présentation (laisser vide pour "à venir" ou `false` pour aucun lien)
# @type booléen | chaîne de caractères
lien: "https://meet.jit.si/DebogueHumanitesCRCEN-BLSH"
# lieu, nom du local
# @type chaîne de caractères libre
lieu: "Bibliothèque des lettres et sciences humaines, local 3091"

# liste des compétences acquises (phrases complètes)
# @type liste<chaîne de caractères>
competences:
- "Rédiger un document en markdown et créer une page web en HTML"
- "Connaître les principaux éditeurs de texte pour écrire en markdown et en HTML"
- "Comprendre l'utilité des langages de balisage pour structurer un texte et l'enrichir sémantiquement"

# liste des formatrice·teur·s de la séance
# @type liste<chaîne de caractères>
formatrices:
- Louis-Olivier Brassard
- Giulia Ferretti

# numéro de la séance
# @type chaîne de caractères
seance: "03"

# saison, pour assurer l'archivage
# @type numéro
saison: 4

# Si la séance doit être listée ou non. La page sera tout de même générée.
# Notez que cela est différent de l'option `published` de Hugo,
# cette dernière permettant de régler la génération ou non de la page comme telle.
# @type booléen (true|false)
visible: true

# disposition style diapositives, voire `layouts/_default/diapositive.html`
# @type chaîne de caractères
layout: diapositive

# si les diapositives doivent figurer sur la page
# @type booléen (true|false)
afficher_diapositives: true

# si la boîte de présentation doit être ouverte par défaut ou non
# @type booléen (true|false) 
diapositives_ouvertes: true

# vignette de couverture (à fabriquer séparément)
images:
- /images/feature/saison-04/seance-03-les-langages-de-balisage.png
---

<!-- Le corps de texte va ici. -->

<!-- Baliser les diapositives avec les shortcodes `{{< psectioni >}}` pour ouvrir et `{{< psectiono >}}` pour fermer ; `{{< pnote >}}` au début et `{{< /pnote >}}` à la fin des notes à montrer dans le texte principal mais pas dans les diapositives, {{< pcache >}} au début et {{< /pcache >}} à la fin des répétitions des titres à montrer dans les diapositives mais pas dans le texte principal
 -->

{{< psectioni >}}
## Plan de la séance
1. Qu’est-ce que un langage de balisage ?
2. Rappel : les formats de balisage sémantique
3. Approfondissement : lire, éditer, convertir
4. Exercice : créer une page web, du markdown vers HTML
5. Les générateurs des sites statiques
{{< pnote >}}
Dans cette troisième séance, nous parlons des langages de balisage. Nous réfléchirons ensemble à leur signification et nous vous accompagnerons dans la manipulation de HTML pour créer votre premier site web !
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 1. Qu’est-ce que un langage de balisage ?

Un [langage de balisage](https://fr.wikipedia.org/wiki/Langage_de_balisage) est un système de codage de texte consistant en un ensemble de symboles insérés dans un document texte pour en contrôler la structure, le formatage ou la relation entre ses parties.   

Quelques exemples :
- LaTeX : se concentre généralement sur la typographie et la présentation
- XML : décrit les composantes sémantiques du texte, qui peuvent ensuite être traitées et mises en forme
- HTML : décrit principalement la structure sémantique du texte

{{< pnote >}}
Quelle est la différence entre un langage de balisage et un langage de programmation ?

- programmation : un ensemble d'instructions données à l'ordinateur pour qu'il effectue une action

- balisage : structuration d'un texte
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
## 2. Le traitement numérique du texte

Avec l'utilisation des logiciels de bureautique, on observe une
confusion entre structure et mise en forme

Deux paradigmes pour les éditeurs de texte :

-   WYSIWYG What You See Is What You Get
-   WYSIWYM What You See Is What You Mean

La structure sémantique permet de :

-   se repérer dans le texte
-   automatiser le traitement

{{< pnote >}}
Les éditeurs de texte WYSIWYM nous permettent de mieux comprendre la différence entre la structure d'un texte numérique et sa mise en forme.     

Nous pouvons donc nous concentrer sur la structure, qui, dans les éditeurs de texte WYSIWYG, est souvent confondue avec la mise en forme.
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
## 3. Les formats de balisage sémantique

-   Markdown
-   HTML
-   XML
Dans cette séance, nous approfondissons les caractéristiques de Markdown et de HTML.

{{< psectiono >}}

{{< psectioni >}}
## 4. Rappel : de Markdown à HTML avec Pandoc

1. ouvrir son terminal ;
2. créer un fichier en markdown (commande : `touch nom-fichier.md`) ;
3. insérer un titre, un sous-titre et un paragraphe de quelques mots ;
4. convertir le texte en html via pandoc.

{{< highlight "bash" "linenos=false" >}}
#!/bin/bash

pandoc texte-de-depart.md -o texte-de-sortie.html
{{< /highlight >}}

5. observer les balises html et ouvrir le document html dans votre navigateur
6. ajouter l'option standalone dans la commande pandoc 


{{< highlight "bash" "linenos=false" >}}
#!/bin/bash

pandoc --standalone texte-de-depart.md -o texte-de-sortie.html
{{< /highlight >}}

7. observer les nouvelles balises html et ouvrir le document dans votre navigateur : remarquez-vous des différences entre les deux conversions ?
{{< psectiono >}}

{{< psectioni >}}
## 5. Markdown et HTML : les balises principales 

-   [Les principales balises du HTML](https://htmlcheatsheet.com/)
-   [Les correspondances entre markdown et HTML](https://en.wikipedia.org/wiki/Markdown#Examples)
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}} 
## 5. Markdown 
{{< /pcache >}}
### 5.1. Markdown, un « standard » ?

_« Markdown » c'est deux choses : une syntaxe de balisage de texte brut et un outil logiciel qui convertit le balisage de texte brut en HTML pour la publication sur le web._
Markdown n'est pas un standard, parce qu'il n'est spécifié de manière non ambiguë.

> A Markdown-formatted document should be publishable as-is, as plain text, without looking like it’s been marked up with tags or formatting instructions.
> (John Gruber, inventeur de Markdown)

- [CommonMark](https://commonmark.org/) : il propose une version _standard_ et minimaliste de Markdown
{{< psectiono >}} 

{{< psectioni >}}
{{< pcache >}} 
## 5. Markdown
{{< /pcache >}}
### 5.2. Markdown et ses saveurs

Il existent nombreuses variantes de la syntaxe de Markdown, qui sont des sur-ensembles de la syntaxe de base. Voici quelques exemples :

- [Multimarkdown](https://fletcherpenney.net/multimarkdown/) : tableaux, citations, notes de bas de page, etc. Version minimaliste, se préoccupe de maintenir l’interopérabilité et la convertibilité des balises (par exemple en HTML ou en PDF en passant pas LaTeX). 
- [GitHub Flavored Markdown (GFM)](https://docs.github.com/en/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax) : pensé pour l'interface en ligne de GitHub. listes des tâches, `~texte barré~`, insertion automatique de liens, possibilité de visualiser les couleurs spécifiés, etc. 
- [Markdown Extra](https://michelf.ca/projects/php-markdown/extra/) : librairie dédié ([PHP Markdown Lib](https://michelf.ca/projects/php-markdown/)). classes et attributs à certains éléments, support pour les abbreviations (`<abbr>`), deux niveaux des classes pour les notes de bas de page, etc.
- [R Markdown](https://rmarkdown.rstudio.com/) : conçu pour la science des données. Exécution du code et intégration les résultats. Attention : l'exécution marche dans des environnements spécifiquement pensés pour R ou Python, comme [RStudio IDE](https://posit.co/download/rstudio-desktop/). Extension du format : `.Rmd`
{{< psectiono >}} 

{{< psectioni >}}
{{< pcache >}} 
## 5. Markdown : les balises principales 
{{< /pcache >}}
### 5.3. Markdown et ses usages
N.B. Notre objectif n'est pas connaître la syntaxe de tous les saveurs de markdown, me de connaître la syntaxe de base et éventuellement les saveurs qui répondent le mieux à nos besoins.

Si vous êtes curieux­·ses, vous pouvez comparer les différences entre les versions de Markdown : 
- [Babelmark](https://babelmark.github.io/faq/) 
- [Un résumé sur GitHub](https://gist.github.com/vimtaai/99f8c89e7d3d02a362117284684baa0f)
{{< psectiono >}}

{{< psectioni >}}
## 6. Où écrire en Markdown ?

Les éditeurs de texte qui supportent Markdown sont beaucoup, mais voici quelques exemples :
- [Stylo](https://stylo.huma-num.fr/)
- [StackEdit](https://stackedit.io/)
- [HedgDoc](https://demo.hedgedoc.org/) (collaboratif !) 
- Un éditeur dédié tel que [Zettlr](https://zettlr.com/) (pensé pour la rédaction scientifique), [Obsidian](https://obsidian.md/) (avec une vue sous forme de graph), [Typora](https://typora.io/) (avec prévisualisation à même le texte balisé), [iA Writer](https://ia.net/writer) (application dont l'ergonomie est professionnellement épurée).
- Un éditeur de texte(!), comme TextEdit (macOS), Notepad (Windows), GEdit ou éditeur de texte (distributions linux), `nano`/`vim`/`emacs`/... (éditeur basé dans le terminal).
{{< psectiono >}}


{{< psectioni >}}
## 7. La conversion en HTML

Attention à la « saveur » de Markdown supportée par vote outil de conversion.      

Par exemple, Pandoc propose une variante particulière : [Pandoc's Markdown](https://garrettgman.github.io/rmarkdown/authoring_pandoc_markdown.html).
{{< psectiono >}}

{{< psectioni >}}
## 7. La conversion en HTML
{{< pcache >}} 
### 7.1. des outils permettant la conversion

Étant donné que plusieurs environnements d'écriture permettent d'exporter en HTML des textes écrits en markdown, bien qu'ils n'offrent pas les mêmes possibilités de personnalisation que Pandoc. 

Voici quelques exemples :

- Stylo (qui utilise Pandoc pour la conversion)
- Hedgedoc
- Zettler

<!--Ajouter exercice HedgeDoc : créer des diapositives avec HedgeDoc grâce à Paged.js-->
{{< /pcache >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}} 
## 7. La conversion en HTML
{{< /pcache >}}
### 7.2. Exercice : une présentation en HTML avec HedgeDoc

- ouvrir un document HedgeDoc « Demo instance » à [CE LIEN](https://hedgedoc.org/demo/)
- copier le texte suivant dans le document
	- HTML 
	- créé en 1989-1990
	- langage de balisage
	- né pour la structuration de documents hypertextes
- diviser le texte en 4 diapositives, en suivant les instructions à [CETTE PAGE](https://docs.hedgedoc.org/references/slide-options/)
- `Menu` > `Slide Mode` pour afficher les diapositives en HTML
{{< pnote >}}
Dans le cas de HedgeDoc, la conversion est effectuée par [Reveal.js](https://revealjs.com/), une collection des bibliothèques (framework) en JavaScript.
{{< /pnote >}}
<!--Ajouter exercice HedgeDoc : créer des diapositives avec HedgeDoc grâce à Paged.js-->
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}} 
## 7. La conversion en HTML
{{< /pcache >}}
### 7.3. Exercice : personnaliser une présentation HedgeDoc

Consultez [CETTE PRÉSENTATION](https://demo.hedgedoc.org/p/slide-example#/ ) pour comprendre les possibilités de personnalisation.

Par exemple, vous pouvez ajouter des métadonnées au début du document :

{{< highlight "yaml" >}}
---
title: HTML
slideOptions:
  theme: simple
  transition: 'fade'
---
{{< /highlight >}}

Ou une image de fond au début de chaque diapositive :

{{< highlight "html" "linenos=false" >}}
<!-- .slide: data-background="proto://lien/vers/l-image/de/fond.jpg" -->
{{< /highlight >}}

{{< psectiono >}}

{{< psectioni >}}
## 8. HTML
### 8.1. Quel est l'intérêt de convertir en HTML ?

HTML est le langage du Web, utilisé par tous les navigateurs pour afficher du contenu.

Par exemple, nous pouvons encore afficher le contenu de [ce site](http://info.cern.ch/hypertext/WWW/TheProject.html), créé par Tim Berners-Lee au CERN en 1990 : du HTML brut avec beaucoup d'hyperliens. C'est le site web le plus ancien que nous connaissons.

{{< pnote >}}
C'est assez surprenant si l'on considère le nombre de modèles d'ordinateurs qui ont été produits et commercialisés depuis 1990. 
{{< /pnote >}}

Regardons le site : pourquoi a-t-il l'air si "daté" ?

{{< pnote >}}
- Les balises HTML nous permettent de **structurer l'information** pour qu'elle puisse être interprétée par un navigateur web et d'ajouter des hyperliens pour lier le document à d'autres documents. 

- Le langage CSS (Cascading Style Sheets), nous permet de structurer la mise en forme des pages web. <!-- Mais il e ... -->
{{< /pnote >}}
<!--ajouter l'histoire HTML : ex. le site du CERN et encore visible-->
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}} 
## 8. HTML
{{< /pcache >}}
### 8.2. Rappel : la structure d'un document en HTML (un exemple)

{{< highlight "html" "linenos=true" >}}
<!DOCTYPE html>
<html lang="fr">
  <head>
	  <title>Ma page</title>
	  <link rel="icon" href= "favicon.ico" />
  </head>
	<body>
	  <p>Du texte.</p>
	</body>
</html>
{{< /highlight >}}

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}} 
## 8. HTML
{{< /pcache >}}
### 8.3. Rappel : les principales caractéristiques de HTML

- HTML est un standard, mais il y a pas de systèmes de validation stricte (comme dans la cas de XML)
- Les balises sont imbriquées. Il n'est PAS possible de les chevaucher. Par exemple, cette syntaxe est incorrecte : `<p><em>du texte</p></em>`.

{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}} 
## 8. HTML
{{< /pcache >}}
### 8.4. Exercice : trouvez l'erreur
Dans ce court texte HTML, il y a des erreurs. Pouvez-vous les trouver ?

{{< highlight "html" "linenos=true" >}}
<!DOCTYPE html>
<html>
  <head>
    <title>Exemple de page Web</title>	
  </head>
    <h1>Ma Page Web</h1>
    <p>Voici un paragraphe.</p>
    
    <h3>Une liste de courses</h3>
    <ul>
      <li>Pommes</li>
      <li>Oranges</li>
      <li><strong>Poires</li></strong>
    </ol>
    
    <h2>Une image de chat</h2>
    <img src="chat.jpg" alt="Un chat mignon">
    
    <p>C'est la fin de ma page.
  </body>
</html>
{{< /highlight >}}

<!--ajouter code-->
{{< psectiono >}}

{{< psectioni >}}
## 9. Pourquoi le format texte ?
« Des fichiers plutôt que des applis » par Steph Ango (l'un des créateurs du logiciel Obsidian) :

> Les applications sont éphémères, mais vos fichiers ont une chance de durer. \
> (<em lang="en">Apps are ephemeral, but your files have a chance to last.</em>)
    
---[Steph Ango](https://stephango.com/file-over-app), <cite lang="en">File over app</cite>.
{{< psectiono >}}

{{< psectioni >}}
## 10. La conversion avec Pandoc 
### 10.1. Pour adapter la conversion à nos besoins : les modèles Pandoc -- Partie 1

- créer un document markdown à partir de votre terminal avec commande `touch`
- inclure un texte de votre choix
- ajouter des métadonnées, qui seront traitées par votre modèle
Par exemple : 

{{< highlight "yaml" >}}
---
title: Mon document markdown => HTML
author: Jeanne Doe
date: 2023-11-07
rights: Creative Commons BY-SA 4.0. Certains droits réservés.
---
{{< /highlight >}}
	
- créer le document contenant votre modèle (commande: `touch mon-template.html`) et insérer votre modèle

{{< pnote >}}

Un exemple très simple de modèle :

{{< highlight "html" "linenos=true" >}}
<!DOCTYPE html>
<html>
  <head>
    <!-- Titre du document (métadonnée) -->
    <title>$title$</title>

    <!-- Auteur du document (métadonnée) -->
    <meta name="author" content="$author$">
  </head>

  <body>
    <!-- Titre du document (affiché sur la page) -->
    <h1 class="title">$title$</h1>

    <!-- Auteur du document (affiché sur la page) -->
    <p class="author">$author$</p>

    <!-- Date (si spécifiée) -->
    $if(date)$
    <p class="date">$date$</p>
    $endif$

    <!-- Corps du texte -->
    <div>$body$</div>

  </body>
</html>
{{< /highlight >}}
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 10. La conversion avec Pandoc 
### 10.2. Pour adapter la conversion à nos besoins : les modèles Pandoc -- Partie 2

Grâce à l'option `--template` de Pandoc, nous pouvons appliquer le modèle à notre html de sortie. 

{{< highlight "bash" "linenos=false" >}}
#!/bin/bash

pandoc --template mon-template.html mon-document.md -o mon-document.html
{{< /highlight >}}

Et si je veux ajouter le pied de page spécifié dans les métadonnées à mon modèle ?
{{< psectiono >}}