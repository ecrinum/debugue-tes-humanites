---
# Modèle de métadonnées pour une séance

# intitulé de la présentation
# @type chaîne de caractères libre
title: "Les formats"

# courte description
# chaîne de caractères libre
description: "Qu'est-ce qu'un format ? C'est une façon particulière d'organiser et structurer des données dans un fichier. Dans cette séance, vous apprendrez les caractéristiques de plusieurs formats, notamment : le texte brut, markdown, HTML, XML."

# date de modification de la page (pour Hugo)
# @type chaîne de caractères contenant une date (format strict: "2006-01-02")
date: 2023-10-24

# heure de la présentation
# @type chaîne de caractères libre
heure_p: "13h-15h"

# lien de présentation (laisser vide pour "à venir" ou `false` pour aucun lien)
# @type booléen | chaîne de caractères
lien: "https://meet.jit.si/DebogueHumanitesCRCEN-BLSH"

# lieu, nom du local
# @type chaîne de caractères libre
lieu: "Bibliothèque des lettres et sciences humaines, local 3091"

# liste des compétences acquises (phrases complètes)
# @type liste<chaîne de caractères>
competences:
- "Utilisation de base de pandoc, un logiciel de conversion entre formats"
- "Connaître la structure des principaux formats de balisage : markdown, HTML, PDF"
- "Comprendre l'intérêt d'utiliser différents formats à des fins différentes"

# liste des formatrice·teur·s de la séance
# @type liste<chaîne de caractères>
formatrices:
- Louis-Olivier Brassard
- Giulia Ferretti

# numéro de la séance
# @type chaîne de caractères
seance: "02"

# saison, pour assurer l'archivage
# @type numéro
saison: 4

# Si la séance doit être listée ou non. La page sera tout de même générée.
# Notez que cela est différent de l'option `published` de Hugo,
# cette dernière permettant de régler la génération ou non de la page comme telle.
# @type booléen (true|false)
visible: true

# disposition style diapositives, voire `layouts/_default/diapositive.html`
# @type chaîne de caractères
layout: diapositive

# si les diapositives doivent figurer sur la page
# @type booléen (true|false)
afficher_diapositives: true

# si la boîte de présentation doit être ouverte par défaut ou non
# @type booléen (true|false) 
diapositives_ouvertes: false

# vignette de couverture (à fabriquer séparément)
images:
- /images/feature/saison-04/seance-02-les-formats.png
---

<!-- Le corps de texte va ici. -->

<!-- Baliser les diapositives avec les shortcodes `{{< psectioni >}}` pour ouvrir et `{{< psectiono >}}` pour fermer ; `{{< pnote >}}` au début et `{{< /pnote >}}` à la fin des notes à montrer dans le texte principal mais pas dans les diapositives, {{< pcache >}} au début et {{< /pcache >}} à la fin des répétitions des titres à montrer dans les diapositives mais pas dans le texte principal
 -->

{{< psectioni >}}
## Plan de la séance

1. Qu'est-ce qu'un format ?
2. Quelles sont ses implications techniques et politiques ?
3. En pratique ! Exercices d'encodage
{{< psectiono >}}

{{< psectioni >}}
## 1. Les formats
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 1. Les formats
{{< /pcache >}}
### 1.1. Qu'est-ce que un format ? -- les origines du terme

Les formats de papier pour l'imprimé, première apparition *technique* du terme ?
{{< pnote >}}
Le terme *format* est un terme technique, son usage permet de délimiter les caractéristiques d'un objet : avec le format nous donnons un certain nombre de données, d'instructions, ou de règles. Pourquoi définir tout cela ? L'objectif est de constituer une série d'informations compréhensible, utilisable et communicable.

Pour prendre un exemple concret du côté du livre, l'impression d'un document nécessite de s'accorder sur un format de papier. Les largeurs, longueurs et orientation sont normalisées, des standards sont établis, ils permettent alors de concevoir des imprimantes qui peuvent gérer des types définis de papier. 

Sans des formats de papier il est difficile de créer des machines adéquates, comme des presses à imprimer ou des imprimantes. L'usage du format dans l'imprimerie est sans doute la première apparition de ce terme technique. Cependant, il est intéressant de noter que le _format_ est ainsi d'abord attaché au livre et à sa fabrication. 

Notons également que des outils ou des processus sont associés au concept de format : les instructions sont définies pour qu'une action soit réalisée par un agent -- humain, analogique, mécanique, numérique. 

Le concept de format è étroitement lié avec le concept de média : les médias supportent des formats spécifiques (par exemple, un lecteur MP3 lit uniquement les informations sonores au format MP3). L'existence des médias dépend de l'existence des formats.
{{< /pnote >}}

{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}} 
## 1. Les formats
{{< /pcache >}}
### 1.2. Et les formats informatiques ?

Une façon particulière d’organiser et structurer des données dans un fichier. 

> En informatique, au niveau le plus fondamental, tout est exprimé dans un alphabet numérique binaire : 0 et 1. Un format, c’est une convention qui établit la correspondance entre une certaine succession de 0 et de 1, et quelque chose d’autre : par exemple une couleur, ou une lettre de l’alphabet, ou la position d’un pixel sur un écran. L’expression « format texte » désigne une catégorie de formats pour lesquels le contenu en binaire des fichiers encode des caractères textuels uniquement.

---[Arthur Perret, 2021](https://www.arthurperret.fr/cours/2021-09-21-ecriture-academique-format-texte.html)


{{< pnote >}}

Un fichier (vidéo, image ou texte) peut être produit, transmis ou lu par l'ordinateur en raison du fait qu'il est encodé dans un format numérique.  

Un format informatique est le lien entre l'infrastructure et l'agent (humain ou programme) qui utilise cette infrastructure.

Le choix des
formats informatiques détermine la manière dont les informations sont
créés, stockées, envoyées, reçues, interprétées, affichées. Aujourd'hui
les formats prennent une place importante dans notre environnement, et
leur incidence dépasse le domaine de l'informatique, leur étude a
pourtant été longtemps délaissée dans le champ des médias.

Exemple du format DOC ou `.doc` : le logiciel Microsoft Word ne peut pas
lire n'importe quel format informatique, les données doivent être
structurées d'une façon précise pour que le logiciel puisse les
interpréter, et ensuite les modifier, et enfin produire une nouvelle
version du fichier. Ici le format DOC a été créé pour les besoins d'un
logiciel spécifique.

Dans cet exemple c'est le format informatique qui est le résultat du
logiciel, mais d'autres fonctionnement sont possibles. Par ailleurs, le
format DOC est longtemps resté propriétaire (jusqu'à l'arrivée du format
DOCX), ses spécifications n'étaient pas publiques et des brevets
empêchaient toute initiative de développement d'un logiciel autre que
Word capable de lire ou de modifier des fichiers `.doc`.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
## 2. Implications technique et politiques
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}} 
## 2. Implications technique et politiques
{{< /pcache >}}
### 2.1. L'interopérabilité

Transmission d'informations entre machines
{{< pnote >}}
L'interopérabilité est un principe qui permet à plusieurs machines de
dialoguer :

- en s'accordant sur des règles pour définir une série d'informations,
  les machines peuvent lire et écrire un fichier ;
- si ces spécifications sont clairement énoncées, il n'y a alors plus
  de dépendance vis-à-vis d'un logiciel spécifique ;
- la question de l'interopérabilité est intimement liée à la
  standardisation et à l'ouverture du format.
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}} 
## 2. Implications technique et politiques
{{< /pcache >}}
### 2.2. Format ouvert ou fermé ?

Standards et licences.
{{< psectiono >}}


{{< psectioni >}}
## 3. PDF, DOCX, XML, MD, HTML de plus près
Standards et licences.
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}} 
## 3. PDF, DOCX, XML, MD, HTML de plus près
{{< /pcache >}}
### 3.1. PDF

- Portable Document Format : pour afficher et imprimer les fichiers
  toujours de la même manière
- 1991 par Adobe
- Depuis 2008, standard ouvert : [ISO standard](https://www.iso.org/about-us.html)
  [32000-1:2008](https://opensource.adobe.com/dc-acrobat-sdk-docs/standards/pdfstandards/pdf/PDF32000_2008.pdf)
- Le fichier est structuré sur quatre niveaux
  - Header : indique la version de la spécification PDF du fichier
  - Body : contenant les objets qui constituent le document contenu
    dans le dossier
  - Cross-reference Table : contenant les informations sur les
    objets indirects dans le fichier
  - Trailer : indiquant l'emplacement de la *cross-reference table*
    dans le corps du fichier.
- À l'intérieur de la balise `body`, [chaque page est traitée
  individuellement](https://pdfux.com/inspect-pdf/)
- En raison de cette structure complexe, les fichiers PDF sont beaucoup plus lourds qu'un simple fichier texte -- c'est-à-dire, dans un format *texte brut*.

{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}} 
## 3. PDF, DOCX, XML, MD, HTML de plus près
{{< /pcache >}}
### 3.2. DOCX

- `.doc` (à partir de MS Word 97) : format binaire, lisible uniquement par le logiciel propriétaire 
    Microsoft Word
  - Suite aux pressions de la communauté, Microsoft a publié une [spécification](https://learn.microsoft.com/en-us/openspecs/office_file_formats/ms-doc/ccd7b486-7881-484c-a137-51170af7cc22) pour ce format
- `.docx` (depuis 2007) :  est format par défaut des documents produit avec le logiciel Microsoft Word
  - `.doc` + `x` (XML) : intégration du langage de balisage XML
  - Qu'est-ce qu'un fichier DOCX ? C'est simplement un dossier zip contenant des fichiers XML !
  - S'il est possible de *lire* l'encodage d'un fichier `.docx`, son format s'avrère très verbeux -- essayez pour voir !
{{< psectiono >}}

{{< psectioni >}}

![Un document de traitement de texte au format Microsoft Word, dans lequel il n'y a d'écrit que « Bonjour ».](/images/2023-10-24-word-bonjour.png)

{{< psectiono >}}

{{< psectioni >}}

![Aperçu des fichiers produits par l'écriture de « Bonjour! » et l'insertion d'une image dans un document Microsoft Word DOCX.](/images/2023-10-24-debogue-word-bonjour.png)

{{< pnote >}}
Un paquet DOCX contient habituellement ces dossiers, ainsi que de nombreux fichiers :
- `word/` : contient le contenu principal du document (texte, styles, thèmes)
- `_rels/` : liste des principales parties du paquet
- `docProps/` : contient les propriétés du document -- dont certaines sont liées à l'application Microsoft Word
- `[Content_Types].xml` : contient des informations sur les types de médias contenus dans le document
{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}} 
## 3. PDF, DOCX, XML, MD, HTML de plus près
{{< /pcache >}}
### 3.3. XML

- XML : acronyme pour *Extensible Markup Language*
- Créée en 1998
- Les données sont stockées sous forme de **texte brut** : les informations sont créées, stockées, transportées et partagées *indépendamment des logiciels utilisés*
- XML est conçu pour la structuration stricte de données
- Il permet de décrire une **structure hiérarchique**
- Les informations sont validées par des **schémas** : un schéma définit et contraint les caractéristiques qu'un document XML donné peut avoir. Par exemple : un livre _doit_ comporter un titre (obligatoire), mais _peut_ contenir des chapitres (facultatif)
- C'est un format général pour décrire les données. (On dira aussi : un *méta-format*.) Il n'y a pas un seul format XML, mais bien plusieurs schémas possibles !

{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}} 
## 3. PDF, DOCX, XML, MD, HTML de plus près
{{< /pcache >}}
### 3.5. HTML

- HTML : acronyme pour *Hypertext Markup Language*
- Créé en 1993
- C'est le langage du Web, permettant d'écrire des **hyperdocuments**, reliés entre eux par des hyperliens (*hyperlinks*)
- Comme XML, HTML a été conçu pour marquer le texte de manière **structurelle** et non pour décrire la *présentation* d'une page web
- En 2000, HTML est devenu un standard international (ISO/IEC 15445:2000)<!--. Entre 1995 et 2000, les spécifications HTML ont été produites et maintenues par les entreprises propriétaires des navigateurs web-->
- La cinquième version (« HTML5 », officialisée en 2014), s'adapte à l'évolution du Web
{{< psectiono >}}



{{< psectioni >}}
{{< pcache >}} 
## 3. PDF, DOCX, XML, MD, HTML de plus près
{{< /pcache >}}
### 3.6. La structure d'un fichier HTML

{{< highlight "html" "linenos=false" >}}
<!doctype html>
 <html>
  <head>
    <meta charset="utf-8">
    <title>Titre dans l'onglet</title>
  </head>
  <body>
    <h1>Titre</h1>
    <p>Ceci contient un paragraphe.</p>
  </body>
</html>
{{< /highlight >}}

{{< pnote >}}
Pour consulter le `html` des pages web : `view-source:` + lien.    

Par exemple, pour consulter le code source de l'article Sens Public dans le navigateur : `view-source:` + [https://www.sens-public.org/articles/1600/](https://www.sens-public.org/articles/1600/)

Quelques balises html : voir [ICI](https://www.codecademy.com/learn/learn-html/modules/learn-html-elements/cheatsheet)
{{< /pnote >}}

{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}} 
## 3. PDF, DOCX, XML, MD, HTML de plus près
{{< /pcache >}}
### 3.4. Markdown
- Créé en 2004
- Les données sont stockées sous forme de **texte brut** : les informations sont créées, stockées, transportées et partagées *indépendamment des logiciels utilisés*
- C'est un langage de balisage **léger**. *Mark up* et *mark down* : baliser, mais baliser « moins »
- Lisible par les humains, mais également pratique pour la rédaction 
- Un surcouche de HTML : tous elements HTML sont valides dans les documents Markdown
- [Tableau d'équivalence avec HTML](https://en.wikipedia.org/wiki/Markdown#Examples)

{{< pnote >}}
Markdown permet de baliser :

- des niveaux de titre, paragraphe, citation longue, listes
- du texte en gras, souligné, italique
- des liens, des images, tableaux, et même des commentaires

{{< /pnote >}}
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}} 
## 3. PDF, DOCX, XML, MD, HTML de plus près
{{< /pcache >}}
### 3.5. Exercice avec Markdown
Baliser le texte d’un article avec du markdown

[Lien vers l’article](https://www.sens-public.org/articles/1600/)

[Lien vers le texte brut à baliser](https://demo.hedgedoc.org/fwPscqC3S6eTDWGPhUiZMQ#)

Identifier
- le titre, sous-titre
- image initiale
- référence
- titre en italique


{{< psectiono >}}


{{< psectioni >}}
## 4. Conversion entre formats
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Conversion entre formats
{{< /pcache >}}
### 4.1. La conversion

Pour les formats textuels, la conversion consiste en la transformation de balises *(plus légères)* en d'autres balises *(plus verbeuses)*.

En entrée :

- texte balisé
- métadonnées sérialisées
- bibliographie structurée

En sortie :

- texte mis en forme, selon la  d'affichage (comme les titres en gras ou en italique)
- bibliographie formatée selon la convention choisie (APA, MLA, Études françaises, etc.)

{{< pnote >}}
La conversion est un processus qui prendre un certain nombre de données
en compte, et pas uniquement le *texte* balisé.
{{< /pnote >}}
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Conversion entre formats
{{< /pcache >}}
### 4.2. Conversion entre formats

Des logiciels permettent de passer aisément d'un format à un autre,
comme [Pandoc](https://en.wikipedia.org/wiki/Pandoc#Supported_file_formats) :

- Créé 2006, par le philosophe [John MacFarlane](https://johnmacfarlane.net/)
- Pandoc est un convertisseur « agnostique », il n'impose pas une pratique
    par rapport à un balisage.
- Bien que ce logiciel puisse faire des conversions entre une myriade de formats, **certaines conversions sont impossibles** : un format complexe peut être
    trop difficile à réduire, au risque de perdre la majorité des
    informations, ou encore il peut être impossible de le lire (c'est le cas des formats binaires dont aucune spécification n'est publiée).
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}}
## 4. Conversion entre formats
{{< /pcache >}}
### 4.3. Fonctionnement de Pandoc

Pandoc fonctionne en ligne de commande, avec un schéma classique
d'options.

En rappel : en ligne de commande, on commence toujours par appeler un *programme* (ici, `pandoc`), en lui fournissant une *séquence d'arguments* (comme un fichier source à convertir) et d'*options* (gestion du format, recours à un modèle, etc.).

```
<programme> <fichier.entrée> (optionnel: <fichier.sortie>, <drapeau>, etc.)
```
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}}
## 4. Conversion entre formats
{{< /pcache >}}
### 4.4. Pandoc -- Exemples

**Conversion d'un fichier source au format markdown.**
Notez que le format de sortie n'a pas été précisé : *par défaut*, Pandoc répondra au format HTML, directement dans la console, sans enregistrer la réponse dans un fichier. Essayez pour voir ce que ça donne !

{{< highlight "shell" "linenos=false" >}}
#!/bin/bash

pandoc mon-fichier.md

# ...
{{< /highlight >}}

{{< psectiono >}}

{{< psectioni >}}
**Production d'un fichier HTML.** Le prochain exemple crée un fichier `html`, nommé `mon-fichier.html` (à partir du fichier source `mon-fichier.md`). On y fournit également les « drapeaux » (ou options) de ligne de commande permettant d'*expliciter* le format d'entrée (`--from`, ou `-f` pour faire court) :

{{< highlight "shell" "linenos=false" >}}
#!/bin/bash
# Note: les barres obliques inversées permettes de poursuivre l'écriture
# de la commande sur une ligne séparée.

pandoc \
  --from markdown \
  --to html \
  mon-fichier.md \
  --output mon-fichier.html`

# => "mon-fichier.html" sera produit !
{{< /highlight >}}

{{< psectiono >}}

{{< psectioni >}}
**Utilisation d'un modèle.** La troisième commande applique un modèle ou gabarit (en anglais : *template*), permettant ainsi
de *prérégler* le rendu du document. On gagnera ainsi du temps en réutilisant un modèle fréquemment utilisé. Ici, on suppose l'existence du fichier `mon-modele.html`, appelé avec l'option `--template` de Pandoc :

{{< highlight "shell" "linenos=false" >}}
#!/bin/bash
# Note: les barres obliques inversées permettes de poursuivre l'écriture
# de la commande sur une ligne séparée.

pandoc \
  --from markdown \
  --to html \
  --template mon-modele.html \
  mon-fichier.md \
  --output mon-fichier.html

# => "mon-fichier.html" sera produit !
{{< /highlight >}}

{{< psectiono >}}

{{< psectioni >}}
## 5. Pandoc en pratique
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}} 
## 5. Pandoc en pratique
{{< /pcache >}}
### 5.1. Installer Pandoc

- Terminal
  - Windows : Start \> Windows Power Shell
    - à partir de Windows 10 : installer l'application [Ubuntu on
      Windows](https://apps.microsoft.com/store/detail/ubuntu-on-windows/9NBLGGH4MSV6?hl=en-ca&gl=ca&rtc=1)
  - Mac : chercher « terminal »
  - Linux : chercher « terminal »
{{< psectiono >}}


{{< psectioni >}}
{{< pcache >}} 
## 5. Pandoc en pratique
{{< /pcache >}}
### 5.2. Installation -- Deuxième étape

Pour installer Pandoc, suivez les instructions sur cette page :
<https://docs.zettlr.com/fr/installing-pandoc/>

{{< psectiono >}}

{{< psectioni >}}
## 6. Exercice
- Créer un document Markdown avec des niveaux de titre, une liste, une
    citation longue et de l'emphase (italique et gras)
- Lancer la commande `pandoc mon-fichier.md`, en l'adaptant selon les exemples montrés précédemment
- Ouvrir le fichier HTML obtenu
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}} 
## 6. Exercice
{{< /pcache >}}
### 6.1. Ajouter des métadonnées 

Pour ajouter des **métadonnées**, on débutera l'écriture du document markdown avec un bloc au format YAML, délimité par les triplets suivants `---` :

{{< highlight "yaml" "linenos=true" >}}
---
titre: Le titre de mon document
auteur: Mon Nom
---

<!-- Le corps du document ira ici -->
{{< /highlight >}}

Essayez de convertir votre fichier markdown en HTML, puis en DOCX. \
*(Indice : [manuel d'utilisateur de Pandoc](https://pandoc.org/MANUAL.html))*
{{< psectiono >}}

{{< psectioni >}}
Suite à vos conversions, vous devriez obtenir la série de fichiers suivants :

{{< highlight "shell" "linenos=false" >}}
# dans votre répertoire courant...
.
├── mon-fichier.docx
├── mon-fichier.html
└── mon-fichier.md
{{< /highlight >}}

Ouvrez les fichiers obtenus. Que remarquez-vous ?
{{< psectiono >}}

{{< psectioni >}}
{{< pcache >}} 
## 6. Exercice
{{< /pcache >}}
### 6.2. Appliquer un modèle

Créez un fichier HTML avec le code suivant :

{{< highlight "html" "linenos=true" >}}
<!doctype html>
<html>
  <head>
    <!-- Titre du document, dans les métadonnées -->
    <title>$titre$</title>

    <!-- Auteur du document, dans les métadonnées -->
    <meta name="author" content="$auteur$">
  </head>

  <body>
    <!-- Titre du document, affiché une balise `h1` -->
    <h1 class="titre">$titre$</h1>

    <!-- Auteur du document -->
    <p class="auteur">$auteur$</p>

    <!-- La date, si indiquée dans le document source -->
    $if(date)$
    <p class="date">$date$</p>
    $endif$

    <!-- Le corps du document, ou contenu du markdown,
         sera inséré ici, dans la variable `$body$` de pandoc -->
    <div>$body$</div>

  </body>
</html>
{{< /highlight >}}

{{< psectiono >}}

{{< psectioni >}}

Lancez la conversion en HTML en appliquant le modèle/template, puis ouvrez le document obtenu.

{{< psectiono >}}

{{< psectioni >}}
## Ressources 
- Arthur Perret, <cite>L'écriture académique au format texte</cite>,
  <https://www.arthurperret.fr/2021-09-21-ecriture-academique-format-texte.html>
- Antoine Fauchié, <cite>Fabriques de publication : Pandoc</cite>,
  <https://www.quaternum.net/2020/04/30/fabriques-de-publication-pandoc/>
- Dennis Tenen et Grant Wythoff, « Rédaction durable avec Pandoc et
  Markdown », <cite>Programming Historian</cite>,
  <https://programminghistorian.org/fr/lecons/redaction-durable-avec-pandoc-et-markdown>
- Nicolas Sauret et Marcello Vitali-Rosati, <cite>tutorielMdPandoc</cite>,
  <https://framagit.org/stylo-editeur/tutorielmdpandoc>
- Documentation de Pandoc : <https://pandoc.org/MANUAL.html>
{{< psectiono >}}
