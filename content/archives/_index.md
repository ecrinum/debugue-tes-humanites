---
title: Archives
outputs: ['HTML', 'yml']
---

Cette page recense les séances passées de l’atelier <a href="/" rel="self" class="debugue">Débogue tes humanités</a>.
Un enregistrement est proposé lorsque cela est possible.
